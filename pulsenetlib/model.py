#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Definition of the neural network model
"""

# native modules
##none##

# third-party modules
import torch
import torch.nn as nn

# local modules
import pulsenetlib
import pulsenetlib.architectures as arch
import pytorch_lightning as pl

class PulseNet(pl.LightningModule):
    """ Neural network model
    
    ----------    
    ARGUMENTS
    
        input_channels (int): number of input channels 
        output_channels (int): number of output channels
        input_frames (int): number of input temporal frames
        output_frames (int): number of output temporal frames
        std_norm (tensor): standard deviation division normalization factors (must have
                  as much values as channels - second dimension
                  of the input)        
        avg_remove (tensor): average removal normalization factors (must
                    have as much values as channels - second dimension
                    of the input)       
        layers_description (dict): layers description for models. See
            architectures/unet.py or architectures/multiscale.py for details
        model_name (str): architecture name (avaliable: 'MultiScale', 'UNet')
        mask_channel (str): index indicating mask in input tensor (second dimension)
        normalization (str or list): normalization applied to all channels
            (str) or per channel (list of str).
            Options: 'gaussian' or 'none'
        padding_mode (str): padding type for convolutions. Default: 'replicate'
        
    """
    def __init__(self, 
                 input_channels, output_channels,
                 input_frames, output_frames,
                 std_norm, avg_remove,
                 layers_description,
                 model_name='UNet',
                 mask_channel=None,
                 normalization='gaussian',
                 padding_mode='replicate'):
        
        super(PulseNet, self).__init__()
        
        self.mask_channel = mask_channel
        self.num_masks = 0
        if self.mask_channel is not None:
            self.num_masks = 1
        
        self.num_input_channels = input_channels
        self.num_output_channels = output_channels
        self.num_input_frames = input_frames
        self.num_output_frames = output_frames
        
        # pages are defined as pages = frames * (channels - masks)
        self.num_pages = self.num_input_frames*(self.num_input_channels-self.num_masks)
       
        # add back the masks only once (they do not change over time)
        self.num_pages_with_masks = self.num_pages + self.num_masks
        if self.num_pages < 1:
            raise ValueError('num_pages found to be negative or zero.')

        self.register_buffer('std_norm', std_norm, persistent=False)
        self.register_buffer('avg_remove', avg_remove, persistent=False)
        self.normalization = normalization
        if not isinstance(self.normalization , list):
            self.normalization = [normalization]*self.num_input_channels
        else:
            if len(normalization) != (self.num_input_channels):
                raise ValueError(f'normalization must have same length '
                                 f'as number of input channels '
                                 f'({self.num_input_channels}). '
                                 f'Found len {len(normalization)}')


        if model_name == 'MultiScale':
            # MultiScale CNN from Mathieu et al. http://arxiv.org/abs/1511.05440
            # with arbitrary number of scales and layers
            if layers_description is None:
                raise ValueError(f'layers_description mut be implemented for'
                                 f'model_name=GeneralScaleNet')
            if 'MultiScale' not in layers_description:
                raise ValueError(f'layers_description must have a "MultiScale" key for'
                                 f'model_name=MultiScale')
            self.net = arch.multiscale.MultiScaleNet(
                            layers_description['MultiScale'], padding_mode)
        elif model_name == 'UNet':
            # U-Net CNN, adapted from 
            # Ronneberger et al. https://arxiv.org/abs/1505.04597
            # with arbitrary number of scales and layers
            if layers_description is None:
                raise ValueError(f'layers_description must be implemented for'
                                 f'model_name=UNet')
            if 'UNet' not in layers_description:
                raise ValueError(f'layers_description must have a "UNet" key for'
                                 f'model_name=UNet')
            self.net = arch.unet.UNet(
                            layers_description['UNet'], padding_mode)
        else:
            raise ValueError(f'Model name "{model_name}" is not implemented.')

        
        
    def forward(self, input_, mask=None):
        """ Method to call the network and evaluate the model.
        
        ----------
        ARGUMENTS
        
            input_: tensor with format (N, C_in, T_in, H, W)
                N: number of batch
                C_in: input channels (pressure, velocity, masks, etc)
                T_in: input frames
                H: frame height
                W: frame width
        
        ----------
        RETURNS
        
            out: prediction with format (N, C_out, T_out, H, W)
                N: number of batch
                C_out: output channels (pressure, velocity, etc)
                T_out: output frames
                H: frame height
                W: frame width
        
        """
        
        # input format
        bsz, csz, tsz = input_.shape[:3]


        dim_shape = ()
        for size in input_.shape[3:]:
            dim_shape = dim_shape + (size,)

        data_channels = torch.arange(self.num_input_channels, dtype=torch.long, device=input_.device) 
        output_shape = (bsz, self.num_output_frames, self.num_output_channels) + dim_shape

        # mask are additional input channels where physical values are not
        # defined (e.g. obstacle) thus must be treated separately 
        if self.mask_channel is not None:
            # all channel indices excluding the mask channel
            data_channels = data_channels[data_channels!=self.mask_channel]
        
        input_ = input_.index_select(dim=1, index=data_channels).contiguous() 

        std_first_frames = []
        mean_first_frames = []
        gaussian_norm_channels = []
        gaussian_norm_counter = 0
        for channel, norm in enumerate(self.normalization):
            if norm == 'gaussian': 
                gaussian_norm_channels.append(gaussian_norm_counter)
                # calculate the std of the first frame (timestep) of every
                # element in the input scene
                first_frame = input_[:,channel,0].clone().unsqueeze(1).unsqueeze(1)
                first_frame_mask = None
                if mask is not None and self.mask_channel is not None:
                    # create the mask 
                    first_frame_mask = mask

                std_first_frames.append(
                    pulsenetlib.transforms.std_first_frame(first_frame,
                        first_frame_mask)
                )  
                mean_first_frames.append(
                    pulsenetlib.transforms.mean_first_frame(first_frame,
                        first_frame_mask)
                )
                zero_std = torch.isclose(std_first_frames[gaussian_norm_counter], torch.zeros_like(std_first_frames[gaussian_norm_counter]))
                if torch.any(zero_std):
                    std_first_frames[gaussian_norm_counter] = std_first_frames[gaussian_norm_counter].clone()
                    std_first_frames[gaussian_norm_counter][zero_std] = torch.ones_like(std_first_frames[gaussian_norm_counter])[zero_std]
        
                input_mask = None 
                if mask is not None and self.mask_channel is not None:
                    # create the input mask (same shape as input_) 
                    input_mask = torch.zeros_like(input_, dtype=torch.bool, device=input_.device)
                    input_mask[:,:self.mask_channel] = mask
                    input_mask = input_mask[:,0]


                # remove the average, considering the first frame
                input_[:,channel] = pulsenetlib.transforms.add_frame_(
                    input_[:,channel], mean_first_frames[gaussian_norm_counter], self.avg_remove[gaussian_norm_counter], 
                        input_mask)
                # normalize by std, considering the first frame
                input_[:,channel] = pulsenetlib.transforms.divide_frame_(
                    input_[:,channel], std_first_frames[gaussian_norm_counter], self.std_norm[gaussian_norm_counter],
                        input_mask)

                gaussian_norm_counter += 1
        

        # Input dim = (batch, channels, time, ...) 
        # channels and time dimensions are combined in order
        # to perform 2D (BSZ, C, H, W) or
        # 3D (BSZ, C, D, H, W) convolutions.
        # Data is presented to the neural network as:
        #
        #     channel 0: frame 0, channel 1: frame 0, ....
        #       channel 0: frame 1, channel 1, frame 1, ....
        #         channel 0: frame 2, channel 1, frame 2, ....
        #           ...
        #
        # the transpose operation is necessary so they are sorted
        # by time and not by channel
        input_shape = (bsz,self.num_pages) + dim_shape
        x = input_.transpose(1,2).contiguous().view(input_shape)
        
        # concatenate obstacle mask at the end of input
        if mask is not None and self.mask_channel is not None:
            x[~mask[:,0,0][:,None].expand(input_shape)] = 0
            x = torch.cat((x, mask[:,0,0][:,None]), dim=1).contiguous()

        y = self.net(x)
        output_mask = None 
        if mask is not None:
            output_mask = mask[:,0] 

        # returning to original data format
        # First as (BSZ, T, C, ...), later transposed to (BSZ, C, T, ...)
        y = y.view(output_shape).transpose(1,2)
        
        for channel in gaussian_norm_channels:
            # going back to physical quantity performing the opposite
            # operations
            y[:,channel] = pulsenetlib.transforms.multiply_frame_(
                y[:,channel], std_first_frames[channel], self.std_norm[channel],
                output_mask)
            y[:,channel] = pulsenetlib.transforms.add_frame_(
                y[:,channel], mean_first_frames[channel], -self.avg_remove[channel],
                output_mask)

        # set to zero inside obstacles
        if mask is not None:
           output_shape = y.shape
           y[~mask.expand(output_shape)] = 0

        return y
