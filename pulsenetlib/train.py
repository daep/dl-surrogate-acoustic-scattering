#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Training code. 

    To run, a configuration file is needed 
    python train.py --trainingConf configuration.xml

    Add --preproc option to do preprocessing.
"""

# native modules
import os
import sys
import datetime
import copy
import functools
from collections import OrderedDict
import logging

# third party modules
os.environ['OPENBLAS_NUM_THREADS'] = '1'
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
import torchvision.transforms
import pytorch_lightning as pl

# local modules
import pulsenetlib

log = logging.getLogger(__name__)

def cli_main(args=None, configuration=None):

    # print training header
    log.info('\n\n' + 72*'-' + '\n')
    log.info('Launching training')
    log.info(f'{datetime.datetime.now()} \n')

    #****************** Arguments and inputs ***********************************
    # parsing execution arguments
    if args is None:
        args = sys.argv[1:]
    arguments = pulsenetlib.arguments.parse_arguments(args)

    # reading configuration file. script arguments overwrite the
    # values on configuration file
    training_configuration, resume, verbose, print_training,\
    initial_checkpoint = \
        pulsenetlib.arguments.read_arguments(arguments,
            conf_resume=configuration)
    
    if torch.cuda.is_available():
        log.info(f'Active GPU index: {torch.cuda.current_device()}')    
        number_gpus = arguments.gpus
        device=torch.device('cuda:0')
    else:
        log.info('\nNot using GPU.')
        number_gpus = 0
        device=torch.device('cpu')

    if 'seed' in training_configuration: 
        pl.seed_everything(training_configuration['seed'])
    else:
        pl.seed_everything(12)
    
    preprocess = arguments.preproc
    overwrite_preprocess = arguments.overwritePreproc
    
    # creating destination folder, if is not there
    model_path = os.path.realpath(training_configuration['modelDir'])
    if not os.path.isdir(training_configuration['modelDir']):
        log.info(f'Creating destination folder:\n'
              f'{model_path}\n')
        os.makedirs(model_path, exist_ok=True)
        
    # split the model configuration as an unique dictionary and 
    # remove it from training_configuration
    model_configuration = copy.deepcopy(
            training_configuration['modelParam'])
    del training_configuration['modelParam']
    
    version = training_configuration['version']
    logger_kwargs, version, log_dir, ckpt_dir = \
         pulsenetlib.logger.HandleVersion(model_path,version,resume)
    training_configuration['version'] = version
    
    if resume:
        log.info('\n{:^72}\n'.format('#RESTARTING#TRAINING#').\
              replace(' ','-').replace('#',' '))
        
        # load configuration files from checkpoint
        temp_training_configuration, temp_model_configuration = \
            pulsenetlib.datatransfer.resume_configuration_files(log_dir)
        
        # overwriting model and its configuration from checkpoint
        model_configuration.update(temp_model_configuration)
        
        # overwritting the parameters passed as arguments, if any
        training_configuration, resume, verbose, print_training, \
         initial_checkpoint = \
            pulsenetlib.arguments.read_arguments(
                arguments, training_configuration)

    # save configuration files to log folder
    pulsenetlib.datatransfer.save_configuration_files(
        log_dir, training_configuration, model_configuration)
    
    #************************* DataModule **************************************
    channels = model_configuration['channels']

    data_fmt = 'vtk'
    if 'rawFormat' in training_configuration:
        data_fmt = training_configuration['rawFormat']

    log.info(f'Data format = {data_fmt}')

    if data_fmt == 'vtk':
        rawloader = pulsenetlib.preprocess_vtk.VTKLoader(channels)
    else:
        data_fmt_options = ['vtk']
        raise ValueError(f'data format ("{data_fmt}") must be one of these options:\n'
                         f'{data_fmt_options}')

    num_input_channels = len(channels)
    output_channels = channels
    if 'outputChannels' in  model_configuration:
        output_channels = model_configuration['outputChannels']
        for channel in output_channels:
            if channel not in channels:
                raise ValueError(f"Output channel name ('{channel}') "
                                 f"not found among available "
                                 f"channels ({channels}).")  

    num_output_channels = len(output_channels)
    input_frames = model_configuration['numInputFrames']
    output_frames = model_configuration['numOutputFrames']
    layers_description = model_configuration['layersDescription']
    normalization = 'gaussian'
    if 'normalization' in model_configuration:
        normalization = model_configuration['normalization']
    frame_step = training_configuration['frameStep']
    select_mode = training_configuration['selectMode']
    mask_name = model_configuration['mask']
    padding_mode = 'replicate'
    if 'paddingMode' in model_configuration:
        padding_mode = model_configuration['paddingMode']

    mask_channel = None
    if mask_name is not None:        
        for index, field in enumerate(channels):
            if field == mask_name:
                mask_channel = index
        if mask_channel is None:
            raise ValueError(f"mask name ('{mask_name}') "
                             f"not found among available "
                             f"channels ({channels}).")  
    
    # selecting the data augmentation operations, performed just
    # after reading the data files
    preprocess_transform = []
    online_transform = []
    data_augmentation_preprocess = None
    data_augmentation_online = None
    if training_configuration['flipData']['activate']:
        if training_configuration['flipData']['phase'] == 'preprocess':
            log.info('Flip data augmentation (preprocess)')
            preprocess_transform.append(pulsenetlib.transforms.RandomFlip())
        if training_configuration['flipData']['phase'] == 'online':
            log.info('Flip data augmentation (online)')
            online_transform.append(pulsenetlib.transforms.RandomFlip())
    
    data_augmentation_preprocess = \
        torchvision.transforms.Compose(preprocess_transform)
    data_augmentation_online = torchvision.transforms.Compose(online_transform)
    
    num_preproc_threads = int(os.environ.get('SLURM_CPUS_PER_TASK',default=1))
    num_workers = int(os.environ.get('SLURM_CPUS_PER_TASK',default=0))

    if num_workers == 0 and 'numWorkers' in training_configuration:
        num_workers = training_configuration['numWorkers']
    if num_workers == 1 and 'numWorkers' in training_configuration:
        num_preproc_threads = training_configuration['numWorkers']

    labels = {'train': 'training',
              'val': 'validation'}

    train_val_split = None
    if 'trainValSplit' in training_configuration:
        train_val_split = training_configuration['trainValSplit'] 
        labels = {'trainVal': 'training_validation'}

    if isinstance(training_configuration['maxDatapoints'], int):
        max_datapoints = \
            {'train': training_configuration['maxDatapoints'],
             'val': training_configuration['maxDatapoints']}
    elif isinstance(training_configuration['maxDatapoints'], list):
        max_datapoints = \
            {'train': training_configuration['maxDatapoints'][0],
             'val': training_configuration['maxDatapoints'][1]}
    elif isinstance(training_configuration['maxDatapoints'], dict):
        max_datapoints = \
            {'train':
                training_configuration['maxDatapoints']['training'],
            'val': 
                training_configuration['maxDatapoints']['validation']}
    else:
        raise ValueError(f'trainingConfiguration/maxDatapoints'
                         f' must be of type int, list or dict.')

    max_datapoints['trainVal'] = max_datapoints['train']
        
    data_module_kwargs = {
                        'save_dir' : training_configuration['dataPath'],
                        'data_format' : training_configuration['formatPT'],
                        'labels' : labels,
                        'batch_size' : training_configuration['batchSize'],
                        'num_workers' : num_workers,
                        'shuffle' : training_configuration['shuffleTraining'],
                        'channels' : channels,
                        'frames' : (input_frames, output_frames, frame_step),
                        'train_val_split' : train_val_split,
                        'max_datapoints' : max_datapoints,
                        'rawloader' : rawloader,
                        'data_augmentation_preprocess' : data_augmentation_preprocess,
                        'data_augmentation_online' : data_augmentation_online,
                        'select_mode' : select_mode,
    }
    if data_fmt == 'vtk':
        data_module = pulsenetlib.datamodule.PulseNetPalabosDataModule(**data_module_kwargs)
    else:
        ValueError(f'data_fmt {data_fmt} not supported')

    if preprocess:
        if data_fmt == 'vtk':
            raw_format = training_configuration['formatVTK']
            data_module.preprocess(
                            'train',
                            raw_save_dir=training_configuration['dataPathRaw'],
                            raw_format=raw_format,
                            num_preproc_threads=num_preproc_threads,
                            overwrite_preprocess=overwrite_preprocess
                    )
    
    data_module.setup('fit')
        
    #************************ Data transforms **********************************
    scalar_add = torch.tensor(model_configuration['scalarAdd'])

    transforms= []
    transforms.append(
        functools.partial(pulsenetlib.transforms.offset_,
                          scalars=scalar_add)
    )
                                  
    scalar_mul = torch.tensor(model_configuration['scalarMul'])
    transforms.append(
        functools.partial(pulsenetlib.transforms.multiply_,
                          scalars=scalar_mul)
    )
    
    std_norm = torch.tensor(model_configuration['stdNorm'])
    avg_remove = torch.tensor(model_configuration['avgRemove'])

    
    #************************* Create the model ********************************
    log.info('\n{:^72}\n'.format('#MODEL#')\
          .replace(' ','-').replace('#',' '))
    
    # create or load model
    net = pulsenetlib.model.PulseNet(num_input_channels, num_output_channels,
                                    input_frames, output_frames,
                                    std_norm, avg_remove,
                                    layers_description,
                                    model_name=model_configuration['model'],
                                    mask_channel=mask_channel,
                                    normalization=normalization,
                                    padding_mode=padding_mode,
                                 )

    # printing model
    log.info('Neural network:\n')
    log.info(net)

    # This can be put inside a callback
    # initialize network weights with Kaiming normal method (a.k.a MSRA)
    def init_weights(m):
        if type(m) == nn.Conv2d:
            if m.weight.requires_grad:
                torch.nn.init.kaiming_uniform_(m.weight)
            if m.bias.requires_grad:
                m.bias.data.fill_(0.0)

    if not resume:
        net.apply(init_weights)

    # This can be put inside a callback    
    log.info('\nLoaded/initiated weights')
    list_parameters = torch.cat(
        [p.view(-1) for p in net.parameters() if p.requires_grad])
    
    log.info('Trainable parameters statistics:\n')
    log.info('  number: {:}'.format(list_parameters.numel()))
    log.info('  AVG: {:+.3e}'.format(list_parameters.mean()))
    log.info('  STD: {:+.3e}'.format(list_parameters.std()))
        
    # clean variable
    del list_parameters
        
    #********************** Optimizer definition *******************************
    callbacks = []
    
    log.info('\n{:^72}\n'.format('#OPTIMIZER#').\
          replace(' ','-').replace('#',' '))
    
    lr = training_configuration['optimizer']['lr']
    wd = training_configuration['optimizer']['weight_decay']
    
    optimizer = torch.optim.Adam(
        net.parameters(), lr=lr, weight_decay=wd)

    for param_group in optimizer.param_groups:
        log.info('Initial Learning Rate of optimizer = {:}'.format(
            str(param_group['lr'])))

    #********************** Scheduler definition *******************************
    log.info('\n{:^72}\n'.format('#SCHEDULER#').\
          replace(' ','-').replace('#',' '))

    scheduler = None
    if 'type' in training_configuration['scheduler']:
        scheduler_type = training_configuration['scheduler']['type']

    if scheduler_type == 'None' or scheduler_type == 'none' or scheduler_type is None:
        scheduler = None
    elif scheduler_type == 'multiStepLR':
        scheduler = optim.lr_scheduler.MultiStepLR(
                optimizer,
                **training_configuration['scheduler']['multiStepLR'])
        scheduler_type = 'MultiStepLR'
        training_configuration['scheduler']['type'] =  scheduler_type
        training_configuration['scheduler'][scheduler_type] = \
            training_configuration['scheduler']['multiStepLR']
        del training_configuration['scheduler']['multiStepLR']
        pulsenetlib.datatransfer.save_configuration_files(
            log_dir, training_configuration, model_configuration)
    elif scheduler_type == 'reduceLROnPlateau':
        scheduler = optim.lr_scheduler.ReduceLROnPlateau(
            optimizer,
            **training_configuration['scheduler']['reduceLROnPlateau'])
        scheduler_type = 'ReduceLROnPlateau'
        training_configuration['scheduler']['type'] =  scheduler_type
        training_configuration['scheduler'][scheduler_type] = \
            training_configuration['scheduler']['reduceLROnPlateau']
        del training_configuration['scheduler']['reduceLROnPlateau']
        pulsenetlib.datatransfer.save_configuration_files(
            log_dir, training_configuration, model_configuration)
    else:
        func = getattr(optim.lr_scheduler, scheduler_type)
        scheduler = func(
            optimizer,
            **training_configuration['scheduler'][scheduler_type])
    
    # print scheduler parameters
    if scheduler is not None:
        log.info('Scheduler params:\n')
        for key, value in \
            training_configuration['scheduler'][scheduler_type].items():
            log.info('   {:} = {:}'.format(key,value))
    
    
    #************************ Define Criterion (Losses) ************************

    criterion = pulsenetlib.criterion.Criterion(
        output_channels, 
        model_configuration['L2Lambda'],
        model_configuration['GDL2Lambda'],
        normalize=False)

    #********************** Data Statistics ************************************
    log.info('\n{:^72}\n'.format('#DATA#STATISTICS#')\
          .replace(' ','-').replace('#',' '))
    
    for name, loader in zip(
                        ('Training','Validation'),
                        (
                            data_module.train_dataloader(),
                            data_module.val_dataloader()
                        )
                    ):
        log.info(f'{name}:\n'
              f'  batch size: {loader.batch_size:d}\n'
              f'  number of batches: {len(loader):d}\n')
       
    # calculating mean and std of the training data, complete
    # database
    data_mean, data_std = \
        pulsenetlib.math_tools.online_mean_and_std(
            data_module.train_dataloader(), transforms
        )
    
    log.info('Calculating first and second moments...\n')
    for index_channel, channel in enumerate(channels):
        log.info('  {:}:'.format(channel))
        log.info('    AVG [data, gradx, grady]: ' +
              '[{:+.3e}, {:+.3e}, {:+.3e}]'.format(
                *data_mean[index_channel,:]))
        log.info('    STD [data, gradx, grady]: ' +
              '[{:+.3e}, {:+.3e}, {:+.3e}]'.format(
                *data_std[index_channel,:]))
        log.info('\n')
    
    
    stats_frequency = min(
        [len(data_module.train_dataloader()),
         len(data_module.val_dataloader())]) // 3 
    if stats_frequency == 0: 
        stats_frequency += 1
 
    callbacks.append(
        pulsenetlib.callbacks.PrintStatistics(
            stats_frequency, verbose=verbose)
    )
    callbacks.append(
        pl.callbacks.LearningRateMonitor(logging_interval='epoch')
    )

    if print_training:
        callbacks.append(
            pulsenetlib.callbacks.SaveTrainingImage(
                channels=channels,
                channels_to_plot=training_configuration['channelsToPlot'],
                plot_list=training_configuration['slices'],
                freq_to_file=training_configuration['freqToFile'],
                loader=data_module.val_dataloader()
            )
        ) 
    

    trainer_kwargs = { 'gpus': number_gpus,
                      'enable_progress_bar' : False,
                      'weights_summary' : None,
                      'default_root_dir': log_dir}   

    if 'maxEpochs' in training_configuration and 'maxSteps' in training_configuration:
        raise ValueError('only maxEpochs or maxSteps should be given in training_configuration (mutually exclusive)')
    elif 'maxSteps' in training_configuration:
        trainer_kwargs['max_steps'] = training_configuration['maxSteps']
    elif 'maxEpochs' in training_configuration: 
        trainer_kwargs['max_epochs'] = training_configuration['maxEpochs']
    else:
        raise ValueError('maxEpochs or maxSteps should be given in training_configuration')
    
    if number_gpus > 1:
        trainer_kwargs.update({'accelerator': 'ddp'})

    # when using a list of loggers, lightning automatically concatenates all
    # `name` and `version`, for usage in model_checkpoint callback.
    # To prevent this, we force all the loggers to have the same version and
    # name in the model_checkpoint callback, and we pass a custom checkpoint
    # path to the ModelCheckpoint callback.
    logger_name = training_configuration['logger']

    logger = []
    if 'np' in logger_name:
        log.info('Using disk numpy logger\n')
        logger.append(
            pulsenetlib.logger.DiskLogger(
                resume=resume, **logger_kwargs)
        )
    if 'tb' in logger_name:
        log.info('Using tensorboard logger\n')
        logger.append(
            pl.loggers.TensorBoardLogger(
                **logger_kwargs)
    )
    if not logger_name:
        raise ValueError(f'logger list ({logger_name}) cannot be empty') 

    trainer_kwargs.update({'logger': logger})

    extended_name = ''
    if 'ckptName' in training_configuration:
        extended_name = training_configuration['ckptName']
    if extended_name is None:
        extended_name = ''
    
    save_model_freq = 1
    if 'saveModelFreq' in training_configuration:
        save_model_freq = training_configuration['saveModelFreq']

    checkpoint_callback_top = pl.callbacks.ModelCheckpoint(
        dirpath=ckpt_dir,
        filename='best_{epoch:06d}' + extended_name,
        monitor='val_loss',
        save_top_k=training_configuration['saveTopK'],
        save_last=False,
        verbose=verbose
    )
    callbacks.append(
        checkpoint_callback_top
    )
    checkpoint_callback_freq = pl.callbacks.ModelCheckpoint(
        dirpath=ckpt_dir,
        filename='{epoch:06d}' + extended_name,
        monitor='val_loss',
        save_top_k=-1,
        every_n_epochs=save_model_freq,
        save_last=True,
        verbose=verbose
    )
    checkpoint_callback_freq.CHECKPOINT_NAME_LAST = "last" + extended_name
    callbacks.append(
        checkpoint_callback_freq
    )
    
    callbacks.append(
        pulsenetlib.callbacks.HandleCheckpointSaving(training_configuration['saveTopK'])
    )


    if resume:
        path_to_ckpt = None
        if 'pathToCkpt' in training_configuration:
            path_to_ckpt = training_configuration['pathToCkpt']
        if path_to_ckpt is None:
            path_to_ckpt = pulsenetlib.datatransfer.load_pl_ckpt(ckpt_dir)

        trainer_kwargs.update(
            {'resume_from_checkpoint': path_to_ckpt }
        )

    
    pulsenet = pulsenetlib.lightning_module.TrainPulseNet(
                   model=net,
                   optimizer=optimizer,
                   scheduler=scheduler,
                   criterion=criterion,
                   transforms=transforms)

    if not resume and initial_checkpoint is not None:
        log.info(f'Set initial condition to checkpoint: {initial_checkpoint}')
        pulsenet = pulsenet.load_from_checkpoint(initial_checkpoint,
            model=net,
            optimizer=optimizer,
            scheduler=scheduler,
            criterion=criterion,
            transforms=transforms)
    
    trainer_kwargs.update(
        {'callbacks': callbacks}
    )
    
    trainer = pl.Trainer(**trainer_kwargs,
                         deterministic=False)
    trainer.fit(model=pulsenet,
                datamodule=data_module)


if __name__ == "__main__":
    os.environ['OPENBLAS_NUM_THREADS'] = '1'
    os.environ['MKL_THREADING_LAYER'] = 'GNU'

    cli_main()


