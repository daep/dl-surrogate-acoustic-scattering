#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
    Lightning Module
"""

# native modules
import inspect
import logging
from collections import Counter
from enum import Enum
from typing import Union

# third-party modules
import torch
import pytorch_lightning as pl
from pytorch_lightning.loggers.tensorboard import TensorBoardLogger
from pytorch_lightning.utilities.exceptions import MisconfigurationException

# local modules
#none

log = logging.getLogger(__name__)

class TrainPulseNet(pl.LightningModule):
    def __init__(self,
            model, optimizer, scheduler,
            criterion,
            transforms=[]):

        super().__init__()
        self.model = model
        self.optimizer = optimizer
        self.scheduler = scheduler
        
        self.criterion = criterion

        self.transforms = transforms

        if self.optimizer is not None: 
            self.hparams.optimizer = optimizer.__class__.__name__
            for i, group in enumerate(self.optimizer.param_groups):
                self.hparams['{0}'.format(i)] = {}
                for key in sorted(group.keys()):
                    if key != 'params':
                        self.hparams['{0}'.format(i)][key] = group[key]
        
        if self.scheduler is not None:
            self.hparams.scheduler = scheduler.__class__.__name__

            scheduler_args = inspect.signature(
                self.scheduler.__init__).parameters.copy()
        
            for name in scheduler_args.keys():
                if name != 'optimizer' and name != 'min_lr':
                    if isinstance(self.scheduler.__getattribute__(name), Counter):
                        self.hparams[name] = list(self.scheduler.__getattribute__(name)) 
                    else:
                        self.hparams[name] = self.scheduler.__getattribute__(name) 
        
        self.mask_channel = model.mask_channel

        self.num_output_channel = model.num_output_channels
        

        log.info('\n{:^72}\n'.format('#DEFINING#TRAINING#CRITERIA#')\
              .replace(' ','-').replace('#',' '))
        self.criterion.summary() 

    def on_fit_start(self):
        for loss_name, loss_lambda in zip(self.criterion.get_loss_names(), self.criterion.get_loss_lambdas().tolist()):
            self.hparams[loss_name] = loss_lambda

        self.hparams["batch_size"] = 0.0
        for logger in self.loggers:
            if isinstance(logger, TensorBoardLogger):
                logger.log_hyperparams(self.hparams)

    def _step(self, batch, batch_idx, validation=False):
        """ Operations at the step that are common
        to training and validation

        """
        data, target = batch

        # data transforms (e.g. uniform shift as in LBM background density is 1)
        for transform in self.transforms:
            data = transform(data)
            target = transform(target)

        if self.num_output_channel is not None: 
            # remove mask from target for loss calculation
            data_channels = torch.arange(
                self.num_output_channel, dtype=torch.long, device=data.device
                ) 
            target = target.index_select(dim=1, index=data_channels)
            
            past = data.index_select(dim=1, index=data_channels)
        
        
        mask = None
        if self.mask_channel is not None:
            # create the mask (with 1 frame and 1 channel)
            bsz = data.shape[0]
            mask_shape = (bsz, 1, 1)
            for size in data.shape[3:]:
                mask_shape = mask_shape + (size,)
            mask = ~data[:,self.mask_channel,0].to(torch.bool).view(mask_shape)

        # *extremely important* Zero obstacle pixels in target:
        mask_output = None 
        if mask is not None:
            target_shape = target[:,:,0][:,:,None].shape
            target[:,:,0][:,:,None][~mask.expand(target_shape)] = 0
            mask_output = mask.expand(target_shape).contiguous()
        
        out = self.model(data.clone(), mask=mask)

        # forcing masked elements to nan, so mask information is known at 
        # the plotting time
        target_plot = target[:,:,0][:,:,None].clone()
        if mask is not None:
            target[:,:,0][:,:,None][~mask_output] = 0
            target_plot[~mask_output] = float('nan')
        
        detailed_supervised_loss = self.criterion(out, target[:,:,0][:,:,None], mask=mask_output)
        
        # forcing masked elements to nan, so mask information is known at 
        # the plotting time
        out_plot = out.clone()
        if mask is not None:
            out_plot[~mask_output] = float('nan')
        
        # used to visualize predictions
        self.last_prediction = out_plot
        self.last_target = target_plot

        # scalar loss
        loss = torch.mean(detailed_supervised_loss) 

        # detailed losses
        loss_names = self.criterion.get_loss_names() 
        lambdas = self.criterion.get_loss_lambdas().to(data.device)
        pad = (0, len(loss_names) - detailed_supervised_loss.size(0))
        lambdas[lambdas==0] = 1 
        dict_metrics = dict(
            zip(loss_names, detailed_supervised_loss)
        )
        
        return loss, dict_metrics
    
    def training_step(self, batch, batch_idx):
        loss, dict_metrics = self._step(batch, batch_idx, False)
        self.log("batch_size", batch[0].shape[0])
        return {'loss': loss, 'metrics': dict_metrics}


    def validation_step(self, batch, batch_idx):
        loss, dict_metrics = self._step(batch, batch_idx, True)
        return {'val_loss': loss, 'metrics': dict_metrics}

    def training_epoch_end(self, training_step_outputs):       
        self.log(
            'loss',
            torch.stack([output['loss'] 
                for output in training_step_outputs]).mean(),
            on_epoch=True
            )
        for key in training_step_outputs[0]['metrics'].keys():
            self.log(
                f'training/{key}',
                torch.stack([output['metrics'][key] 
                    for output in training_step_outputs]).mean(),
                on_epoch=True
                )

    def validation_epoch_end(self, validation_step_outputs):
        self.log(
            'val_loss',
            torch.stack([output['val_loss']
                for output in validation_step_outputs]).mean(),
            on_epoch=True
            )
        for key in validation_step_outputs[0]['metrics'].keys():
            self.log(
                f'validation/{key}',
                torch.stack([output['metrics'][key]
                    for output in validation_step_outputs]).mean(),
                on_epoch=True
                )


    def teardown(self, stage):
        pass

    def configure_optimizers(self):
        optimizer = self.optimizer
        if self.scheduler is not None:
            scheduler = {
                    'scheduler': self.scheduler,
                    'monitor': 'val_loss',
                    'interval': 'epoch',
                    'frequency': 1
                }
            return [optimizer], [scheduler] 
        else:
            return optimizer

