#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Lightning datamodule
"""

# native modules
import sys
import os
from typing import Callable, Mapping, Optional, Sequence, Tuple, Union

# third-party modules
import torch
import pytorch_lightning as pl

# local modules
import pulsenetlib.transforms

class PulseNetPalabosDataModule(pl.LightningDataModule):
    def __init__(
        self,
        save_dir: str,
        data_format: str,
        labels: Mapping,
        batch_size: int,
        num_workers: int,
        channels: Sequence[str],
        frames: Tuple[int, int, int] ,
        max_datapoints: int,
        rawloader: Callable,
        shuffle: bool = False,
        train_val_split : float = None, 
        data_augmentation_preprocess: Optional[Callable] = None,
        data_augmentation_online: Optional[Callable] = None,
        select_mode: Optional[str] = 'start',
        raw_save_dir = None,
        raw_format = None, 
        num_preproc_threads = None,
        overwrite_preprocess = None,
        drop_last = True,
    ):
        super().__init__()
        self.save_dir = save_dir
        self.data_format = data_format
        self.labels = labels

        self.batch_size = batch_size
        self.num_workers = num_workers
        self.shuffle = shuffle

        self.channels = channels 
        self.number_channels = len(channels)

        self.input_frames = frames[0] 
        self.output_frames = frames[1] 
        self.frame_step = frames[2]

        self.train_val_split = train_val_split

        self.max_datapoints = max_datapoints 
        self.rawloader = rawloader
        
        self.data_augmentation_preprocess = data_augmentation_preprocess
        self.data_augmentation_online = data_augmentation_online
        self.select_mode = select_mode

        self.raw_save_dir = raw_save_dir
        self.raw_format = raw_format
        self.num_preproc_threads = num_preproc_threads
        self.overwrite_preprocess = overwrite_preprocess

        self.drop_last = drop_last

    def preprocess(self,
            preprocess_stage, 
            raw_save_dir,
            raw_format,
            overwrite_preprocess,
            num_preproc_threads=1):
        """ Perfoming preprocess of raw files

        Group the frames into input + output, saves data in pytorch
        format. Finishes run at end.
               
        If a max_datapoints is defined (not zero), the number of 
        validation datapoints is fixed as a quater of that value.
        
        More details in pulsenetlib.dataset.PulseNetDataset

        """

        if preprocess_stage == 'train':
            if 'train' in self.labels and 'val' in self.labels and \
                self.labels['train'] is not None and self.labels['val'] is not None:
                for label in ['train', 'val']:
                    path_data_format = os.path.join(self.save_dir,
                                                    self.labels[label],
                                                    self.data_format)
                    path_raw_format = os.path.join(raw_save_dir,
                                                   self.labels[label],
                                                   raw_format)
        
                    pulsenetlib.dataset.PulseNetDataset(
                                path_data_format,
                                (self.input_frames, self.output_frames, self.frame_step),
                                self.channels,
                                self.max_datapoints[label], 
                                self.labels[label],
                                data_augmentation_preprocess=self.data_augmentation_preprocess,
                                preprocess=True,
                                path_raw_format=path_raw_format,
                                rawloader=self.rawloader.load,
                                num_preproc_threads=num_preproc_threads,
                                select_mode=self.select_mode,
                                overwrite_preprocess=overwrite_preprocess)
            
            elif 'trainVal' in self.labels and self.labels['trainVal'] is not None:
                label = 'trainVal'
                path_data_format = os.path.join(self.save_dir,
                                                self.labels[label],
                                                self.data_format)
                path_raw_format = os.path.join(raw_save_dir,
                                               self.labels[label],
                                               raw_format)
        
                pulsenetlib.dataset.PulseNetDataset(
                            path_data_format,
                            (self.input_frames, self.output_frames, self.frame_step),
                            self.channels,
                            self.max_datapoints[label], 
                            self.labels[label],
                            data_augmentation_preprocess=self.data_augmentation_preprocess,
                            preprocess=True,
                            path_raw_format=path_raw_format,
                            rawloader=self.rawloader.load,
                            num_preproc_threads=num_preproc_threads,
                            select_mode=self.select_mode,
                            overwrite_preprocess=overwrite_preprocess)
            else:
                raise ValueError(f'labels should have either "train" and "val"\n'
                                 f'or "trainVal" keys.')
        
            sys.exit()
        
        elif preprocess_stage == 'test': 
                label = 'test'
                path_data_format = os.path.join(self.save_dir,
                                                self.labels[label],
                                                self.data_format)
                path_raw_format = os.path.join(raw_save_dir,
                                               self.labels[label],
                                               raw_format)
        
                pulsenetlib.dataset.PulseNetDataset(
                            path_data_format,
                            (self.input_frames, self.output_frames, self.frame_step),
                            self.channels,
                            self.max_datapoints[label], 
                            self.labels[label],
                            data_augmentation_preprocess=self.data_augmentation_preprocess,
                            preprocess=True,
                            path_raw_format=path_raw_format,
                            rawloader=self.rawloader.load,
                            num_preproc_threads=num_preproc_threads,
                            select_mode=self.select_mode,
                            overwrite_preprocess=overwrite_preprocess)

        else:
            raise ValueError('preprocess_stage should be "fit" or "test"')
    
            
    def setup(self, stage=None):
        if stage == 'fit' or stage is None:
            if 'train' in self.labels and 'val' in self.labels and \
                self.labels['train'] is not None and self.labels['val'] is not None:
                path_data_format = os.path.join(self.save_dir,
                                                self.labels['train'],
                                                self.data_format)
                self.train_database = pulsenetlib.dataset.PulseNetDataset(
                                path_data_format,
                                (self.input_frames, self.output_frames, self.frame_step),
                                self.channels,
                                max_data_points=self.max_datapoints['train'],
                                label=self.labels['train'],
                                data_augmentation_online=self.data_augmentation_online)
                path_data_format = os.path.join(self.save_dir,
                                                self.labels['val'],
                                                self.data_format)
                self.validation_database = pulsenetlib.dataset.PulseNetDataset(
                                path_data_format,
                                (self.input_frames, self.output_frames, self.frame_step),
                                self.channels,
                                max_data_points=self.max_datapoints['val'],
                                label=self.labels['val'],
                                data_augmentation_online=None)
            elif 'trainVal' in self.labels and self.labels['trainVal'] is not None:
                if self.train_val_split is None:
                    raise ValueError(f'Train/Validation split cannot be None when setting\n'
                                     f' "train_val_split" to None.')
                if self.train_val_split < 0. or self.train_val_split > 1.:
                    raise ValueError(f'train_val_split must be in [0.,1.] \n'
                                     f'Found ({self.train_val_split})')
                path_data_format = os.path.join(self.save_dir,
                                                self.labels['trainVal'],
                                                self.data_format)
                database = pulsenetlib.dataset.PulseNetDataset(
                                path_data_format,
                                (self.input_frames, self.output_frames, self.frame_step),
                                self.channels,
                                max_data_points=self.max_datapoints['train'],
                                label=self.labels['trainVal'],
                                data_augmentation_online=self.data_augmentation_online)
                len_database = len(database)
                train_split = int(len_database*self.train_val_split)
                val_split = int(len_database) - train_split
                self.train_database, self.validation_database = \
                    torch.utils.data.random_split(database, [train_split, val_split])

            else:
                raise ValueError(f'labels should have either "train" and "val"\n'
                                 f'or "trainVal"')
        elif stage == 'test':
            path_data_format = os.path.join(self.save_dir,
                                            self.labels['test'],
                                            self.data_format)
            self.test_database = pulsenetlib.dataset.PulseNetDataset(
                            path_data_format,
                            (self.input_frames, self.output_frames, self.frame_step),
                            self.channels,
                            max_data_points=self.max_datapoints['test'],
                            label=self.labels['test'],
                            data_augmentation_online=self.data_augmentation_online)
        else:
            raise NotImplementedError(f'stage {stage} not implemented')


    def train_dataloader(self):
        return torch.utils.data.DataLoader(
                    self.train_database,
                    batch_size=self.batch_size,
                    num_workers=self.num_workers,
                    shuffle=self.shuffle,
                    pin_memory=True,
                    drop_last=self.drop_last)
    

    def val_dataloader(self):
        return torch.utils.data.DataLoader(
                    self.validation_database,
                    batch_size=self.batch_size,
                    num_workers=self.num_workers,
                    shuffle=False,
                    pin_memory=True,
                    drop_last=self.drop_last)
    
    def test_dataloader(self):
        return torch.utils.data.DataLoader(
                    self.test_database,
                    batch_size=self.batch_size,
                    num_workers=self.num_workers,
                    shuffle=False,
                    pin_memory=True,
                    drop_last=False)


class PulseNetAVBPDataModule(pl.LightningDataModule):
    def __init__(
        self,
        save_dir: str,
        data_format: str,
        labels: Mapping,
        batch_size: int,
        num_workers: int,
        channels: Sequence[str],
        frames: Tuple[int, int, int] ,
        max_datapoints: int,
        rawloader: Callable,
        shuffle: bool = False,
        train_val_split : float = None, 
        data_augmentation_preprocess: Optional[Callable] = None,
        data_augmentation_online: Optional[Callable] = None,
        select_mode: Optional[str] = 'start',
        num_preproc_threads = None,
        overwrite_preprocess = None,
        drop_last = True,
    ):
        super().__init__()
        self.save_dir = save_dir
        self.data_format = data_format
        self.labels = labels

        self.batch_size = batch_size
        self.num_workers = num_workers
        self.shuffle = shuffle

        self.channels = channels 
        self.number_channels = len(channels)

        self.input_frames = frames[0] 
        self.output_frames = frames[1] 
        self.frame_step = frames[2]

        self.train_val_split = train_val_split

        self.max_datapoints = max_datapoints 
        self.rawloader = rawloader
        
        self.data_augmentation_preprocess = data_augmentation_preprocess
        self.data_augmentation_online = data_augmentation_online
        self.select_mode = select_mode

        self.num_preproc_threads = num_preproc_threads
        self.overwrite_preprocess = overwrite_preprocess

        self.drop_last = drop_last

    def preprocess(self,
            raw_save_dir : str,
            mesh_raw_format : str,
            solut_raw_format : str,
            init_raw_format: Optional[str] = None,
            initchoices_raw_format: Optional[str] = None,
            avg_raw_format: Optional[str] = None,
            solutbound_raw_format: Optional[str] = None,
            asciibound_raw_format: Optional[str] = None,
            overwrite_preprocess: Optional[bool] = True,
            num_preproc_threads: Optional[int] = 1):
        """ Perfoming preprocess of raw files

        Group the frames into input + output, saves data in pytorch
        format. Finishes run at end.
               
        If a max_datapoints is defined (not zero), the number of 
        validation datapoints is fixed as a quater of that value.
        
        More details in pulsenetlib.dataset.PulseNetDataset

        """

        for label in self.labels:
            path_data = os.path.join(self.save_dir,
                                     self.labels[label],
                                     self.data_format)
            path_mesh = os.path.join(raw_save_dir,
                                     self.labels[label],
                                     mesh_raw_format)
            path_solut = os.path.join(raw_save_dir,
                                      self.labels[label],
                                      solut_raw_format)
            path_init = None
            if init_raw_format is not None:
                path_init = os.path.join(raw_save_dir,
                                          self.labels[label],
                                          init_raw_format)
            path_initchoices = None
            if initchoices_raw_format is not None:
                path_initchoices = os.path.join(raw_save_dir,
                                          self.labels[label],
                                          initchoices_raw_format)
            path_solutbound = None
            if solutbound_raw_format is not None:
                path_solutbound = os.path.join(raw_save_dir,
                                          self.labels[label],
                                          solutbound_raw_format)
            path_asciibound = None
            if asciibound_raw_format is not None:
                path_asciibound = os.path.join(raw_save_dir,
                                          self.labels[label],
                                          asciibound_raw_format)
        
            pulsenetlib.dataset_mesh.PulseNetMeshDataset(
                        path_data,
                        (self.input_frames, self.output_frames, self.frame_step),
                        self.channels,
                        self.max_datapoints[label], 
                        self.labels[label],
                        data_augmentation_preprocess=self.data_augmentation_preprocess,
                        preprocess=True,
                        mesh_raw_format=path_mesh,
                        solut_raw_format=path_solut,
                        init_raw_format=path_init,
                        initchoices_raw_format=path_initchoices,
                        solutbound_raw_format=path_solutbound,
                        asciibound_raw_format=path_asciibound,
                        rawloader=self.rawloader.load,
                        num_preproc_threads=num_preproc_threads,
                        select_mode=self.select_mode,
                        overwrite_preprocess=overwrite_preprocess)
        sys.exit()

    def setup(self, stage):
            if 'trainVal' in self.labels and self.labels['trainVal'] is not None:
                if self.train_val_split is None:
                    raise ValueError(f'Train/Validation split cannot be None when setting\n'
                                     f' "train_val_split" to None.')
                if self.train_val_split < 0. or self.train_val_split > 1.:
                    raise ValueError(f'train_val_split must be in [0.,1.] \n'
                                     f'Found ({self.train_val_split})')
                path_data_format = os.path.join(self.save_dir,
                                                self.labels['trainVal'],
                                                self.data_format)
                database = pulsenetlib.dataset.PulseNetDataset(
                                path_data_format,
                                (self.input_frames, self.output_frames, self.frame_step),
                                self.channels,
                                max_data_points=self.max_datapoints['train'],
                                label=self.labels['trainVal'],
                                data_augmentation_online=self.data_augmentation_online)
                len_database = len(database)
                train_split = int(len_database*self.train_val_split)
                val_split = int(len_database) - train_split
                self.training_database, self.validation_database = \
                    torch.utils.data.random_split(database, [train_split, val_split])

            else:
                for label in self.labels:
                    path_data_format = os.path.join(self.save_dir,
                                                    self.labels[label],
                                                    self.data_format)
                    data_augmentation = None
                    if label == 'train':
                        data_augmentation = self.data_augmentation_online

                    database = pulsenetlib.dataset.PulseNetDataset(
                                    path_data_format,
                                    (self.input_frames, self.output_frames, self.frame_step),
                                    self.channels,
                                    max_data_points=self.max_datapoints[label],
                                    label=self.labels[label],
                                    data_augmentation_online=data_augmentation)
                    setattr(self, self.labels[label] + '_database', database)
            

    def train_dataloader(self):
        return torch.utils.data.DataLoader(
                    self.training_database,
                    batch_size=self.batch_size,
                    num_workers=self.num_workers,
                    shuffle=self.shuffle,
                    pin_memory=True,
                    drop_last=self.drop_last)
    

    def val_dataloader(self):
        return torch.utils.data.DataLoader(
                    self.validation_database,
                    batch_size=self.batch_size,
                    num_workers=self.num_workers,
                    shuffle=False,
                    pin_memory=True,
                    drop_last=self.drop_last)
    
    def test_dataloader(self):
        return torch.utils.data.DataLoader(
                    self.test_database,
                    batch_size=self.batch_size,
                    num_workers=self.num_workers,
                    shuffle=False,
                    pin_memory=True,
                    drop_last=False)
