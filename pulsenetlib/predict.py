#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Prediction code.
    
    The trained neural net (see train.py) is evaluated in an auto-regressive
    way to predict the full spatio-temporal evolution of physics quantities

    Usage: python predict.py --simConf configuration.xml
"""

# native modeules
import glob
import os
import argparse
import datetime

# third party modules
import yaml
import torch
import numpy as np
import numpy.ma as ma

import matplotlib
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.font_manager as font_manager
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm
from matplotlib.colors import ListedColormap
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable

# local modules
import pulsenetlib

def insert_colobar(fig, ax, im, bbox_to_anchor, loc, width='5%', height='100%',  **kwargs):
    axins = inset_axes(ax,
       width=width,
       height=height,
       loc=loc,
       bbox_to_anchor=bbox_to_anchor,
       bbox_transform=ax.transAxes,
       borderpad=0,
       )         
    return fig.colorbar(im, cax=axins, **kwargs)

def auto_fit_fontsize(text, width, height, fig=None, ax=None):
    '''Auto-decrease the fontsize of a text object.

    Args:
        text (matplotlib.text.Text)
        width (float): allowed width in data coordinates
        height (float): allowed height in data coordinates
    '''
    fig = fig or plt.gcf()
    ax = ax or plt.gca()

    # get text bounding box in figure coordinates
    renderer = fig.canvas.get_renderer()
    bbox_text = text.get_window_extent(renderer=renderer)

    # transform bounding box to data coordinates
    bbox_text = matplotlib.transforms.Bbox(ax.transData.inverted().transform(bbox_text))

    # evaluate fit and recursively decrease fontsize until text fits
    fits_width = bbox_text.width < width if width else True
    fits_height = bbox_text.height < height if height else True
    if not all((fits_width, fits_height)):
        text.set_fontsize(text.get_fontsize()-1)
        auto_fit_fontsize(text, width, height, fig, ax)

def cli_main():
    parser = argparse.ArgumentParser(
                description='Temporal propagation and ' 
                    'scattering of acoustic waves.')
    parser.add_argument('--simConf',
            required=True,
            help='yaml config file with simulation params')

    arguments = parser.parse_args()

    # load simulation config dict
    simConfPath = arguments.simConf
    with open(simConfPath, 'r') as f:
        simConf = yaml.load(f, Loader=yaml.FullLoader)

    # chose hardware
    device = torch.device('cpu')
    if torch.cuda.is_available():
        print('Active CUDA Device: GPU', torch.cuda.current_device())
        device = torch.device('cuda')

    ########################### model loading #######################
    model_path = simConf['modelDir']
    # indicate particular ckpt, if none, choses the one with
    # min loss (stored in /paht/to/model/checkpoints/best_k_models.yaml
    checkpoint=None
    if 'checkpoint' in simConf:
        checkpoint = simConf['checkpoint']
    
    # read configuration files and loading model from the folder
    net, state_dict, train_configuration, model_configuration = \
        pulsenetlib.datatransfer.load_model(
            model_path, ckpt_name=checkpoint, run_device=device)

    ########################### output options ######################
    output_folder = simConf['outputFolder']
    if (not os.path.exists(output_folder)):
        os.makedirs(output_folder, exist_ok=True)


    test_filename = simConf['testFilename']
    filename = output_folder + test_filename
    
    figsize = (4,4) 
    
    if 'fileFormat' in simConf:
        fileformat = simConf['fileFormat']        
    else:
        fileformat = 'png'

    plotType = simConf['plots']
    plotTar = plotType['Target'] == 1
    plotNN = plotType['NN'] == 1
    plotErrorAbs = plotType['Error'] == 1
    plotErrorRel = plotType['Error'] == 1
    plotSlice = plotType['Slices'] == 1
    
    print('Output following results:')
    print('  Target      = ' + str(plotTar))
    print('  NN          = ' + str(plotNN))
    print('  Error (abs) = ' + str(plotErrorAbs))
    print('  Error (rel( = ' + str(plotErrorRel))
    print('  Slices      = ' + str(plotSlice))
    
    ########################### figure options ######################
    latex_style_times = matplotlib.RcParams(
            {
                'font.family': 'stixgeneral',
                'mathtext.fontset': 'stix',
                'text.usetex': False,
            })
    plt.rcParams['axes.unicode_minus'] = False
    plt.style.use(latex_style_times)
    
    # fontsize
    SIZE = 30
    plt.rc('font'  , size=SIZE)       # controls default text sizes
    plt.rc('axes'  , titlesize=SIZE)  # fontsize of the axes title
    plt.rc('axes'  , labelsize=SIZE)  # fontsize of the x and y labels
    plt.rc('xtick' , labelsize=SIZE)  # fontsize of the tick labels
    plt.rc('ytick' , labelsize=SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=SIZE)   # legend fontsize
    plt.rc('figure', titlesize=SIZE)  # fontsize of the figure title
    
    # fluctuating pressure
    max_pred = simConf['prediction']['max']
    nlevels =simConf['prediction']['numLevels']
    power_ten = simConf['prediction']['divideByPowerOfTen']
    vmin = -max_pred 
    vmax = max_pred
    vstep = (vmax - vmin)/(nlevels-1)
    levels = np.linspace(vmin, vmax, num=nlevels-1)
    
    field_map = cm.get_cmap("seismic",lut=nlevels)
    field_map.set_bad('gray')
    
    # errors
    err_max = simConf['error']['max']
    err_nlevels = simConf['error']['numLevels']
    err_map = cm.get_cmap("afmhot",lut=err_nlevels)
    err_map.set_bad('gray')
    err_levels = np.linspace(0,err_max,err_nlevels-1)
    
    # ticks colorbar
    ticks_bar = [vmin, 0.5*vmin, 0.0, 0.5*vmax, vmax]
    ticks_bar_err_abs =[0.0, 0.5*err_max, err_max]
    ticks_bar_err_rel =[0.0, 0.5, 1.0]
    
    # linewidths 
    lw = 1.5
    lw_slice = 3

    # Markers (slices)
    marker='s'
    markevery_nn = 10
    markersize_nn = 5
        
    bbox_extra_artists = []
    
    ######################### simulation options ####################
    with torch.no_grad():
        net.to(device)
        net.eval()

        channels = model_configuration['channels']
        number_channels = len(channels)
        mask_channel = None
        if 'object' in channels:
            mask_channel = channels.index('object')
        
        input_frames = model_configuration['numInputFrames']
        output_frames = model_configuration['numOutputFrames']
        total_number_frames = input_frames + output_frames
        scalar_add = torch.tensor(
            model_configuration['scalarAdd']).to(device=device)
        scalar_mul = torch.tensor([1.], device=device)
        if 'scalarMul' in model_configuration:
            scalar_mul = torch.tensor(
                model_configuration['scalarMul'], device=device)
        frame_step = train_configuration['frameStep']

        # index of predicted field in channel dim
        channel = 0

        stat_freq_config = simConf['statFreq']
        phase = 0
        if isinstance(stat_freq_config, int):
            stat_freq = stat_freq_config 
            stat_freq_config = [{'end': np.inf, 'freq':stat_freq}]
        if isinstance(stat_freq_config, list):
            stat_freq = stat_freq_config[phase]['freq']
        benchmark = False
        if 'benchmark' in simConf:
            benchmark = simConf['benchmark']
            counter = []
       
        # batch size (=1 by default)
        bsz = 1
        show_batch_idx = 0
        if 'batchSize' in simConf:
            bsz = simConf['batchSize']
            show_batch_idx = simConf['showBatchIdx']
            if show_batch_idx > bsz - 1:
                raise ValueError(f'showBatchIdx ({show_batch_idx}) must be smaller than ({bsz-1}).')

        # data loader
        datadir = os.path.join(simConf['dataDir'], simConf['dataset'])
        vtkloader =  pulsenetlib.preprocess_vtk.VTKLoader(channels)
        field_name = channels[0]

        format_files = 'vtk{0:06d}.vti'
        regex_files = 'vtk*.vti'
        
        _, mesh_info = vtkloader.load(os.path.join(datadir, format_files.format(0)))
        nx, ny = mesh_info['nx'], mesh_info['ny']
        
        init_t = 0
        if 't0' in simConf:
            init_t = simConf['t0']
        
        start_io = simConf['startIO']
        max_iter = simConf['maxIter']
        if max_iter < 0:
            print('Using all available dataset frames')
            list_files = glob.glob(os.path.join(datadir, regex_files))
            max_iter = len(list_files) - input_frames - init_t
        
        if start_io >= max_iter:
            raise ValueError('startIO must be smaller than maxIter')

        # physics parameters
        Ly = simConf['domainLength']
        D = simConf['charLength']
        D_name = simConf['charLengthName']
        if len(D_name) > 1:
            raise ValueError('charLengthName must be a string of length <= 1 (if not latex display will be wrong).')
        dx = Ly / (ny-1)

        class NonDimTime():
            def __init__(self, L, c0, dx):
                self.L = L
                self.c0 = c0
                self.dx = dx

            def get_time(self, it):
                return self.c0 * self.dx * it / self.L 

        # c0 is the speed of sound in LB units
        nd_time = NonDimTime(L=D, c0=1/3**0.5, dx=dx)
        
        ########################### slices options ######################
        slice_line = simConf['slice']
        n_slices = len(slice_line)
        output_raw_slice = simConf['outputRawSlice']

        ampl = []
        x_slice = []
        y_slice = []
        slice_coord = []
        for id_slice, s in enumerate(slice_line):
            if np.any(np.less(np.array(s[:4]), 0.)) or np.any(np.greater(np.array(s[:4]), 1.)):
                raise ValueError('Distances in simConf["slice"] are relative to domain length.'
                                 'Expected range is [0,1] and values found are {}'.format(s))
            if s[0] == s[2]:
                x_slice.append(False)
                y_slice.append(True)
                slice_coord.append(int(s[0]*ny-1))
            elif s[1] == s[3]:
                x_slice.append(True)
                y_slice.append(False)
                slice_coord.append(int(s[1]*nx-1))
            else:
                raise ValueError(f'At slice {id_slice}, should be either a x-slice or a y-slice')
            
            ampl.append(s[4])

        max_slice = np.zeros(n_slices)
        min_slice = np.zeros(n_slices)
            

        ########################### probes options ######################
        class Probe:
            def __init__(self,probe_name,x,y,quantity_name,folder):
                self.x = x
                self.y = y
                self.filename = os.path.join(folder, probe_name)
                self.quantity_name = quantity_name

                self.history = []

            def add_meas(self, t, quantity, target):
                self.history.append([t, quantity[self.x, self.y], target[self.x, self.y]])

            def save(self):
                print(f'Saving probe {self.filename}')
                hist_np = np.array(self.history)
                dict_to_save = {
                    't': hist_np[:,0],
                    self.quantity_name: hist_np[:,1],
                    self.quantity_name+'_tar': hist_np[:,2]
                }

                np.savez(self.filename, **dict_to_save)
        
        probes_conf = []
        if 'probes' in simConf:
            probes_conf = simConf['probes']
            probe_folder = os.path.join(output_folder, 'probes')
            os.makedirs(probe_folder, exist_ok=True)

        probes = []
        for probe_id, p in enumerate(probes_conf):
            if not isinstance(p[0], str):
                raise ValueError(
                            f'At {probe_id}, {p[0]} must be a string (name of probe).'
                        )
            if p[1] < 0 or p[1] > 1.0:
                raise ValueError(f'At probe, x ({p[1]}) should be in the range [0,1].')
            if p[2] < 0 or p[2] > 1.0:
                raise ValueError(f'At probe, y ({p[2]}) should be in the range [0,1].')
            if p[3] not in channels:
                raise ValueError(f'At probe, quantity to save ({p[3]}) should be among the available channels {channels}.')
            p[1] = int(p[1]*nx-1)
            p[2] = int(p[2]*ny-1)
            probes.append(Probe(*p,folder=probe_folder))

        # Print sim options 
        print('\nSIMULATION PARAMETERS')
        print(f'{"Domain y-Length =":<32} {Ly}')
        print(f'{"Characteristic Length =":<32} {D}')
        print(f'{"dx =":<32} {dx}')
        print(f'{"Resolution =":<32} [{nx}, {ny}]')
        print(f'{"Initial iteration =":<32} {init_t}')
        print(f'{"Number of iterations =":<32} {max_iter}')
        print(f'{"Output raw slice? =":<32} {output_raw_slice}')
        print('')

        ##################### data intialization ####################
        dtype = torch.float

        data = torch.zeros((bsz, number_channels, input_frames, nx, ny),
                dtype=dtype, device=device)
        initial = torch.zeros((bsz, number_channels, 1, nx, ny),
                dtype=dtype, device=device)
        reference = torch.zeros((bsz, number_channels, output_frames, nx, ny),
                dtype=dtype, device=device)
        grad_rho_tar = torch.zeros((bsz, 2, output_frames, nx, ny),
                dtype=dtype, device=device)
        grad_rho_out = torch.zeros((bsz, 2, output_frames, nx, ny),
                dtype=dtype, device=device)

        error_np = np.empty((max_iter-input_frames+1, 10))
        out_int_E = np.empty((max_iter-input_frames+1, 1))
        out_fluxes_x0 = np.empty((max_iter-input_frames+1, 1))
        out_fluxes_x1 = np.empty((max_iter-input_frames+1, 1))
        out_fluxes_y0 = np.empty((max_iter-input_frames+1, 1))
        out_fluxes_y1 = np.empty((max_iter-input_frames+1, 1))
        tar_int_E = np.empty((max_iter-input_frames+1, 1))
        tar_fluxes_x0 = np.empty((max_iter-input_frames+1, 1))
        tar_fluxes_x1 = np.empty((max_iter-input_frames+1, 1))
        tar_fluxes_y0 = np.empty((max_iter-input_frames+1, 1))
        tar_fluxes_y1 = np.empty((max_iter-input_frames+1, 1))
        
        X, Y = np.linspace(0, nx-1, num=nx), np.linspace(0, ny-1, num=ny)

        for it in range(0, input_frames):
            idx = int((init_t + it)* frame_step)
            data[:,:,it,:,:] = vtkloader.load(os.path.join(datadir, format_files.format(idx)))[0].repeat(bsz,1,1,1)

        mask = None
        mask_input = None
        # create the mask (with channel of size 1)
        if mask_channel is not None:
            mask_shape = (bsz, 1, input_frames)
            for size in data.shape[3:]:
                mask_shape = mask_shape + (size,)
            mask_input = ~data[:,mask_channel,:].to(torch.bool).view(mask_shape)
            mask_shape = (bsz, 1, 1)
            for size in data.shape[3:]:
                mask_shape = mask_shape + (size,)
            mask = ~data[:,mask_channel,0].to(torch.bool).view(mask_shape)
        
        # create the input mask (same shape as data) 
        input_mask = torch.zeros_like(data, dtype=torch.bool, device=data.device)
        input_mask[:,:mask_channel] = mask
        input_mask = input_mask[:,0]
        
        # performing data preparation  (for example, using
        # fluctuating pressure pprime = p - p0 instead of pressure p)
        print('DATA STATISTICS')
        for channel, (s_add, s_mul) in enumerate(zip(scalar_add, scalar_mul)):
            data[:, channel][input_mask] = data[:,channel][input_mask].add(s_add)
            data[:, channel][input_mask] = data[:,channel][input_mask].mul(s_mul)
            for T in range(input_frames):
                mean = pulsenetlib.math_tools.masked_mean(data[:,channel,T]**2, mask[:,0,0])**0.5
                print(f"chan = {channel}, frame = {T} = {mean}")

        # derivative function (for Gradient difference loss evaluation)
        derivate = pulsenetlib.math_tools.Derivation2D(nx, ny, 1, dx=1, dy=1, mask=mask[0].unsqueeze(0)) #LB units!
        
        it = input_frames
        firstOut = True

        ##################### prediction ############################
        print('\nStarting Iterations\n')
        while (it < max_iter+1):
            idx = (init_t + it) * frame_step
            reference[:,:,0,:,:] = vtkloader.load(os.path.join(datadir, format_files.format(idx)))[0].repeat(bsz,1,1,1)
            
            for channel, (s_add, s_mul) in enumerate(zip(scalar_add, scalar_mul)):
                reference[:, channel][mask[:,0]] = reference[:,channel][mask[:,0]].add(s_add)
                reference[:, channel][mask[:,0]] = reference[:,channel][mask[:,0]].mul(s_mul)
            
            # remove mask from target for loss calculation
            data_channels = torch.arange(
                net.num_output_channels, dtype=torch.long, device=device
                ) 
            target = reference.index_select(dim=1, index=data_channels)
            if mask is not None:
                target_shape = target[:,:,0][:,:,None].shape
                target[:,:,0][:,:,None][~mask.expand(target_shape)] = 0

            if benchmark:
                start_time = datetime.datetime.now()

            output = net(data.clone(), mask=mask)
            
            # "Roll" first channel to the last; second to the first etc
            data = torch.cat((data[:,:,1:,:,:], data[:,:,:1,:,:]), dim=2)
            # Replace last frame with the new output
            data[:,:,-1,:,:] = output[:,:,0,:,:].clone()
        
            if benchmark:
                end_time = datetime.datetime.now()
                ellapsed = end_time - start_time
                counter.append(ellapsed)

            ##################### postproc ############################
            output[:, 0][mask[:,0]] = output[:,0][mask[:,0]].div(scalar_mul[0])
            target[:, 0][mask[:,0]] = target[:,0][mask[:,0]].div(scalar_mul[0])

            grad_rho_out[0,0,0], grad_rho_out[0,1,0] = derivate(output[0,0,0])
            grad_rho_tar[0,0,0], grad_rho_tar[0,1,0] = derivate(target[0,0,0])

            alpha = 2
            grad_px_error = torch.abs(  grad_rho_out[:,0] -  grad_rho_tar[:,0] )**alpha + \
                  torch.abs(  grad_rho_out[:,1] -  grad_rho_tar[:,1] )**alpha
            grad_px_error.unsqueeze_(1)

            p_rms_tar = torch.sqrt(pulsenetlib.math_tools.masked_mean(target**2, mask))
            p_rms_out = torch.sqrt(pulsenetlib.math_tools.masked_mean(output**2, mask))
            gradp_rms = torch.sqrt(pulsenetlib.math_tools.masked_mean(grad_rho_tar**2, mask.expand(-1,2,-1,-1,-1)))

            mae = pulsenetlib.math_tools.masked_mean( abs(output - target), mask)
            mse = pulsenetlib.math_tools.masked_mean( (output - target)**2, mask)
            max = torch.max(torch.abs(output- target))
            gdl = pulsenetlib.math_tools.masked_mean( grad_px_error, mask)

            error_np[it - input_frames, 0] = it + init_t
            error_np[it - input_frames, 1] = mae.data.item()
            error_np[it - input_frames, 2] = mse.data.item()
            error_np[it - input_frames, 3] = max.data.item()
            error_np[it - input_frames, 4] = p_rms_tar.data.item()
            error_np[it - input_frames, 5] = p_rms_out.data.item()
            error_np[it - input_frames, 6] = torch.max(torch.abs(target)).data.item()
            error_np[it - input_frames, 7] = gdl.data.item()
            error_np[it - input_frames, 8] = gradp_rms.data.item()
            error_np[it - input_frames, 9] = torch.max(torch.abs(grad_rho_tar)).data.item()
            
            output[:,0][:,None][~mask] = float('nan')
            target[:,0][:,None][~mask] = float('nan')
            output_np = torch.squeeze(output[show_batch_idx,0]).cpu().data.numpy()
            target_np = torch.squeeze(target[show_batch_idx,0]).cpu().data.numpy()
            rel_rms_err_np = np.sqrt((output_np - target_np)**2)/1e-3
            rms_err_np = np.sqrt((output_np - target_np)**2)
            
            for probe in probes:
                probe.add_meas(it,output_np,target_np)
            
            ##################### plot ##############################
            if (it% stat_freq == 0 and it >= start_io):
                print('Iteration {}'.format(it))
                idx = it * frame_step
                tau = nd_time.get_time((it+init_t)*frame_step) 
                iter_title = r'$\tau_{:} = $'.format(D_name) + '{:3.2f} \n'.format(tau) + \
                    '(it = {:03d}'.format(it-input_frames) + ')'

                if plotTar:
                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_p_tar = fig.add_subplot(111)
                    im_tar = ax_p_tar.imshow(target_np,
                        vmin=vmin-vstep/2, vmax=vmax+vstep/2,
                        cmap=field_map,
                        interpolation='none',
                        origin='lower')

                    ax_p_tar.axis('on')
                    ax_p_tar.set_xlabel('')
                    ax_p_tar.set_ylabel('')
                    ax_p_tar.set_xticks([0, 0.25*nx, 0.5*nx, 0.75*nx, nx],minor=[])
                    ax_p_tar.set_yticks([0, 0.25*ny, 0.5*ny, 0.75*ny, ny],minor=[])
                    ax_p_tar.set_xticklabels([])
                    ax_p_tar.set_yticklabels([])
                    x0,x1 = ax_p_tar.get_xlim()
                    y0,y1 = ax_p_tar.get_ylim()
                    ax_p_tar.set_aspect((x1-x0)/(y1-y0))
                    ax_p_tar.set_title(iter_title)

                    filename = os.path.join(output_folder, 'target_{:03d}.{:}'.format(it, fileformat))
                    fig.savefig(filename, bbox_inches='tight', format=fileformat)
                    
                    if firstOut:
                        fig = Figure(figsize=(6,3))
                        canvas=FigureCanvas(fig)
                        ax = fig.add_subplot(111)
                        insert_colobar(fig, ax, im_tar,
                                       width='100%',
                                       height='10%',
                                       bbox_to_anchor=(0.1, 0., 1, 1),
                                       loc='lower left',
                                       ticks=ticks_bar,
                                       format='%.0e',
                                       orientation='horizontal')
                        ax.remove()
                        filename = os.path.join(output_folder, f'cbar_horizontal.{fileformat}')
                        fig.savefig(filename, bbox_inches='tight', format=fileformat)
                        
                        fig = Figure(figsize=(4,4))
                        canvas=FigureCanvas(fig)
                        ax = fig.add_subplot(111)
                        insert_colobar(fig, ax, im_tar,
                                       width='7%',
                                       height='100%',
                                       bbox_to_anchor=(1.1, 0., 1.2, 1),
                                       loc='lower left',
                                       ticks=ticks_bar,
                                       format='%.0e',
                                       orientation='vertical')
                        ax.remove()
                        filename = os.path.join(output_folder, f'cbar_vertical.{fileformat}')
                        fig.savefig(filename, bbox_inches='tight', format=fileformat)

                if plotNN:
                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_p = fig.add_subplot(111)
                    im_p = ax_p.imshow(output_np,
                        vmin=vmin-vstep/2, vmax=vmax+vstep/2,
                        cmap=field_map,
                        interpolation='none',
                        origin='lower')
                    ax_p.axis('on')
                    ax_p.set_xlabel('')
                    ax_p.set_ylabel('')
                    ax_p.set_xticks([0, 0.25*nx, 0.5*nx, 0.75*nx, nx],minor=[])
                    ax_p.set_yticks([0, 0.25*ny, 0.5*ny, 0.75*ny, ny],minor=[])
                    ax_p.set_xticklabels([])
                    ax_p.set_yticklabels([])
                    x0,x1 = ax_p.get_xlim()
                    y0,y1 = ax_p.get_ylim()
                    ax_p.set_aspect((x1-x0)/(y1-y0))

                    filename = os.path.join(output_folder, 'output_{:03d}.{:}'.format(it, fileformat))
                    fig.savefig(filename, bbox_inches='tight', format=fileformat)


                if plotErrorRel:
                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_err = fig.add_subplot(111)
                    im_err = ax_err.imshow(rms_err_np/np.nanmax(rms_err_np),
                        vmin=0, vmax=1,
                        cmap=err_map,
                        interpolation='none',
                        origin='lower')
                    ax_err.axis('on')
                    ax_err.set_xlabel('')
                    ax_err.set_ylabel('')
                    ax_err.set_xticks([0, 0.25*nx, 0.5*nx, 0.75*nx, nx],minor=[])
                    ax_err.set_yticks([0, 0.25*ny, 0.5*ny, 0.75*ny, ny],minor=[])
                    ax_err.set_xticklabels([])
                    ax_err.set_yticklabels([])
                    x0,x1 = ax_err.get_xlim()
                    y0,y1 = ax_err.get_ylim()
                    ax_err.set_aspect((x1-x0)/(y1-y0))
                    
                    filename = os.path.join(output_folder, 'norm_error_{:03d}.{:}'.format(it, fileformat))
                    fig.savefig(filename, bbox_inches='tight', format=fileformat)
                        
                    if firstOut:
                        fig = Figure(figsize=(4,4))
                        canvas=FigureCanvas(fig)
                        ax = fig.add_subplot(111)
                        insert_colobar(fig, ax, im_err,
                                       width='10%',
                                       height='100%',
                                       bbox_to_anchor=(1.1, 0., 1.2, 1),
                                       loc='lower left',
                                       format='%.0e',
                                       orientation='vertical')
                        ax.remove()
                        filename = os.path.join(output_folder, f'cbar_error_rel.{fileformat}')
                        fig.savefig(filename, bbox_inches='tight', format=fileformat)
                    
                if plotErrorAbs:
                    fig = Figure(figsize=figsize)
                    canvas=FigureCanvas(fig)
                    ax_err = fig.add_subplot(111)
                    im_err = ax_err.imshow(rel_rms_err_np,
                        vmin=0, vmax=1e-2,
                        cmap=err_map,
                        interpolation='none',
                        origin='lower')
                    ax_err.axis('on')
                    ax_err.set_xlabel('')
                    ax_err.set_ylabel('')
                    ax_err.set_xticks([0, 0.25*nx, 0.5*nx, 0.75*nx, nx],minor=[])
                    ax_err.set_yticks([0, 0.25*ny, 0.5*ny, 0.75*ny, ny],minor=[])
                    ax_err.set_xticklabels([])
                    ax_err.set_yticklabels([])
                    x0,x1 = ax_err.get_xlim()
                    y0,y1 = ax_err.get_ylim()
                    ax_err.set_aspect((x1-x0)/(y1-y0))
                    insert_colobar(fig, ax_err, im_err,
                                   width='10%',
                                   height='100%',
                                   bbox_to_anchor=(1.1, 0., 1.2, 1),
                                   loc='lower left',
                                   format='%.0e',
                                   orientation='vertical')
                    
                    filename = os.path.join(output_folder, 'abs_error_{:03d}.{:}'.format(it, fileformat))
                    fig.savefig(filename, bbox_inches='tight', format=fileformat)
                        
                    if firstOut:
                        fig = Figure(figsize=(4,4))
                        canvas=FigureCanvas(fig)
                        ax = fig.add_subplot(111)
                        insert_colobar(fig, ax, im_err,
                                       width='10%',
                                       height='100%',
                                       bbox_to_anchor=(1.1, 0., 1.2, 1),
                                       loc='lower left',
                                       format='%.0e',
                                       orientation='vertical')
                        ax.remove()
                        filename = os.path.join(output_folder, f'cbar_error_abs.{fileformat}')
                        fig.savefig(filename, bbox_inches='tight', format=fileformat)
    


                for j in range(n_slices):
                    if x_slice[j]:
                        P_out = output_np[slice_coord[j],:]
                        P_tar = target_np[slice_coord[j],:]
                    if y_slice[j]:
                        P_out = output_np[:,slice_coord[j]]
                        P_tar = target_np[:,slice_coord[j]]

                    if output_raw_slice:
                        out_np = [np.arange(P_out.shape[0])/(P_out.shape[0]-1),
                                P_out/(10**power_ten)]
                        tar_np = [np.arange(P_tar.shape[0])/(P_tar.shape[0]-1),
                                P_tar/(10**power_ten)]
                        filename_out_np = os.path.join(output_folder, 'slice_{:03d}_out_it_{:04d}.npy'.format(j, it))
                        filename_tar_np = os.path.join(output_folder, 'slice_{:03d}_tar_it_{:04d}.npy'.format(j, it))
                        if output_raw_slice:
                            np.save(filename_out_np, out_np)
                            np.save(filename_tar_np, tar_np)

                    if plotSlice:
                        fig = Figure(figsize=figsize)
                        canvas=FigureCanvas(fig)
                        ax_slice = fig.add_subplot(111)
                        if firstOut:
                            eps = 1.0e-6
                            max_tar = np.max(P_tar) + eps

                            max_slice[j] = ampl[j]
                            min_slice[j] = -ampl[j]
                        
                        ax_slice.plot(np.arange(P_out.shape[0])/(P_out.shape[0]-1), P_out/(10**power_ten), linewidth=lw,
                                label='NN',  c= 'r',  marker=marker, markevery=markevery_nn,
                                markersize=markersize_nn, markerfacecolor='None')
                        ax_slice.plot(np.arange(P_tar.shape[0])/(P_tar.shape[0]-1), P_tar/(10**power_ten),
                                linewidth=lw, c='k',
                                label='Target')
                        ax_slice.set_xlim([0, 1])
                        ax_slice.set_ylim([min_slice[j]/(10**power_ten), max_slice[j]/(10**power_ten)])
                        ax_slice.set_xlabel(r'$x/D$')
                        ax_slice.set_ylabel(f'{field_name}'+r'[$\times$10^{:1.0f}]'.format(power_ten))
                        x0,x1 = ax_slice.get_xlim()
                        y0,y1 = ax_slice.get_ylim()
                        ax_slice.set_aspect((x1-x0)/(y1-y0))
                        ax_slice.tick_params(which='major',
                                width=0.3,
                                length=0.1,
                                grid_linewidth=0.2,
                                grid_linestyle='--',
                                grid_color='k', labelsize=12)
                        ax_slice.tick_params(which='minor',
                                width=0.2,
                                length=0.05,
                                grid_linewidth=0.15,
                                grid_linestyle='--',
                                grid_color='k')
                        ax_slice.set_xticks([0.25, 0.5, 0.75])
                        ax_slice.yaxis.set_major_locator(ticker.MultipleLocator((y1-y0)/4))
                        ax_slice.yaxis.set_minor_locator(ticker.MultipleLocator((y1-y0)/8))
                        ax_slice.set_title(iter_title)
                        ax_slice.grid(which='both')
                        legend = ax_slice.legend(facecolor='w', edgecolor='w', bbox_to_anchor=(1, 0.5), loc="center left")
                        
                        filename = os.path.join(output_folder, 'slice_{:03d}_it_{:03d}.{:}'.format(j, it, fileformat))
                        fig.savefig(filename, bbox_inches='tight')
                        if firstOut:
                            lines, labels = ax_slice.get_legend_handles_labels()
                            canvas=FigureCanvas(fig)
                            legend_bbox = legend.get_tightbbox(fig.canvas.get_renderer())
                            legend_bbox = legend_bbox.transformed(fig.dpi_scale_trans.inverted())
                            fig, ax = plt.subplots(figsize=(legend_bbox.width, legend_bbox.height))

                            legend = ax.legend(
                                lines, labels, 
                                bbox_transform=fig.transFigure,
                                facecolor='w', 
                                edgecolor='w', 
                                loc='center left',
                                bbox_to_anchor=(0, 0, 1, 1),
                                mode='expand'
                            )
                            ax.axis('off')
                            filename = os.path.join(output_folder, f'legend_slices.{fileformat}')
                            fig.savefig(filename, bbox_inches='tight',bbox_extra_artists=[legend], format=fileformat)


                if firstOut:
                    firstOut = False
            
            # Update iterations
            it += 1
            if it > stat_freq_config[phase]['end']:
                phase += 1
                stat_freq = stat_freq_config[phase]['freq']
                print(f'New output policiy is: {stat_freq_config[phase]}')
        
        print('End of simulation. Postprocessing... \n')


        filename_error_save = os.path.join(output_folder, test_filename + '_error')
        print(f'Saving error file: "{filename_error_save}.npz"')
        np.savez(filename_error_save,
                 it=error_np[:,0],
                 mae=error_np[:,1],
                 mse=error_np[:,2],
                 max=error_np[:,3],
                 rms_tar=error_np[:,4],
                 rms_out=error_np[:,5],
                 maxtar=error_np[:,6],
                 gdl=error_np[:,7],
                 gradprms=error_np[:,8],
                 maxgradtar=error_np[:,9],
                 init_t=init_t,
                 input_frames=input_frames,
                 frame_step=frame_step,
                 dx=dx,
                 D=D)
        
        print(f'Saving probes')
        for p in probes:
            p.save()

        if benchmark:
            avg_time_per_it = np.asarray(counter).mean()
            print(f'Average time per iteration : {avg_time_per_it}')


        print(f'Saving error plots')
        fig = plt.figure()
        plt.semilogy(error_np[:,0], error_np[:,2], label='mse', lw=2, c='k')
        plt.xlabel('iterations')
        plt.legend()
        filename = os.path.join(output_folder, f'mse_error.{fileformat}')
        plt.savefig(filename, bbox_inches='tight', format=fileformat)
 
        fig = plt.figure()
        plt.semilogy(error_np[:,0], error_np[:,7], label='gdl', lw=2, c='k')
        plt.xlabel('iterations')
        plt.legend()
        filename = os.path.join(output_folder, f'gdl_error.{fileformat}')
        plt.savefig(filename, bbox_inches='tight', format=fileformat)


if __name__ == "__main__":
    cli_main()







