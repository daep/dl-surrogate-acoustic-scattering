#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Helper fonction plotting losses extracted from tensorboard logs in pkl fmt
"""

# native modules
import glob
import os
from typing import Tuple, List, Mapping, Union
import argparse

# third party modules
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# local modules
##none##

"""
    Helper function to plot losses extracted from pickle files.
"""

def longest_common_suffix(list_of_strings):
    reversed_strings = [s[::-1] for s in list_of_strings]
    reversed_lcs = os.path.commonprefix(reversed_strings)
    lcs = reversed_lcs[::-1]
    return lcs

class PlotLoss:
    """
    Plot losses and hyper-parameters from one or several
    runs. Data must be stored in .pkl format (e.g. with tensorboard_to_pkl).

    Parameters
    ----------
    pkldir_or_pkfile : str
        Path/list of paths to pkl files.  
        Partial paths are accepted (automatic search performed in
        sub-folders).
    out_dir : str, optional, default='figures'
        Output directory
    grouped_losses : list[str], optional
        Plots several losses from a single run.
        Given N names (substrings), creates N plots with losses matching
        given substrings.
    grouped_runs : list[str], optional
        Plots single loss from several runs.
        Given N names (substrings), creates N plots with losses matching
        given substrings.
    plot_epochs : bool, optional, default=True
        X-axis displays epochs (True) or steps (False)
    print_labels : bool, optional, default=False
        Prints the available labels
        for the found pkl (debug)
    y_scale : str, optional, options: ("linear","log")
        Y-axis scaling.
    y_lim : list, optional
        Set y-axis limits for losses (not hparams).
        Three options are available:
        - 0 args: automatic scaling
        - 1 arg: set y-max
        - 2 arg: sets (y-min, y-max)

    Example usage
    -------------
    # plot individual losses from a single run to default folder
    python plot_loss_pkl.py path/to/pkl_file 

    # plot individual figures and group the "val_loss" and "train_loss" losses
    # sharing the "loss" substring
    python plot_loss_pkl.py path/to/pkl_file --group-losses loss

    # Having 2 runs "run_1" and "run_2", 
    # plot individual figures and group the "val_loss" losses
    # from the 2 runs
    python plot_loss_pkl.py path/to/run_1_pkl_file path/to/run_2_pkl_file
     --group-runs val_loss
    """

    RUN_NAME_SEP = "/"
    PLT_SIZE = 20
    LEGEND_SIZE = 16
    FIGSIZE = (10,10)
    
    latex_style_times = matplotlib.RcParams(
            {
                'font.family': 'stixgeneral',
                'mathtext.fontset': 'stix',
                'text.usetex': False,
            })
    plt.rcParams['axes.unicode_minus'] = False
    plt.style.use(latex_style_times)

    plt.rc('font'  , size=PLT_SIZE)       
    plt.rc('axes'  , titlesize=PLT_SIZE)
    plt.rc('axes'  , labelsize=PLT_SIZE)
    plt.rc('xtick' , labelsize=PLT_SIZE)
    plt.rc('ytick' , labelsize=PLT_SIZE)
    plt.rc('legend', fontsize=LEGEND_SIZE)
    plt.rc('figure', titlesize=PLT_SIZE)

    def __init__(self, pkldir_or_pklfile: List,
                    out_dir: str = 'figures',
                    plot_individual: bool = True,
                    save_to_file: bool = True,
                    grouped_losses: List[str] = None,
                    grouped_runs: List[str] = None,
                    plot_epochs: bool = True,
                    print_labels: bool = False,
                    legend: list = [],
                    y_scale: str = 'linear', y_lim: List[float] = list()
                    ):
        
        self.plot_epochs = plot_epochs
        self.print_labels = print_labels
        self.y_scale = y_scale
        if len(y_lim) == 0:
            self.y_lim = {} 
        if len(y_lim) == 1:
            self.y_lim = {'top' : float(y_lim[0])}
        elif len(y_lim) == 2:
            self.y_lim = {'bottom': float(y_lim[0]), 'top' : float(y_lim[1])}
        if len(y_lim) > 2:
            m = (f'y_lim should have either 1 value (max) '
                f'or 2 values (min, max)')
            raise ValueError(m)

        self.legend = legend

        if len(pkldir_or_pklfile) == 1:
            if os.path.isfile(pkldir_or_pklfile[0]):
                pkl_paths = pkldir_or_pklfile[0]
            else:
                pkl_paths = glob.glob(
                    os.path.join('*' + pkldir_or_pklfile[0] + '*', "**/*.pkl"),
                         recursive=True
                )
        else:
            pkl_paths = []
            for pkl in pkldir_or_pklfile:
                pkl_paths.extend(
                    glob.glob(
                        os.path.join('*' + pkl + '*', "**/*.pkl"),
                        recursive=True
                    )
                )

        common_name = ""
        common_suffix = "/pkl/all_training_logs_in_one_file.pkl"
        if len(pkl_paths) > 1:
            common_name_len = len(os.path.commonpath(pkl_paths))
            common_suffix_len = len(longest_common_suffix(pkl_paths))

        losses = {}
        hparams = {}
        
        for path in pkl_paths:
            run_name = path[common_name_len:-common_suffix_len].replace("/","")
            
            ind_out_dir = os.path.join(out_dir, run_name)
            if plot_individual or save_to_file or grouped_losses is not None:
                os.makedirs(ind_out_dir, exist_ok=True)
            loss, hparam = self._get_losses_and_hparams(
                path,
                run_name=run_name,
                print_labels=print_labels,
                save_to_file=save_to_file,
                save_dir=ind_out_dir)
            losses.update(loss)
            hparams.update(hparam)

            if plot_individual:
                self.plot_individual_losses(
                    loss,
                    ind_out_dir,
                    run_name)
                self.plot_individual_losses(
                    hparam,
                    ind_out_dir,
                    run_name,
                    auto_scale=True)
            if grouped_losses is not None:
                self.plot_several_losses_run(
                    loss, grouped_losses, ind_out_dir, run_name)
                self.plot_several_losses_run(
                    hparam, grouped_losses, ind_out_dir, run_name, auto_scale=True)

        if grouped_runs is not None:
            for group in grouped_runs:
                group_out_dir = os.path.join(out_dir, group.replace("/","_")) 
                os.makedirs(group_out_dir, exist_ok=True)
                self.plot_loss_several_runs(losses, group, group_out_dir)
                self.plot_loss_several_runs(hparams, group, group_out_dir, auto_scale=True)

    def _rename_keys(self, d: dict, keys: dict):
        return dict([(keys.get(k), v) for k, v in d.items()])

    def _get_losses_and_hparams(self,
             path: str,
             labels_to_ext: set = None,
             run_name: str = None,
             print_labels: bool = False,
             save_to_file: bool = False, save_dir: str = None) -> Tuple[Mapping, Mapping]:
        
        if save_to_file:
            assert save_dir is not None, 'save_dir cannot be None for save_to_file=True'
        # Reads pickle, processes it according to the given
        # labels and return a dict with the loss
        df = pd.read_pickle(path)
        all_labels = set(df.loc[:, 'metric'])

        if labels_to_ext is not None: 
            labels = labels_to_ext.intersection(all_labels)
        else:
            labels = all_labels

        losses_labels = []
        hparams_labels = []

        for label in labels:
            if 'val' in label or 'train' in label or 'loss' in label:
                losses_labels.append(label)
            else:
                hparams_labels.append(label)

        if self.print_labels:
            print(f'Available labels for log {path}:')
            print(f'Losses')
            print(f'{losses_labels}')
            print(f'Hyper-parameters')
            print(f'{hparams_labels}')
            
        losses_dict = {key: None for key in losses_labels}
        hparams_dict = {key: None for key in hparams_labels}
        for key in losses_dict.keys():
            mask = df.loc[:,'metric'] == key

            # curate data (eliminate repeated steps)
            current_loss = df[mask].drop_duplicates('step')

            x = current_loss['step'].values.astype(float) 
            y = current_loss['value'].values.astype(float) 

            sorted_idx = np.argsort(x, kind='stable')

            losses_dict[key] = {
                'x': x[sorted_idx], 
                'y': y[sorted_idx]
            } 
        
        for key in hparams_dict.keys():
            mask = df.loc[:,'metric'] == key

            # curate data (eliminate repeated steps)
            current_loss = df[mask].drop_duplicates('step')

            hparams_dict[key] = {
                'x': current_loss['step'].values.astype(float), 
                'y': current_loss['value'].values.astype(float)}

        
        if 'loss' in losses_dict.keys():
            losses_dict['train_loss'] = losses_dict['loss']
            del losses_dict['loss']

        conversion = 1.0
        if self.plot_epochs:
            conversion = np.mean((hparams_dict['epoch']['y'] +1) /\
            (hparams_dict['epoch']['x']))

        for key in hparams_dict.keys():
            hparams_dict[key]['x'] =  hparams_dict[key]['x']*conversion
        for key in losses_dict.keys():
            losses_dict[key]['x'] =  losses_dict[key]['x']*conversion

        # append run name to loss keys
        if run_name is not None:
            keys = losses_dict.keys()
            new_keys = {}
            for k in keys:
                new_keys[k] = run_name + self.RUN_NAME_SEP + k
            losses_dict = self._rename_keys(losses_dict, new_keys)
            
            keys = hparams_dict.keys()
            new_keys = {}
            for k in keys:
                new_keys[k] = run_name + self.RUN_NAME_SEP + k
            hparams_dict = self._rename_keys(hparams_dict, new_keys)
        
        if save_to_file:
            losses_file = os.path.join(save_dir, 'losses.h5')
            hparams_file = os.path.join(save_dir, 'hparams.h5')
            store_loss = pd.HDFStore(losses_file)
            for key, value in losses_dict.items():
                df = pd.DataFrame(data=value)
                store_loss[key] = df
            store_loss.close()
            store_hparams = pd.HDFStore(hparams_file)
            for key, value in hparams_dict.items():
                df = pd.DataFrame(data=value)
                store_hparams[key] = df
            store_hparams.close()
            print(f'Saving loss log to file {losses_file}')
            print(f'Saving hparam log to file {hparams_file}')

        print(f'log {path} : extraction done \n')
        return losses_dict, hparams_dict

    def plot_individual_losses(self, 
            losses: Mapping,
            out_dir: str,
            run_name: str = None,
            auto_scale: bool = False):

        for name, value in losses.items():
            if run_name is not None:
                match = name.find(run_name)
                end = match + len(run_name) + len(self.RUN_NAME_SEP)
                if match > -1:
                    name = name[end:]

            fig = plt.figure(figsize=self.FIGSIZE)
            plt.plot(
                value['x'],
                value['y'],
                label=name, lw=3, c='k')
            if self.plot_epochs:
                plt.xlabel('epochs')
            else:
                plt.xlabel('steps')
            if not auto_scale:
                plt.yscale(self.y_scale)
                plt.ylim(**self.y_lim)
            plt.legend()
            plt.grid()
            save_name = os.path.join(out_dir, name.replace("/", "_"))
            plt.savefig(save_name, bbox_inches='tight')
            plt.close()
    
    def plot_several_losses_run(self,
            losses: Mapping, 
            regex: list,
            out_dir: str,
            run_name: str = None,
            auto_scale: bool = False):
        for reg in regex:
            names = []
            loss_to_plot = []
            for m in losses:
                save_name = False
                match = m.find(reg) 
                if match > -1:
                    save_name = True
                    loss_to_plot.append(m)
                    start = match
                    end = match + len(regex)
                    name = m[:start] 

                if run_name is not None:
                    match = m.find(run_name)
                    end = match + len(run_name) + len(self.RUN_NAME_SEP)
                    if match > -1 and save_name:
                        name = name[end:]

                if save_name:
                    # remove heading underscores
                    if name[0] == "_":
                        name = name[1:] 
                    # remove trailing underscores
                    if name[-1] == "_":
                        name = name[:-1] 
                    names.append(name.replace("/",""))

            if len(names) == 0:
                continue
            
            fig = plt.figure(figsize=self.FIGSIZE)
            for name, m in zip(names, loss_to_plot):
                plt.plot(losses[m]['x'],
                         losses[m]['y'],
                         label=name,
                         lw=3)
            if self.plot_epochs:
                plt.xlabel('epochs')
            else:
                plt.xlabel('steps')
            if not auto_scale:
                plt.yscale(self.y_scale)
                plt.ylim(**self.y_lim)
            plt.title(reg.replace("/", "_"))
            plt.legend()
            plt.grid()
            save_name = os.path.join(out_dir, reg.replace("/", "_"))
            plt.savefig(save_name, bbox_inches='tight')
            plt.close()

    def plot_loss_several_runs(self,
             loss: Mapping,
             regex: str,
             out_dir: str,
             run_name: str = None, 
             auto_scale: bool = False):
        names = []
        loss_to_plot = []
        for m in loss:
            save_name = False
            match = m.find(regex) 
            if match > -1:
                save_name = True
                loss_to_plot.append(m)
                start = match
                end = match + len(regex)
                name = m[:start] 
                
            if run_name is not None:
                match = m.find(run_name)
                end = match + len(run_name) + len(self.RUN_NAME_SEP)
                if match > -1 and save_name:
                    name = name[end:]

            if save_name:
                name = name.replace("/","_")
                # remove heading underscores
                if name[0] == "_":
                    name = name[1:] 
                # remove trailing underscores
                if name[-1] == "_":
                    name = name[:-1] 
                names.append(name)
        
        if len(names) == 0:
            return

        if len(self.legend) == len(names):
            names = self.legend
        
        fig = plt.figure(figsize=self.FIGSIZE)
        for name, m in zip(names, loss_to_plot):
            print(name)
            plt.plot(loss[m]['x'],
                     loss[m]['y'],
                     label=name,
                     lw=3)
        if self.plot_epochs:
            plt.xlabel('epochs')
        else:
            plt.xlabel('steps')
        if not auto_scale:
            plt.yscale(self.y_scale)
            plt.ylim(**self.y_lim)
        plt.title(regex.replace("/", "_"))
        plt.legend()
        plt.grid()
        out_name = os.path.join(out_dir, regex.replace("/", "_").replace(".", "_"))
        plt.savefig(out_name, bbox_inches='tight')
        plt.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                description='Plot losses from pkl data.'
    )
    parser.add_argument('pkl_files',
        nargs='+',
        help='location of file where data is located')
    parser.add_argument('--out-dir', '-o',
        dest='out_dir',
        default='figures',
        help='location of output dir')
    parser.add_argument('--epochs', '-e', action='store_true',
        dest='plot_epochs',
        help="Plot losses versus epochs")
    parser.add_argument('--steps', '-s', action='store_false',
        dest='plot_epochs',
        help="Plot losses versus steps")
    parser.set_defaults(plot_epochs=True)
    parser.add_argument('--plot_individual_losses', '-ind', action='store_true',
        default='False',
        help="Plot a separate graph for each individual metric")
    parser.add_argument('--save_loss', '-save', action='store_true',
        default='False',
        help="Save h5 files")
    parser.add_argument('--group-losses','-ls',
        dest='group_losses',
        nargs='+', help=f'Group several losses from a single run in a plot.')
    parser.add_argument('--group-runs','-rs',
        dest='group_runs',
        nargs='+', help='Group a loss from several runs in a plot.')
    parser.add_argument('--legend','-leg',
        nargs='+', help='Set legend for runs.')
    parser.add_argument('--show-labels', '-labs', action='store_true',
        dest='show_labels',
        help="Print all available labels in each pkl for debugging")
    parser.add_argument('--yscale', '-ys',
        default='linear',
        help="Set y-axis scaling. Options: lin/log")
    parser.add_argument('--ylim', '-ylim',
        nargs='+',
        help=f"Set y-axis limits. If one arg is provided, only max is set."
            f" Two args correspond to the (min, max) tuple."
            f" More args raise an error.")

    args = parser.parse_args()

    PlotLoss(args.pkl_files,
             args.out_dir,
             args.plot_individual_losses,
             args.save_loss,
             args.group_losses,
             args.group_runs,
             args.plot_epochs,
             args.show_labels,
             args.legend,
             args.yscale,
             args.ylim
            )
