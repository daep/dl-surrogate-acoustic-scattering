#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Acoustic related quantities
"""

# native modules
import os

# third party modules
import torch
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable

# local modules
import pulsenetlib.transforms
import pulsenetlib.math_tools

def fmt():
    fmt = ticker.ScalarFormatter(useMathText=True)
    fmt.set_powerlimits((0, 0))
    return fmt

def colorbar(mappable, ax):
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)

    return fig.colorbar(mappable, cax=cax, format=fmt())

def compare_velocities(vel1, vel2,
            x_slice, X, Y, dx, name1, name2, suptitle, outname,
            levels=None):
     
        fig, axs = plt.subplots(1,3, figsize=(35,10))
        axs = axs.flatten()

        if levels is None:
            min_1 = vel1.min()
            max_1 = vel1.max()
            min_2 = vel2.min()
            max_2 = vel2.max()
            min_level = np.min((min_1, min_2))
            max_level = np.max((max_1, max_2))
            levels = np.linspace(min_level, max_level, 8)
        
        vel1_plot = axs[0].contourf(X, Y, vel1, levels=levels)
        axs[0].axvline(x_slice*dx, c='k', lw=2, ls='--')
        x0,x1 = axs[0].get_xlim()
        y0,y1 = axs[0].get_ylim()
        axs[0].set_aspect((x1-x0)/(y1-y0))
        axs[0].set_title(name1)
        cbar = colorbar(vel1_plot, axs[0])
        cbar.formatter.set_powerlimits((0, 0))
        
        vel2_plot = axs[1].contourf(X, Y, vel2, levels=levels)
        axs[1].axvline(x_slice*dx, c='k', lw=2, ls='--')
        x0,x1 = axs[1].get_xlim()
        y0,y1 = axs[1].get_ylim()
        axs[1].set_aspect((x1-x0)/(y1-y0))
        axs[1].set_title(name2)
        cbar = colorbar(vel2_plot, axs[1])
        cbar.formatter.set_powerlimits((0, 0))

        axs[2].plot(vel1[x_slice,:], label=name1, ls='-', c='r', lw=4)
        axs[2].plot(vel2[x_slice,:], label=name2, c='k', ls='--', lw=4)
        axs[2].set_ylim(levels[0], levels[-1])
        
        axs[2].ticklabel_format(axis='y', scilimits=(0,0))

        x0,x1 = axs[2].get_xlim()
        y0,y1 = axs[2].get_ylim()
        axs[2].set_aspect((x1-x0)/(y1-y0))
        axs[2].set_title('slice x= {:2.0f}'.format(x_slice*dx))
        axs[2].legend(loc="lower right") #bbox_to_anchor=(1.01,0.6)

        fig.suptitle(suptitle)
        fig.savefig(outname, bbox_inches='tight')
        plt.close()

def acousticEnergy(p, U, rho0=1, c0sq=340**2):
    r""" Calculates the acoustic energy according to Kirchhoff's equation
    for quiescent fluids (see Rienstra and Hirschberg "An Introduction to
    Acoustics" Chapter 2.7.2).
    
    Careful use this function as it is only valid until strong assumptions 
    (quiescent fluids, reflecting walls, etc).
    
    ----------
    ARGUMENTS
    
        p (Tensor): acoustic pressure field, format (N, 1, D, H, W)
        U (Tensor): input perturbed velocity field, format
                    (N, C, D, H, W) (2 channels in dim 1 if 2D)
        rho0 (float): steady background density
        c0sq (float): squared speed of sound (constant)
    
    ----------
    RETURNS
    
        (Tensor) output scalar acoustic energy density field.
    
    """

    term1 = (0.5/(rho0*c0sq)) * torch.pow(p, 2)
    term2 = 0.5 * rho0 * torch.pow(torch.norm(U, dim=1, keepdim=True), 2)

    return term1.unsqueeze(1) + term2

def masked_mean(input, mask):
    return torch.sum(input[mask]) / torch.count_nonzero(mask)

class EnergyPreservingCorrection(object):
    def __init__(self, nx, ny, dt, dx, c0, rho0, mask=None, device='cpu',
                 apply_correction=True, debug=False, output_debug=None):
        
        self.nx = nx
        self.ny = ny
        self.dt = dt
        self.dx = dx 
        
        if mask.dim() != 3:
            raise ValueError(
                f'mask tensor must have 3 dims, (bsz, ny, nx).'
                f' Found dim={mask.dims()}')

        self.mask = mask.transpose(-1,-2)

        self.c0sq = c0*c0
        self.rho0 = rho0

        self.mask = mask

        # derivative in LB units!
        self.derivate = pulsenetlib.math_tools.Derivation2D(
            nx, ny, 1,
            mask=mask,
            dx=1, dy=1, device=device)

        self.dtype = torch.float32
        self.device = device
        
        self.t = torch.arange(0, 2, device=self.device)*self.dt

        self.apply_correction = apply_correction
        self.debug = debug
        self.output_debug = output_debug
        if debug:
            assert self.output_debug is not None
            os.makedirs(self.output_debug, exist_ok=True)
        self.check_nan = False
        
        self.history = {
                'iteration' : np.array([]),
                'fluxes_x0' : np.array([]), 'fluxes_x1' : np.array([]), 'fluxes_y0' : np.array([]),'fluxes_y1' : np.array([]), 
                'A' : np.array([]),
                'B' : np.array([]),
                'C' : np.array([]),
                'D' : np.array([]),
                'dE_dt' : np.array([]),
                'correction' : np.array([]),
                'correction_th' : np.array([]),
                'rho_max' : np.array([]),
                'rho_rms' : np.array([]),
                'onoff' : np.array([]), 
                }

        self.ode = pulsenetlib.math_tools.Solve_1ord_ODE(self.dt, self.debug)

    def init(self, rho_init):
        if rho_init.dim() != 4:
            raise ValueError(
                f'rho_init tensor must have 4 dims, (bsz, T, ny, nx).'
                f' Found dim={rho_init.dims()}')
        bsz = rho_init.shape[0]
        input_frames = rho_init.shape[1]

        rho_init = rho_init.transpose(-1,-2)
        
        # Variables are stored at T-1 and T
        dorder = 2
        ndim = 2
        self.rho = torch.zeros((bsz,dorder,self.nx,self.ny), dtype=self.dtype, device=self.device)
        self.grad_rho = torch.zeros((bsz,dorder,ndim,self.nx,self.ny), dtype=self.dtype, device=self.device)
        self.vel = torch.zeros((bsz,dorder,ndim,self.nx,self.ny), dtype=self.dtype, device=self.device)

        self.rho[:,0] = rho_init[:,0]
        # calculate initial velocity
        self.grad_rho[:,0,0], self.grad_rho[:,0,1] = self.derivate(self.rho[:,0])
        self.vel[:,0] = 0.0

        for i in range(0,input_frames):
            if i==0:
                self.rho[:,1] = rho_init[:,i+1]
                self.grad_rho[:,1,0], self.grad_rho[:,1,1] = self.derivate(self.rho[:,1])
                self.vel[:,1] = torch.trapz(-self.c0sq*self.grad_rho, x=self.t, dim=1) + self.vel[:,0]

                dp_dt = self.c0sq*(self.rho[:,1] - self.rho[:,0]) / self.dt
                dp2_dt = self.c0sq**2*(self.rho[:,1]**2 - self.rho[:,0]**2) / self.dt
        
                vel_norm_m = ((self.vel[:,0,0]**2 + self.vel[:,0,1]**2))**0.5
                vel_norm = ((self.vel[:,1,0]**2 + self.vel[:,1,1]**2))**0.5
                du2_dt = (vel_norm**2 - vel_norm_m**2) / self.dt

            else:
                self.rho[:,1] = rho_init[:,i]
                self.grad_rho[:,1,0], self.grad_rho[:,1,1] = self.derivate(self.rho[:,1])
                self.vel[:,1] = torch.trapz(-self.c0sq*self.grad_rho, x=self.t, dim=1) + self.vel[:,0]

                dp_dt = self.c0sq*(self.rho[:,1] - self.rho[:,0]) / self.dt
                dp2_dt = self.c0sq**2*(self.rho[:,1]**2 - self.rho[:,0]**2) / self.dt
        
                vel_norm_m = ((self.vel[:,0,0]**2 + self.vel[:,0,1]**2))**0.5
                vel_norm = ((self.vel[:,1,0]**2 + self.vel[:,1,1]**2))**0.5
                du2_dt = (vel_norm**2 - vel_norm_m**2) / self.dt
            
            fluxes_c_x0 = -self.vel[:,1,0,0,:][None,:]
            fluxes_c_x1 =  self.vel[:,1,0,-1,:][None,:]
            fluxes_c_y0 = -self.vel[:,1,1,:,0][None,:]
            fluxes_c_y1 =  self.vel[:,1,1,:,-1][None,:]
        
            fluxes_d_x0 = -(self.c0sq*self.rho[:,1,0,:]*self.vel[:,1,0,0,:])[None,:]
            fluxes_d_x1 =  (self.c0sq*self.rho[:,1,-1,:]*self.vel[:,1,0,-1,:])[None,:]
            fluxes_d_y0 = -(self.c0sq*self.rho[:,1,:,0]*self.vel[:,1,1,:,0])[None,:]
            fluxes_d_y1 =  (self.c0sq*self.rho[:,1,:,-1]*self.vel[:,1,1,:,-1])[None,:]

            i_fluxes_c_x0 = torch.trapz(fluxes_c_x0, dx=1)
            i_fluxes_c_x1 = torch.trapz(fluxes_c_x1, dx=1)
            i_fluxes_c_y0 = torch.trapz(fluxes_c_y0, dx=1)
            i_fluxes_c_y1 = torch.trapz(fluxes_c_y1, dx=1)

            i_fluxes_d_x0 = torch.trapz(fluxes_d_x0, dx=1)
            i_fluxes_d_x1 = torch.trapz(fluxes_d_x1, dx=1)
            i_fluxes_d_y0 = torch.trapz(fluxes_d_y0, dx=1)
            i_fluxes_d_y1 = torch.trapz(fluxes_d_y1, dx=1)
        
            total_fluxes_c = i_fluxes_c_x0 + i_fluxes_c_x1 + i_fluxes_c_y0 + i_fluxes_c_y1
            total_fluxes_d = i_fluxes_d_x0 + i_fluxes_d_x1 + i_fluxes_d_y0 + i_fluxes_d_y1

            cte = 1.0/(self.rho0*self.c0sq) 
            A = self.mask.count_nonzero()*cte*0.5 
            B = torch.trapz(torch.trapz(cte*self.rho[:,1], dx=1, dim=-1), dx=1, dim=-1)
            C = torch.trapz(torch.trapz(cte*dp_dt, dx=1, dim=-1), dx=1, dim=-1) + \
                total_fluxes_c
            dE_dt = torch.trapz(torch.trapz(0.5*cte*dp2_dt + 0.5*self.rho0*du2_dt, dx=1, dim=-1), dx=1, dim=-1)
            D = dE_dt + total_fluxes_d
            
            self.history['iteration'] = np.concatenate(( self.history['iteration'], np.array([i]) ))
            self.history['dE_dt'] = np.concatenate(( self.history['dE_dt'], np.array([dE_dt.cpu().item()]) ))
            self.history['A'] = np.concatenate(( self.history['A'], np.array([A.cpu().item()]) ))
            self.history['B'] = np.concatenate(( self.history['B'], np.array([B.cpu().item()]) ))
            self.history['C'] = np.concatenate(( self.history['C'], np.array([C.cpu().item()]) ))
            self.history['D'] = np.concatenate(( self.history['D'], np.array([D.cpu().item()]) ))
            self.history['fluxes_x0'] = np.concatenate(( self.history['fluxes_x0'], np.array([i_fluxes_d_x0.cpu().item()]) ))
            self.history['fluxes_x1'] = np.concatenate(( self.history['fluxes_x1'], np.array([i_fluxes_d_x1.cpu().item()]) ))
            self.history['fluxes_y0'] = np.concatenate(( self.history['fluxes_y0'], np.array([i_fluxes_d_y0.cpu().item()]) ))
            self.history['fluxes_y1'] = np.concatenate(( self.history['fluxes_y1'], np.array([i_fluxes_d_y1.cpu().item()]) ))
            rho_max = torch.max(self.rho[:,1]).cpu().item()
            self.history['rho_max'] = np.concatenate(( self.history['rho_max'], np.array([(torch.max(self.rho[:,1])).cpu().item()]) ))
            rho_rms = masked_mean(self.rho[:,1]**2, self.mask)**0.5
            self.history['rho_rms'] = np.concatenate(( self.history['rho_rms'], np.array([rho_rms.cpu().item()]) ))
            self.history['onoff'] = np.concatenate(( self.history['onoff'], np.array([0.0]) ))

            self.correction = 0.0
            if self.apply_correction:
                if i>input_frames-1:
                    _, self.correction, self.int0 = self.ode.solve_o1(i, self.correction, self.int0,
                        self.history['B'][i-1:i+1], self.history['C'][i-1:i+1], self.history['D'][i-1:i+1])
                    self.history['correction_th'] = np.concatenate(( self.history['correction_th'], np.array([self.correction]) ))
                else:
                    self.int0 = 0.0
            
            self.history['correction'] = np.concatenate(( self.history['correction'], np.array([0.0]) ))

            if self.debug:
                print(f"i= {i}")
                print(f"rho_rms= {rho_rms:2.2e}")
                print(f"rho_max= {rho_max:2.2e}")
            
                iteration_name = f"t={i:05d}"
                name1 = r"$-(1/\rho_0) \int_{t0}^t \nabla_x p dt$"
                name2 = r"$-(1/\rho_0) \int_{t0}^t \nabla_y p dt$"
                output_file = "vel_" + iteration_name
                levels = np.linspace(-0.0003, 0.0003, 9)
                outname = os.path.join(self.output_debug, output_file)
                X,Y = np.mgrid[0:self.nx,0:self.ny]
                X = X*self.dx
                Y = Y*self.dx

                compare_velocities(self.vel[0,1,0].cpu(), self.vel[0,1,1].cpu(),
                                   150, X, Y, self.dx,
                                   name1, name2,
                                   iteration_name, 
                                   outname,
                                   levels=levels,
                                  )
            
            if i>0:
                # store for next iteration
                self.rho[:,0] = self.rho[:,1]
                self.grad_rho[:,0] = self.grad_rho[:,1]
                self.vel[:,0] = self.vel[:,1]
 
        if self.check_nan:
            nanrho = (torch.isnan(rho_init)).nonzero()            
            nangradrho_init = (torch.isnan(grad_rho_init)).nonzero()            
            nangradrho = (torch.isnan(self.grad_rho)).nonzero()            
            nanvel = (torch.isnan(self.vel)).nonzero()

            print('Checking for nans')
            print(f'Nan rho: \n {nanrho}')         
            print(f'Nan gradrhoinit: \n {nangradrho_init}')         
            print(f'Nan gradrho: \n {nangradrho}')         
            print(f'Nan vel: \n {nanvel}')         
            print()
        

    def apply(self, i, rho_pred):
        if rho_pred.dim() != 4:
                f'rho_pred tensor must have 4 dims, (bsz, T, ny, nx).'
                f' Found dim={rho_pred.dims()}'
        rho_pred = rho_pred.transpose(-1,-2)

        self.rho[:,1] = rho_pred[:,0]
        self.grad_rho[:,1,0], self.grad_rho[:,1,1] = self.derivate(self.rho[:,1])
        self.vel[:,1] = torch.trapz(-self.c0sq*self.grad_rho, x=self.t, dim=1) + self.vel[:,0]
        
        if self.check_nan:
            nanrho = (torch.isnan(self.rho)).nonzero()            
            nangradrho = (torch.isnan(self.grad_rho)).nonzero()            
            nanvel = (torch.isnan(self.vel)).nonzero()

            print('Checking for nans')
            print(f'Nan rho: \n {nanrho}')         
            print(f'Nan gradrho: \n {nangradrho}')         
            print(f'Nan vel: \n {nanvel}')
            print() 
        
        # time derivatives
        dp_dt = self.c0sq*(self.rho[:,1] - self.rho[:,0]) / self.dt
        dp2_dt = self.c0sq**2*(self.rho[:,1]**2 - self.rho[:,0]**2) / self.dt
        
        vel_norm_m = ((self.vel[:,0,0]**2 + self.vel[:,0,1]**2))**0.5
        vel_norm = ((self.vel[:,1,0]**2 + self.vel[:,1,1]**2))**0.5
        du2_dt = (vel_norm**2 - vel_norm_m**2) / self.dt

        fluxes_c_x0 = -self.vel[:,1,0,0,:][None,:]
        fluxes_c_x1 =  self.vel[:,1,0,-1,:][None,:]
        fluxes_c_y0 = -self.vel[:,1,1,:,0][None,:]
        fluxes_c_y1 =  self.vel[:,1,1,:,-1][None,:]
        
        fluxes_d_x0 = -(self.c0sq*self.rho[:,1,0,:]*self.vel[:,1,0,0,:])[None,:]
        fluxes_d_x1 =  (self.c0sq*self.rho[:,1,-1,:]*self.vel[:,1,0,-1,:])[None,:]
        fluxes_d_y0 = -(self.c0sq*self.rho[:,1,:,0]*self.vel[:,1,1,:,0])[None,:]
        fluxes_d_y1 =  (self.c0sq*self.rho[:,1,:,-1]*self.vel[:,1,1,:,-1])[None,:]
            
        i_fluxes_c_x0 = torch.trapz(fluxes_c_x0, dx=1)
        i_fluxes_c_x1 = torch.trapz(fluxes_c_x1, dx=1)
        i_fluxes_c_y0 = torch.trapz(fluxes_c_y0, dx=1)
        i_fluxes_c_y1 = torch.trapz(fluxes_c_y1, dx=1)

        i_fluxes_d_x0 = torch.trapz(fluxes_d_x0, dx=1)
        i_fluxes_d_x1 = torch.trapz(fluxes_d_x1, dx=1)
        i_fluxes_d_y0 = torch.trapz(fluxes_d_y0, dx=1)
        i_fluxes_d_y1 = torch.trapz(fluxes_d_y1, dx=1)
        
        total_fluxes_c = i_fluxes_c_x0 + i_fluxes_c_x1 + i_fluxes_c_y0 + i_fluxes_c_y1
        total_fluxes_d = i_fluxes_d_x0 + i_fluxes_d_x1 + i_fluxes_d_y0 + i_fluxes_d_y1

        cte = 1.0/(self.rho0*self.c0sq) 
        A = self.mask.count_nonzero()*cte*0.5 
        B = torch.trapz(torch.trapz(cte*self.rho[:,1], dx=1, dim=-1), dx=1, dim=-1)
        C = torch.trapz(torch.trapz(cte*dp_dt, dx=1, dim=-1), dx=1, dim=-1) + \
            total_fluxes_c
        dE_dt = torch.trapz(torch.trapz(0.5*cte*dp2_dt + 0.5*self.rho0*du2_dt, dx=1, dim=-1), dx=1, dim=-1)
        D = dE_dt + total_fluxes_d

        self.history['iteration'] = np.concatenate(( self.history['iteration'], np.array([i]) ))
        self.history['dE_dt'] = np.concatenate(( self.history['dE_dt'], np.array([dE_dt.cpu().item()]) ))
        self.history['A'] = np.concatenate(( self.history['A'], np.array([A.cpu().item()]) ))
        self.history['B'] = np.concatenate(( self.history['B'], np.array([B.cpu().item()]) ))
        self.history['C'] = np.concatenate(( self.history['C'], np.array([C.cpu().item()]) ))
        self.history['D'] = np.concatenate(( self.history['D'], np.array([D.cpu().item()]) ))
        self.history['fluxes_x0'] = np.concatenate(( self.history['fluxes_x0'], np.array([i_fluxes_d_x0.cpu().item()]) ))
        self.history['fluxes_x1'] = np.concatenate(( self.history['fluxes_x1'], np.array([i_fluxes_d_x1.cpu().item()]) ))
        self.history['fluxes_y0'] = np.concatenate(( self.history['fluxes_y0'], np.array([i_fluxes_d_y0.cpu().item()]) ))
        self.history['fluxes_y1'] = np.concatenate(( self.history['fluxes_y1'], np.array([i_fluxes_d_y1.cpu().item()]) ))
        rho_max = torch.max(self.rho[:,1]).cpu().item()
        self.history['rho_max'] = np.concatenate(( self.history['rho_max'], np.array([(torch.max(self.rho[:,1])).cpu().item()]) ))
        rho_rms = masked_mean(self.rho[:,1]**2, self.mask)**0.5
        self.history['rho_rms'] = np.concatenate(( self.history['rho_rms'], np.array([rho_rms.cpu().item()]) ))

        # store for next iteration
        self.rho[:,0] = self.rho[:,1]
        self.grad_rho[:,0] = self.grad_rho[:,1]
        self.vel[:,0] = self.vel[:,1]

        if self.debug:
            print(f'fluxes C = [{i_fluxes_c_x0.cpu().item()}, {i_fluxes_c_x1.cpu().item()}, {i_fluxes_c_y0.cpu().item()}, {i_fluxes_c_y1.cpu().item()}]')
            print(f'fluxes D = [{i_fluxes_d_x0.cpu().item()}, {i_fluxes_d_x1.cpu().item()}, {i_fluxes_d_y0.cpu().item()}, {i_fluxes_d_y1.cpu().item()}]')
            print()
        
        if self.debug: 
            iteration_name = f"t={i:05d}"
            print(f"rho_rms= {rho_rms:2.2e}")
            print(f"rho_max= {rho_max:2.2e}")
            
            name1 = r"$-(1/\rho_0) \int_{t0}^t \nabla_x p dt$"
            name2 = r"$-(1/\rho_0) \int_{t0}^t \nabla_y p dt$"
            output_file = "vel_" + iteration_name
            levels = np.linspace(-0.0003, 0.0003, 9)
            outname = os.path.join(self.output_debug, output_file)
            X,Y = np.mgrid[0:self.nx,0:self.ny]
            X = X*self.dx
            Y = Y*self.dx

            compare_velocities(self.vel[0,1,0].cpu(), self.vel[0,1,1].cpu(),
                               150, X, Y, self.dx,
                               name1, name2,
                               iteration_name, 
                               outname,
                               levels=levels,
                              )
        

        
        if self.apply_correction:
            _, self.correction, self.int0 = self.ode.solve_o1(i, self.correction, self.int0,
                self.history['B'][i-1:i+1], self.history['C'][i-1:i+1], self.history['D'][i-1:i+1])
            self.history['correction_th'] = np.concatenate(( self.history['correction_th'], np.array([self.correction]) ))
        
        if self.apply_correction: #torch.any(torch.abs(total_fluxes_d) > eps):
            self.history['onoff'] = np.concatenate(( self.history['onoff'], np.array([1.0]) ))
            res = self.rho[:,1][:,None].transpose(-1,-2) + self.correction
        else:
            self.correction = 0.0 
            self.history['onoff'] = np.concatenate(( self.history['onoff'], np.array([0.0]) ))
            res = self.rho[:,1][:,None].transpose(-1,-2)

        self.history['correction'] = np.concatenate(( self.history['correction'], np.array([self.correction]) ))
        
        return res 

    def save(self, folder, filename):
        os.makedirs(folder, exist_ok=True)
        savename = os.path.join(folder, filename)
        np.savez(savename, **self.history)
