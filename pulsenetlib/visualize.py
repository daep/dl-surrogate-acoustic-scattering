#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Plotting functions
"""

# native modules
import os
import copy
import logging
import warnings

# third-party modules
import torch
import numpy as np

import scipy.interpolate

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
import matplotlib.colors
import matplotlib.patches as patches

from pytorch_lightning.loggers import TensorBoardLogger

# local modules
import pulsenetlib.math_tools
import pulsenetlib.logger

log = logging.getLogger(__name__)

class TrainingPanel(object):
    r""" Plot a panel with fields and slices of output and target.
    
    For plotting the predicted and target fields and evaluating
    the quality of the model
    
    ----------
    ARGUMENTS
    
        output: tensor with the model output (predicted) field,
                format (H, W)
        target: tensor with the target field, format (H, W)
        slice_dict: dictionary with the slice to plot properties
                    {'x_slice' or 'y_slice' : location}. Optional,
                    if None, no slices are plotted.
        epoch: integer with the number of the epoch.
               Optional, None as default.
        field_name: string with the field name.
                    Optional; "" as default
        normalization: normalization to be perfomed on the fields.
                       Optional, None as default.
        plotField: boolean for indicating to plot the fields.
                   Optional, True as default
        plotFieldGrad: boolean for indicating to plot the fields
                       gradients. Optional, True as default.
        title: string with the title of the figure.
               Optional, True as default.
    
    ----------
    RETURNS

        ##none##
    
    """
    def __init__(self,
                output, target,
                slice_dict=None,
                epoch=None,
                field_name="",
                normalization=None,
                plotField=True,
                plotFieldGrad=True,
                title=None):
                        
        #output, target = torch.transpose(output, -1, -2), torch.transpose(target, -1, -2)
        self.epoch = epoch

        matplotlib.rc('text')
    
        ############ general plot properties ############
        self.colorbar_label_format = '%.1e'
        self.colorbar_label_size = 10
        self.title_fontsize = 15
        self.label_fontsize = 15
    
        # colors of the line for the slices plot
        slice_output_color = "C0"
        slice_target_color = "C1"
    
        # scalar and error fields colormaps. Set pixel to black if 
        # value is 'nan' or 'inf'
        target_cmap = copy.copy(cm.get_cmap('seismic'))
        target_cmap.set_bad(color='black')

        prediction_cmap = copy.copy(cm.get_cmap('seismic'))
        prediction_cmap.set_bad(color='black')

        err_cmap = copy.copy(cm.OrRd)
        err_cmap.set_bad(color='black')
    
        # field dimension (x in cols, y in rows)
        ny, nx = list(target.shape)[-2:]

        # initiating properties
        plotSlices = False
        self.x_slice = None
        self.y_slice = None

        if slice_dict is not None:
            if len(slice_dict.keys()) != 1:
                raise ValueError('Only one slice allowed in `slice_dict`')
            plotSlices = True

            selected_slice = list(slice_dict.keys())[0]
            slice_position = slice_dict[selected_slice]

            if selected_slice == 'x_slice': self.x_slice = slice_position            
            elif selected_slice == 'y_slice': self.y_slice = slice_position
            else: raise ValueError('Type must be either x_slice or y_slice')
            
        fieldnorm = 1.0
        gradnorm = 1.0
    
        if normalization is not None:
            fieldnorm = normalization[0]
            gradnorm = normalization[1]

        self.formatter = ticker.ScalarFormatter(
            useOffset=True, useMathText=True)
        self.formatter.set_powerlimits((-2,2))
    
        if plotFieldGrad:
            gdl_tar = torch.zeros(1, 2, ny, nx)
            gdl_out = torch.zeros(1, 2, ny, nx)

            # function to select range of plot
            select_range = lambda field: \
                np.max(
                    np.abs(
                        [fun(np.ma.masked_invalid(field))
                            for fun in (np.min, np.max)]
                            )
                    )

            # adapting the prediction and target to the shape necessary
            # for calculating the derivatives (N, C, T, H, W)
            target_to_der = target.permute((0,2,1))
            output_to_der = output.permute((0,2,1))

            # defines a mask based on the nan values
            # if nan's are created during the training, (should not
            # be happening) they will also be ignored when calculating
            # the derivatives and plotting!
            mask = torch.isnan(target).squeeze()
            if torch.all(~mask):
                mask = None
            else:
                #Since the derivation algorithm
                # considers a different standard (True for calculating the 
                # derivation and False for ignoring), the opposite of mask is used
                mask = ~mask

            # Building the derivation operator.
            derivate = pulsenetlib.math_tools.Derivation2D(
                nx, ny, 1, mask=mask, device=target.device)
            
            gdl_tar = gdl_tar.permute((0,1,3,2))
            gdl_out = gdl_out.permute((0,1,3,2))

            gdl_tar[:,0], gdl_tar[:,1] = derivate(target_to_der)
            gdl_out[:,0], gdl_out[:,1] = derivate(output_to_der)

            gdl_tar = gdl_tar.permute((0,1,3,2))
            gdl_out = gdl_out.permute((0,1,3,2))

            gdl_tar = gdl_tar/gradnorm
            gdl_out = gdl_out/gradnorm

            # transforming into numpy arrays
            gdl_tar_x = torch.squeeze(gdl_tar[:,0]).data.numpy()
            gdl_tar_y = torch.squeeze(gdl_tar[:,1]).data.numpy()

            gdl_out_x = torch.squeeze(gdl_out[:,0]).data.numpy()
            gdl_out_y = torch.squeeze(gdl_out[:,1]).data.numpy()

            err_grad_x = np.abs(gdl_out_x - gdl_tar_x)
            err_grad_y = np.abs(gdl_out_y - gdl_tar_y)

            max_err_gradx = np.max(np.ma.masked_invalid(err_grad_x))
            max_err_grady = np.max(np.ma.masked_invalid(err_grad_y))
            # common max - for colorbar
            max_err_grad = np.max([max_err_gradx, max_err_grady])

            # MaxMin grad-x and grad-y
            mmax_gradx_tar = select_range(gdl_tar_x)
            mmax_grady_tar = select_range(gdl_tar_y)

            max_gradx, min_gradx = 1.2*mmax_gradx_tar, -1.2*mmax_gradx_tar
            max_grady, min_grady = 1.2*mmax_grady_tar, -1.2*mmax_grady_tar

        if plotField or plotSlices:
            tar_np = torch.squeeze(target / fieldnorm).data.numpy()
            out_np = torch.squeeze(output / fieldnorm).data.numpy()
            err = (output / fieldnorm - target / fieldnorm)
            err_np = np.abs(torch.squeeze(err).data.numpy())

            mmax_tar = select_range(tar_np)
            max_val_tar, min_val_tar = 1.2*mmax_tar, -1.2*mmax_tar

        if plotSlices:
            # defining slices
            s = np.linspace(0, nx)
            if self.x_slice is not None: f_s = self.x_slice
            if self.y_slice is not None: f_s = self.y_slice

        ncol = 3
        if plotSlices: ncol += 1
        width_ratios = ncol*[1]        

        nrow = 0
        if plotField: nrow += 1
        if plotFieldGrad: nrow += 2
        height_ratios = nrow*[1]

        # starting figure
        # figure properties
        px, py = 700, 400*ny/nx
        dpi = 50
        # figure size (in inches)
        figx, figy = px/dpi, py/dpi
        #self.fig = plt.figure(figsize=(figx, figy), dpi=dpi)
        self.fig = plt.figure(figsize=(figx, figy), constrained_layout=True)

        # defining subplot grid    
        self.gridspec = gridspec.GridSpec(nrow, ncol,
                                          width_ratios=width_ratios,
                                          height_ratios=height_ratios,
                                          wspace=0.5, hspace=0.01,
                                          top=0.95, bottom=0.05,
                                          left=0.05, right=0.95)

        if title is not None:
            if self.epoch is not None:
                self.fig.suptitle(r'$\bf{PulseNet\ output}$' + \
                             ' - Loss = ' + title + \
                             ' - ' + r'$\bf{' + 'Epoch : ' + str(self.epoch) + '}$')
            else:
                self.fig.suptitle(r'$\bf{PulseNet\ output}$' ' - Loss = ' + title)


        # TODO: replace the multiple calls to plot_slice and plot_field by
        # a loop and create a dict with the recurrent parameters (title, colormap, etc)
        it_row = 0
        # plotting scalar field
        if plotField:        
            it_col = 0
            if plotSlices:
                if self.x_slice is not None:
                    line_out = out_np[f_s,:]
                    line_tar = tar_np[f_s,:]
                if self.y_slice is not None:
                    line_out = out_np[:,f_s]
                    line_tar = tar_np[:,f_s]

                mean_y = 0 

                # plotting slices
                self.plot_slice(it_row, it_col,
                                line_out, line_tar, mean_y,
                                axis_title=field_name,
                                axis_label=field_name,
                                color_out=slice_output_color,
                                color_tar=slice_target_color)
                it_col += 1

            # plotting the predicted field
            self.plot_field(it_row, it_col,
                            out_np,
                            axis_title='prediction',
                            colormap=prediction_cmap,
                            colorbar_limits=[min_val_tar,max_val_tar],
                            s=s, f_s=f_s,
                            slice_color=slice_output_color)        
            it_col += 1

            # plotting the target field
            self.plot_field(it_row, it_col,
                            tar_np,
                            axis_title='target',
                            colormap=target_cmap,
                            colorbar_limits=[min_val_tar,max_val_tar],
                            s=s, f_s=f_s,
                            slice_color=slice_target_color)        
            it_col += 1

            # plotting the error field
            self.plot_field(it_row, it_col,
                            err_np,
                            axis_title='PixelSquareError',
                            colormap=err_cmap,
                            colorbar_limits=None,
                            slice_color=None)        
            it_row += 1

        # plotting gradient
        if plotFieldGrad:
            it_col = 0
            if plotSlices:
                # plotting slices of the gradient field
                if self.x_slice is not None:
                    grad_out = gdl_out_x[f_s,:]
                    grad_tar = gdl_tar_x[f_s,:]
                if self.y_slice is not None:
                    grad_out = gdl_out_x[:,f_s]
                    grad_tar = gdl_tar_x[:,f_s]

                mean_y = 0 #np.mean(grad_tar)
                # plotting slices
                self.plot_slice(it_row, it_col,
                                grad_out, grad_tar, mean_y,
                                axis_title=None,
                                axis_label='x-gradient',
                                color_out=slice_output_color,
                                color_tar=slice_target_color)
                it_col += 1

            # plotting the predicted x-gradient
            self.plot_field(it_row, it_col,
                            gdl_out_x,
                            axis_title=None,
                            colormap=prediction_cmap,
                            colorbar_limits=[min_gradx,max_gradx],
                            s=s, f_s=f_s,
                            slice_color=slice_output_color)
            it_col += 1

            # plotting the target x-gradient
            self.plot_field(it_row, it_col,
                            gdl_tar_x,
                            axis_title=None,
                            colormap=target_cmap,
                            colorbar_limits=[min_gradx,max_gradx],
                            s=s, f_s=f_s,
                            slice_color=slice_target_color)
            it_col += 1
            
            # plotting the x-gradient error
            self.plot_field(it_row, it_col,
                            err_grad_x,
                            axis_title=None,
                            colormap=err_cmap,
                            colorbar_limits=[0,max_err_grad],
                            slice_color=None)
            it_row += 1

            it_col = 0
            #x_slice = 90
            if plotSlices:
                if self.x_slice is not None:
                    grad_out = gdl_out_y[f_s,:]
                    grad_tar = gdl_tar_y[f_s,:]
                if self.y_slice is not None:
                    grad_out = gdl_out_y[:,f_s]
                    grad_tar = gdl_tar_y[:,f_s]

                mean_y = 0 # np.mean(grad_tar)            
                self.plot_slice(it_row, it_col,
                                grad_out, grad_tar, mean_y,
                                axis_title=None,
                                axis_label="y-gradient",
                                color_out=slice_output_color,
                                color_tar=slice_target_color)
                it_col += 1

            # plotting the predicted y-gradient
            self.plot_field(it_row, it_col,
                            gdl_out_y,
                            axis_title=None,
                            colormap=prediction_cmap,
                            colorbar_limits=[min_grady,max_grady],
                            s=s, f_s=f_s,
                            slice_color=slice_output_color)
            it_col += 1

            # plotting the target y-gradient
            self.plot_field(it_row, it_col,
                            gdl_tar_y,
                            axis_title=None,
                            colormap=target_cmap,
                            colorbar_limits=[min_grady,max_grady],
                            s=s, f_s=f_s,
                            slice_color=slice_target_color)
            it_col += 1

            # plotting the y-gradient error
            self.plot_field(it_row, it_col,
                            err_grad_y,
                            axis_title=None,
                            colormap=err_cmap,
                            colorbar_limits=[0,max_err_grad],
                            s=s, f_s=f_s,
                            slice_color=None)
            it_row += 1


    def log_figure(self, logger, tag):
        if isinstance(logger, TensorBoardLogger):
            import io
            import PIL.Image
            import torchvision.transforms

            buf = io.BytesIO()
            self.fig.savefig(buf, format='png')
            buf.seek(0)
            image = PIL.Image.open(buf)
            image = torchvision.transforms.ToTensor()(image)

            log.info('Logging: {:}'.format(tag))
            logger.experiment.add_image(tag, image,
                                        global_step=self.epoch,
                                        dataformats='CHW')
    
        if isinstance(logger, pulsenetlib.logger.DiskLogger):
            log.info('Logging: {:}'.format(tag))
            logger.experiment.log_images(tag, self.fig, step=self.epoch)

    def save_figure(self, filename):
        log.info('Saving: {:}'.format(filename))
        self.fig.savefig(filename)
    
    def close_figure(self):
        plt.close(self.fig)

    # Functions defining the plots (field and slice)    
    def plot_field(self,
                   it_row, it_col, field,
                   axis_title,
                   colormap, colorbar_limits,
                   s=None, f_s=None,
                   slice_color=None):
        """ Function to plot a scalar field in the subplot grid

        -----------
        ARGUMENTS

            it_row, it_col: index of the row and column on the
                            subplot grid
            field: 2D array with the scalar field
            axis_title: string with the axis title.
                        None for no title.
            colormap: plot colormap
            colobar_limits: tuple with colorbar limits
            slice_color: color of the line indicating the slice.
                         Optional, none (no plot) as default.

        ----------
        RETURNS

            ##none##

        """

        # selecting the axis in the grid
        ax = plt.subplot(self.gridspec[it_row, it_col])

        if axis_title is not None:
            ax.set_title(axis_title, fontsize=self.title_fontsize)

        # creating axis for the colorbar
        cax = make_axes_locatable(ax).append_axes(
            "right",size="10%",pad="10%")

        # plotting the field
        # if the colorbar limits are not defined, the default
        if colorbar_limits is None:
            im = ax.imshow(field,
                           cmap=colormap,
                           origin="lower",
                           interpolation="none")
        else:
            im = ax.imshow(field,
                           cmap=colormap,
                           origin="lower",
                           interpolation="none",
                           clim=colorbar_limits)

        # adding the colorbar
        cbar = self.fig.colorbar(im,
                            cax=cax,
                            format=self.colorbar_label_format)
        cbar.ax.tick_params(labelsize=self.colorbar_label_size)

        # fixing the axis limits based on the field
        xlim, ylim = ax.get_xlim(), ax.get_ylim()

        if slice_color is not None:
            # adding lines indicating the slices
            if self.x_slice is not None:
                ax.plot(s, [f_s for i in range(len(s))], c=slice_color)
            if self.y_slice is not None:
                ax.plot([f_s for i in range(len(s))], s, c=slice_color)

        # re-setting to original axis limits
        ax.set_xlim(xlim), ax.set_ylim(ylim)
    
    def plot_slice(self,
                   it_row, it_col,
                   line_out, line_tar, mean_value,
                   axis_title=None, axis_label=None,
                   color_out=None, color_tar=None):
        """ Function to plot a slice (lines) in the subplot grid

        ----------
        ARGUMENTS

            it_row, it_col: index of the row and column on
                            the subplot grid
            line_out: output values for the slice
            line_tar: target values for the slice
            mean_value: value to be removed for both output
                        and target
            axis_title: string with the axis title.
                        Optional, None (no title) as default
            axis_label: string with the y-axis label.
                        Optional, None (no label) as default

        ----------
        RETURNS

            ##none##

        """

        ax = plt.subplot(self.gridspec[it_row,it_col])

        ax.plot(line_out - mean_value,
                label="output", c=color_out)
        ax.plot(line_tar - mean_value,
                label="target", c=color_tar)

        ax.yaxis.set_major_formatter(self.formatter)

        x0, x1 = ax.get_xlim()
        y0, y1 = ax.get_ylim()
        ax.set_aspect((x1 - x0)/(y1 - y0))

        ax.legend()

        if axis_title is not None:
            ax.set_title(axis_title, fontsize=self.title_fontsize)
        if axis_label is not None:
            ax.set_ylabel(axis_label, fontsize=self.label_fontsize)
