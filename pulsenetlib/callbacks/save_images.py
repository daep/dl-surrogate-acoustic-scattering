#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Callback for saving individual predictions during training as images
"""

# native modules
import os
import logging
from typing import Iterable, List, Optional

# third-party modules
import numpy as np
import torch
from torch.utils.data import DataLoader, RandomSampler
from pytorch_lightning.callbacks.base import Callback
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.utilities.exceptions import MisconfigurationException
from pytorch_lightning.utilities import rank_zero_only, rank_zero_warn

# local modules
import pulsenetlib.visualize
from pulsenetlib.logger import DiskLogger

log = logging.getLogger(__name__)

class SaveTrainingImage(Callback):
    r"""
    Save selected images of predictions during validation.
    Automatically logs images to Lightning Trainer logger,
    when available.

    Args:

        channels: available channels in data.
        channels_to_plot: channels to plot among the
                          available ones, defined in ``'channels'``.
        plot_list: list containing the parameters
                   passed to the image plotter. Each element
                   is composed of a list of 3 elements:
                    - batch index
                    - `x_slice` or `y_slice` strings
                    - coordinate of slice.

                   Example::

                   # Plot data index 10 with a x-slice at y=25 and
                   # data index 40 with a y-slice at x=75 
                   >>> plot_list = [
                        [10, 'x_slice', 25],
                        [40, 'y_slice', 75]
                      ]

        freq_to_file: logging frequency in epochs.
        loader: dataloader for which predictions are plotted.
        output_to_disk: Optional, if True, saves the images in
                        `save_dir`.
        save_dir: Optional, directory where images are saved when
                  `output_to_disk=True`.
    
    """
    def __init__(self,
            channels : Iterable[str],
            channels_to_plot : Iterable[str],
            plot_list : Iterable[List],
            freq_to_file : int,
            loader : DataLoader,
            output_to_disk : Optional[bool] = False,
            save_dir : Optional[str] = None,
        ):
        super().__init__()
        self.channels = channels
        self.channels_to_plot = channels_to_plot
        self.plot_list = plot_list
        self.freq_to_file = freq_to_file
        
        self.length_training_batchs = len(loader)
        self.batch_size = loader.batch_size
        if isinstance(loader.sampler, RandomSampler):
            rank_zero_warn(f'DataLoader has shuffle=True. '
                           f'Save Images will not be the same between two epochs.')

        self.save_dir = save_dir
        
        self.output_to_disk = output_to_disk
        if output_to_disk:
            if save_dir is None: 
                raise ValueError(
                    '`save_dir` must be set when `output_to_disk` is True.'
                    )
            os.makedirs(save_dir, exist_ok=True)


    @rank_zero_only
    def on_init_end(self, trainer):
        # check for loggers
        if trainer.logger is not None:
            self.loggers = trainer.loggers
            self.output_to_logger = True
            self.output_mode = 'logger'
        else:
            self.output_to_logger = False
            self.output_mode = 'disk'
        if not (self.output_to_disk or self.output_to_logger):
            raise MisconfigurationException(
            'Either `output_to_disk` is True or a logger is passed to pl.Trainer'
         )
        
        # defining channel index based on channel name
        self.index_channels_to_plot = []
        for channel_to_plot in self.channels_to_plot:
            if not channel_to_plot in self.channels:
                raise ValueError(
                    f'Channel to plot ({channel_to_plot}) is not available '
                    f' in channels ({self.channels})'
            )
            self.index_channels_to_plot.append(
                np.argwhere([channel_to_plot == channel for
                     channel in self.channels])[0][0])
        
        self._print_info()

        # setup the mask (minibatch and inside minibatch) 
        # for plotting the error panel
        self.minibatch_mask = \
            self._prepare_training_print(
                self.plot_list, self.batch_size, self.length_training_batchs)
        self.minibatch_mask = torch.tensor(
            self.minibatch_mask, dtype=torch.bool)     
        
        # dictionaries defining the properties of the slices 
        # to be plotted
        self.slice_dicts = []
        for slice_prop in self.plot_list: 
            self.slice_dicts.append({slice_prop[1]:slice_prop[2]}) 
        
        
    def _print_info(self):
        log.info('\n{:^72}\n'.format('#TRAINING#MONITORING#')\
              .replace(' ','-').replace('#',' '))
        log.info('Output mode: ')
        output_mode_names = \
            {'logger':f'mode = Save to logger {self.loggers}',
             'disk':'mode = Save to disk'}
        log.info(f'{output_mode_names[self.output_mode]} \n')
        log.info(f'Channels to plot: {self.channels_to_plot}')


    @rank_zero_only
    def on_validation_epoch_start(self, trainer, pl_module):
        self.epoch = pl_module.current_epoch
        self.print_this_epoch = False
        if (self.epoch % self.freq_to_file == 0):
            self.index_batch_plot = 0
            self.print_this_epoch = True
        self.index_slice_plot = 0


    @rank_zero_only
    def on_validation_batch_end(self, trainer, pl_module, outputs,
                                batch, batch_idx, dataloader_idx):
        # if any element on this batch is True do the plotting   
        if self.print_this_epoch and \
                torch.any(self.minibatch_mask[batch_idx,:]):

            try:
                out = pl_module.last_prediction.cpu()
            except AttributeError as e:
                m = """please track the last_prediction in the training_step like so:
                    def training_step(...):
                        self.last_prediction = your_prediction
                """
                raise AttributeError(m)
            try:
                target = pl_module.last_target.cpu()
            except AttributeError as e:
                m = """please track the last_prediction in the training_step like so:
                    def training_step(...):
                        self.last_target = your_target
                """
                raise AttributeError(m)

            # defining index of elements inside batch
            indices = self.minibatch_mask[batch_idx].nonzero(as_tuple=False)[:,0]
            
            # loop over channels (fields) 
            for index_channel_plot in self.index_channels_to_plot:
                # extracting data to be plotted             
                out_print = torch.index_select(
                    out[:,index_channel_plot], 0, indices)
                target_print = torch.index_select(
                    target[:,index_channel_plot], 0, indices)

                # provisional solution for 3D fields
                if target.dim() == 6:
                    target_print = target_print[:,:,int(target_print.size(2)/2),:,:]
                    out_print = out_print[:,:,int(out_print.size(2)/2),:,:]
         
                # loop over individual indexes in this minibatch
                for k in range(len(indices)):
                    # getting slice properties
                    if self.slice_dicts:
                        slice_dict = self.slice_dicts[self.index_slice_plot]
                    else:
                        slice_dict = None

                    with torch.no_grad():
                        # plot panel with the preliminary results
                        panel = pulsenetlib.visualize.TrainingPanel(
                                output=out_print[k],
                                target=target_print[k],
                                slice_dict=slice_dict,
                                epoch=self.epoch,
                                field_name=self.channels[index_channel_plot],
                                plotField=True,
                                plotFieldGrad=True,
                                title=None,
                            )
                    
                    
                    if self.output_to_logger:
                        # don't save image twice, only save it in tb if both loggers
                        # are employed
                        for logger in pl_module.loggers:
                            if (isinstance(logger, TensorBoardLogger)):
                                figure_filename = \
                                'output_{:}_val_{:05d}.png'.format(
                                        self.channels[index_channel_plot],
                                        self.plot_list[self.index_slice_plot][0]
                                        )
                                panel.log_figure(logger, figure_filename)
                            elif (isinstance(logger, DiskLogger)):
                                figure_filename = \
                                'output_{:}_val_{:05d}_epoch={:04d}.png'.format(
                                        self.channels[index_channel_plot],
                                        self.plot_list[self.index_slice_plot][0],
                                        self.epoch)
                                panel.log_figure(logger, figure_filename)
                            else:
                                continue
                    if self.output_to_disk:
                        filename = os.path.join(
                            self.save_dir, figure_filename)
                        panel.save_figure(filename)
                    
                    panel.close_figure()    

                    # update the index of the slice to plot
                    self.index_slice_plot += 1
        
            # update the index of the batch to plot
            self.index_batch_plot += 1



    def _prepare_training_print(self, plot_list, batch_size, number_batchs):
        """ Creates a mask array indicating the elements to be plotted.

        Does not account for a last batch not being complete. User must
        pay attention to not ask for elements that do not exist.

        ----------
        ARGUMENTS

            plot_list: list of lists with the slice plot
                       properties [index of the case, type of slice,
                       position of the slice]
            batch_size: size of the full batch
            number_batchs: number of batchs in the loader.


        ----------
        RETURNS

            minibatch_mask: array of booleans indicating the elements
                            to be plotted, format (number batches, batch size). 

        """

        minibatch_mask = np.zeros(
            (number_batchs, batch_size), dtype=bool)
        
        log.info('Cases and slices to plot:\n')
        for item in plot_list:
            # define the minibatch index and the index inside the
            # minibatch
            minibatch_idx = item[0] // batch_size
            sub_idx = item[0] % batch_size

            log.info('  index: {:}'.format(item[0]))
            log.info('  type: {:}'.format(item[1]))
            log.info('  slice: {:}'.format(item[2]))
            log.info('  minibatch: {:}'.format(minibatch_idx))
            log.info('  index in minibatch: {:}\n'.format(sub_idx))

            minibatch_mask[minibatch_idx, sub_idx] = True

        return minibatch_mask
