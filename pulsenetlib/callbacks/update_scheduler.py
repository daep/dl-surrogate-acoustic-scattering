#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Lightning Callbacks
"""

# native modules
import logging

# third-party modules
import torch
import pytorch_lightning as pl
from pytorch_lightning.callbacks import Callback

from pytorch_lightning.utilities import rank_zero_only
# local modules
##none##

log = logging.getLogger(__name__)

class UpdateScheduler(Callback):
    """ Callback for changing scheduler in the middle of a training
    From: https://github.com/PyTorchLightning/pytorch-lightning/issues/3095

    ----------
    ARGUMENTS
        new_scheduler:

    """
    def __init__(self, new_scheduler : torch.optim.lr_scheduler._LRScheduler):
                
        self.new_scheduler = new_scheduler
        self.first_call = True
        
    def on_train_epoch_start(self, trainer, pl_module):
        if self.first_call:
            log.info(f'Updating scheduler at epoch {trainer.current_epoch}')
            log.info(f'New scheduler is: {self.new_scheduler}')
            scheduler = {
                    'scheduler': self.new_scheduler,
                    'monitor': 'val_loss',
                    'interval': 'epoch',
                    'frequency': 1
                }
            trainer.lr_schedulers = trainer._configure_schedulers(
                [scheduler],
                scheduler['monitor'],
                not pl_module.automatic_optimization
            )
            self.first_call = False