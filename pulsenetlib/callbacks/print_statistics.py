#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Lightning Callbacks
"""

# native modules
import glob
import os
import datetime
import logging

# third-party modules
import numpy as np
import torch
from pytorch_lightning.callbacks import Callback

from pytorch_lightning.utilities import rank_zero_only
# local modules
##none##

log = logging.getLogger(__name__)

class PrintStatistics(Callback):
    """ Class defining operations that display the evolution training and validation.

    ----------
    ARGUMENTS

        stats_frequency: refresing frequency to print the progress of
                         training/validation in terms o batches in the epoch.
                         Default (1) indicates that a print is performed
                         at the end of every batch. Only applies if verbose
                         is true
        verbose: boolean to select a verboragic class. If True,
                 losses and time stats (average epoch time, remaining time
                 and estimated time of arrival (ETA) are displayed after every
                 epoch end

    """

    def __init__(self,stats_frequency : int = 1,
                 verbose : bool = False ):
        self.stats_frequency = stats_frequency
        self.verbose = verbose
        
        self.num_train_eval = 0
        self.num_valid_eval = 0                

        self.cumulative_epoch_time = datetime.timedelta()

    @rank_zero_only
    def on_epoch_start(self, trainer, pl_module):
        self.epoch_start_date = datetime.datetime.now() 
        log.info('\nepoch: {:5d}'.format(pl_module.current_epoch))

       
    @rank_zero_only
    def on_train_epoch_start(self, trainer, pl_module):
        self.num_train_eval = 0
        log.info('| training...')

    
    @rank_zero_only
    def on_validation_epoch_start(self, trainer, pl_module):
        self.num_valid_eval = 0
        log.info('| validation...')

 
    @rank_zero_only
    def on_train_batch_end(self, trainer, pl_module, outputs,
                           batch, batch_idx, dataloader_idx):
        if self.verbose:
            self.num_train_eval += batch[0].shape[0]
            if batch_idx % self.stats_frequency == 0:
                self._print_perc(self.num_train_eval,
                                 len(trainer.datamodule.train_dataloader().dataset))

               
    @rank_zero_only
    def on_validation_batch_end(self, trainer, pl_module, outputs,
                                batch, batch_idx, dataloader_idx):
        if self.verbose:
            self.num_valid_eval += batch[0].shape[0]
            if batch_idx % self.stats_frequency == 0:
                self._print_perc(self.num_valid_eval,
                                 len(trainer.datamodule.val_dataloader().dataset))
              

    @rank_zero_only
    def _print_perc(self, num_performed, num_total):
        """ Print evolution of run in terms of datapoints """
        log.info('|   {:05d}/{:05d}'.format(
              num_performed, num_total)
        )


    @rank_zero_only
    def on_epoch_end(self, trainer, pl_module):
        """ Print total losses and training time stats """
        ellapsed_time = datetime.datetime.now() - self.epoch_start_date
        self.cumulative_epoch_time += ellapsed_time
       
        average_epoch_time = \
            self.cumulative_epoch_time/(pl_module.current_epoch + 1)
        remaining_time = \
            ((trainer.max_epochs - 1) - pl_module.current_epoch)*average_epoch_time 
        estimated_time_arrival = datetime.datetime.now() + remaining_time
        
        if self.verbose:
            log.info('train/val losses: {:12.6e}'.format(
                trainer.callback_metrics['val_loss'])
            )
            log.info(('{:^20} '*3 + '\n' + '{:^20} '*3).format(
                'avg epoch time','remaining time','ETA',
                str(average_epoch_time)[:-4],
                str(remaining_time)[:-5],
                str(estimated_time_arrival)[:-7])
            ) 
 

