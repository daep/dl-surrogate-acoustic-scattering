#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Callback for saving individual predictions during training as vtk files
"""

# native modules
import os
import logging
from typing import Iterable, List, Optional

# third-party modules
import numpy as np
import torch
from torch.utils.data import DataLoader, RandomSampler
from pytorch_lightning.callbacks.base import Callback
from pytorch_lightning.loggers import TensorBoardLogger, LoggerCollection
from pytorch_lightning.utilities.exceptions import MisconfigurationException
from pytorch_lightning.utilities import rank_zero_only, rank_zero_warn
import importlib.util

# third-party modules
# none

# local modules
import pulsenetlib.visualize
from pulsenetlib.logger import DiskLogger

log = logging.getLogger(__name__)

class SaveTrainingVTK(Callback):
    try:
        import pyevtk.hl as vtk
    except ModuleNotFoundError:
        raise ImportError("Please install the pyevtk library if you want to use the SaveTrainingVTK callback")
    
    r"""
    Save selected images of predictions during validation.
    Automatically logs images to Lightning Trainer logger,
    when available.

    Args:

        channels: available channels in data.
        channels_to_plot: channels to plot among the
                          available ones, defined in ``'channels'``.
        output_list: list of batch indexes to output
        freq_to_file: logging frequency in epochs.
        loader: dataloader for which predictions are plotted.
        save_dir: Optional, directory where images are saved when
                  `output_to_disk=True`.
    
    """
    def __init__(self,
            channels : Iterable[str],
            channels_to_plot : Iterable[str],
            output_list : Iterable[List],
            freq_to_file : int,
            loader : DataLoader,
            save_dir : str
        ):
        super().__init__()
        self.channels = channels
        self.channels_to_plot = channels_to_plot
        self.output_list = output_list
        self.freq_to_file = freq_to_file
        
        self.length_training_batchs = len(loader)
        self.batch_size = loader.batch_size
        if isinstance(loader.sampler, RandomSampler):
            rank_zero_warn(f'DataLoader has shuffle=True. '
                           f'Save Images will not be the same between two epochs.')

        self.save_dir = save_dir
        os.makedirs(save_dir, exist_ok=True)


    @rank_zero_only
    def on_init_end(self, trainer):
        # defining channel index based on channel name
        self.index_channels_to_plot = []
        for channel_to_plot in self.channels_to_plot:
            if not channel_to_plot in self.channels:
                raise ValueError(
                    f'Channel to save ({channel_to_plot}) is not available '
                    f' in channels ({self.channels})'
                )
            self.index_channels_to_plot.append(
                np.argwhere([channel_to_plot == channel for
                     channel in self.channels])[0][0])
        
        self._print_info()

        # setup the mask (minibatch and inside minibatch) 
        # for plotting the error panel
        self.minibatch_mask = \
            self._prepare_training_print(
                self.output_list, self.batch_size, self.length_training_batchs)
        self.minibatch_mask = torch.tensor(
            self.minibatch_mask, dtype=torch.bool)     
        
        
    def _print_info(self):
        log.info('\n{:^72}\n'.format('#TRAINING#MONITORING#')\
              .replace(' ','-').replace('#',' '))
        log.info(f'{output_mode_names[self.output_mode]} \n')
        log.info(f'Channels to save in VTK: {self.channels_to_plot}')


    @rank_zero_only
    def on_validation_epoch_start(self, trainer, pl_module):
        self.epoch = pl_module.current_epoch
        self.print_this_epoch = False
        if (self.epoch % self.freq_to_file == 0):
            self.index_batch_plot = 0
            self.print_this_epoch = True
        self.index_slice_plot = 0


    @rank_zero_only
    def on_validation_batch_end(self, trainer, pl_module, outputs,
                                batch, batch_idx, dataloader_idx):
        # if any element on this batch is True do the plotting   
        if self.print_this_epoch and \
                torch.any(self.minibatch_mask[batch_idx,:]):

            try:
                out = pl_module.last_prediction.cpu()
            except AttributeError as e:
                m = """please track the last_prediction in the training_step like so:
                    def training_step(...):
                        self.last_prediction = your_prediction
                """
                raise AttributeError(m)
            try:
                target = pl_module.last_target.cpu()
            except AttributeError as e:
                m = """please track the last_prediction in the training_step like so:
                    def training_step(...):
                        self.last_target = your_target
                """
                raise AttributeError(m)

            # defining index of elements inside batch
            indices = self.minibatch_mask[batch_idx].nonzero(as_tuple=False)[:,0]
            
            # loop over individual indexes in this minibatch
            for k in range(len(indices)):
                # loop over channels (fields) 
                for index_channel_plot in self.index_channels_to_plot:
                    # extracting data to be plotted             
                    out_print = torch.index_select(
                        out[:,index_channel_plot], 0, indices)
                    target_print = torch.index_select(
                        target[:,index_channel_plot], 0, indices)

                    # provisional solution for 3D fields
                    if target.dim() == 6:
                        target_print = target_print[:,:,int(target_print.size(2)/2),:,:]
                        out_print = out_print[:,:,int(out_print.size(2)/2),:,:]

                    grid = {}
                    with torch.no_grad():
                        # plot panel with the preliminary results
                        grid_temp, x, y, z = self._create_vtk_grid(
                                output=out_print[k],
                                target=target_print[k],
                                field_name=self.channels[index_channel_plot],
                                outputField=True,
                                outputFiedGrad=True,
                            )
                        grid.update(grid_temp)
                    
                    filename = os.path.join(self.save_dir,
                            'output_{:}_val_{:05d}_epoch_{:04d}'.format(
                                self.channels[index_channel_plot],
                                self.output_list[self.index_slice_plot],
                                self.epoch))

                    vtk.gridToVTK(filename, x, y, z, pointData = grid)

                    self.index_slice_plot +=1

            # update the index of the batch to plot
            self.index_batch_plot += 1



    def _prepare_training_print(self, output_list, batch_size, number_batchs):
        """ Creates a mask array indicating the elements to be plotted.

        Does not account for a last batch not being complete. User must
        pay attention to not ask for elements that do not exist.

        ----------
        ARGUMENTS

            output_list: list of lists with the slice plot
                       properties [index of the case, type of slice,
                       position of the slice]
            batch_size: size of the full batch
            number_batchs: number of batchs in the loader.


        ----------
        RETURNS

            minibatch_mask: array of booleans indicating the elements
                            to be plotted, format (number batches, batch size). 

        """

        minibatch_mask = np.zeros(
            (number_batchs, batch_size), dtype=bool)
        
        log.info('Cases and slices to plot:\n')
        for item in output_list:
            # define the minibatch index and the index inside the
            # minibatch
            minibatch_idx = item // batch_size
            sub_idx = item % batch_size

            log.info('  index: {:}'.format(item))
            log.info('  minibatch: {:}'.format(minibatch_idx))
            log.info('  index in minibatch: {:}\n'.format(sub_idx))

            minibatch_mask[minibatch_idx, sub_idx] = True

        return minibatch_mask

    def _create_vtk_grid(self,
                output, target,
                field_name="",
                normalization=None,
                outputField=True,
                outputFiedGrad=True):

        output = output.transpose(-1, -2)
        target = target.transpose(-1, -2)

        nx, ny = list(target.shape)[-2:]

        lx, ly = nx-1, ny-1 
        dx, dy = lx/(nx-1), ly/(ny-1)

        # Coordinates
        x = np.arange(0, lx + 0.1*dx, dx, dtype='float32')
        y = np.arange(0, ly + 0.1*dy, dy, dtype='float32')
        z = np.zeros(1, dtype='float32')
        
        fieldnorm = 1.0
        gradnorm = 1.0
    
        if normalization is not None:
            fieldnorm = normalization[0]
            gradnorm = normalization[1]

        grid = {}

        if outputFiedGrad:
            gdl_tar = torch.zeros(1, 2, nx, ny)
            gdl_out = torch.zeros(1, 2, nx, ny)
        
            target_to_der = target.unsqueeze(0).unsqueeze(0)
            output_to_der = output.unsqueeze(0).unsqueeze(0)
        
            mask = torch.isnan(target).squeeze()

            # Building the derivation operator. Since the derivation algorithm
            # considers a different standard (True for calculating the 
            # derivation and False for ignoring), the opposite of mask is used
            derivate = pulsenetlib.math_tools.Derivation2D(
                nx, ny, 1, mask=~mask, device=target.device)

            gdl_tar[:,0], gdl_tar[:,1] = derivate(target_to_der)
            gdl_out[:,0], gdl_out[:,1] = derivate(output_to_der)

            gdl_tar = gdl_tar/gradnorm
            gdl_out = gdl_out/gradnorm

            gdl_tar[mask[None,None,:,:].expand(1,2,ny,nx)] = float('nan')
            gdl_out[mask[None,None,:,:].expand(1,2,ny,nx)] = float('nan')
            
            # transforming into numpy arrays
            grid[field_name + '_grad_x_tar'] = np.ascontiguousarray(torch.squeeze(gdl_tar[:,0])[:,:,None].data.numpy())
            grid[field_name + '_grad_y_tar'] = np.ascontiguousarray(torch.squeeze(gdl_tar[:,1])[:,:,None].data.numpy())

            grid[field_name + '_grad_x_out'] = np.ascontiguousarray(torch.squeeze(gdl_out[:,0])[:,:,None].data.numpy())
            grid[field_name + '_grad_y_out'] = np.ascontiguousarray(torch.squeeze(gdl_out[:,1])[:,:,None].data.numpy())
        
        if outputField:
            mask = torch.isnan(target).squeeze()
            
            grid[field_name + '_mask'] = np.ascontiguousarray(torch.squeeze(mask)[:,:,None].data.numpy().astype('float32'))
            grid[field_name + '_field_tar'] = np.ascontiguousarray(torch.squeeze(target / fieldnorm)[:,:,None].data.numpy())
            grid[field_name + '_field_out'] = np.ascontiguousarray(torch.squeeze(output / fieldnorm)[:,:,None].data.numpy())


        return grid, x, y, z