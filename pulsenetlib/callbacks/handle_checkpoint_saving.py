#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Handle checkpoint saving.
"""

# native modules
import os
import re
import logging

# third-party modules
import yaml
from pytorch_lightning.callbacks import Callback, ModelCheckpoint
from pytorch_lightning.utilities import rank_zero_only, rank_zero_warn

# local modules
##none##

log = logging.getLogger(__name__)



class HandleCheckpointSaving(Callback):
    def __init__(self, best_save_top_k):
        self.best_save_top_k = best_save_top_k

    def _select_to_discard(self, best_list, save_top_k):
        num_ckpt = len(best_list)
        if save_top_k > num_ckpt:
            save_top_k = num_ckpt
        return best_list[:num_ckpt-save_top_k]

    @rank_zero_only
    def to_yaml(self, checkpoint, filepath = None) -> None:
        """Saves the `best_k_models` dict containing the checkpoint paths with the corresponding scores to a YAML
        file."""
        best_k = {os.path.basename(k): v.item() for k, v in checkpoint.best_k_models.items()}
        if filepath is None:
            filepath = os.path.join(checkpoint.dirpath, "best_k_models.yaml")

        if os.path.exists(filepath):
            with checkpoint._fs.open(filepath, "r") as fp:
                old_best_k = yaml.load(fp, Loader=yaml.FullLoader)
            # converting into "new" format, so that it does not show
            # all path
            old_best_k = {os.path.basename(k): v for k, v in old_best_k.items()}
            best_k = {**old_best_k, **best_k}

            key_list = list(best_k.keys())
            r = re.compile("best*")
            best_list = sorted(list(filter(r.match, key_list)))
            discard_list = self._select_to_discard(best_list, self.best_save_top_k)
            
            for k in discard_list:
                print(f'Deleting key {k}') 
                del best_k[k]
                path_to_delete = os.path.join(checkpoint.dirpath, k)
                if os.path.isfile(path_to_delete):
                    os.remove(path_to_delete)

            with checkpoint._fs.open(filepath, "w") as fp:
                yaml.dump(best_k, fp)
        else:
            with checkpoint._fs.open(filepath, "w") as fp:
                yaml.dump(best_k, fp)


    @rank_zero_only
    def on_epoch_start(self, trainer, pl_module):
        # save yaml with k best checkpoints at validation end after last checkpoint
        checkpoint_callbacks = [c for c in
            trainer.callbacks if isinstance(c, ModelCheckpoint)]
    
        epoch = pl_module.current_epoch

        for c in checkpoint_callbacks:
            if (epoch + 1)% c._every_n_epochs == 0:
                log.info(f'At epoch {epoch}: saving yaml with best checkpoints')
                self.to_yaml(c)

    def on_keyboard_interrupt(self, trainer, pl_module):
        # disable logger and checkpoint when interrupting in the middle on an epoch
        # so that it does not mess logs.
        trainer.logger = None
        rank_zero_warn(f'Detected KeyboardInterrupt: disabling metrics logging '
                       f'for current epoch')
        
        checkpoint_callbacks = [c for c in
            trainer.callbacks if isinstance(c, ModelCheckpoint)]
        
        for c in checkpoint_callbacks:
            self.to_yaml(c)
            c._every_n_epochs = -1
        
        rank_zero_warn(f'Detected KeyboardInterrupt: disabling model checkpoint '
                       f'for current epoch')