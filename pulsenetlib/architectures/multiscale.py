#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Multi-Scale CNN with arbitrary number of scales and layers
    Mathieu, M., Couprie, C., & LeCun, Y. (2016). 
    Deep multi-scale video prediction beyond mean square error. 
    4th International Conference on Learning Representtions, ICLR.
    http://arxiv.org/abs/1511.05440
"""

# native modules
import logging
from typing import List, Mapping, Sequence

# third-party modules
import torch
import torch.nn as nn
import torch.nn.functional as F

# local modules
##none##

log = logging.getLogger(__name__)

def _make_scale(scale_factor : int,
                kernel_sizes : List,
                in_channels : List,
                out_channels : List,
                activations: List,
                padding_mode='zeros'):
    r"""Sequential CNN scale used for multi-scale CNN.

    The following arguments should have the same list length:
    'kernel_size', 'in_channels', 'out_channels', 'activations'.

    Inputs:
        kernel_sizes: list of integers defining the kernel sizes of the
                      sequence of convolution blocks.
        in_channels: list of integers defining the number of input features of the
                      sequence of convolution blocks.
        out_channels: list of integers defining the number of output features of the
                      sequence of convolution blocks.
        activations: list of booleans defining if a non-linear (ReLU) activation is
                     used (True) or not (False) for the sequence of convolution blocks.
        padding_mode: padding mode
    """

    if len(kernel_sizes) != len(activations):
        raise ValueError(f'"kernels_sizes" (len = {len(kernel_sizes)}) '
                         f'must have the same'
                         f'length as "activations" (len = {len(activations)})')
    
    if not isinstance(scale_factor, int):
       raise ValueError(f'Scale factor ({scale_factor}) should be an integer number')

    layers = []
    
    for kernel, in_c, out_c, activ in zip(kernel_sizes, in_channels, out_channels, activations):
        activation = lambda: nn.ReLU() if activ else nn.Identity()
        padding =int((kernel-1)/2)
        layers.append(
            nn.Conv2d(in_c, out_c, kernel_size=kernel, padding=padding, padding_mode=padding_mode)
        )
        layers.append(
            activation()
        )
    return layers

def _check_dict_consistency(scales_dict):
    scale_keys = {'scale_factor', 'kernel_sizes', 'in_channels', 'out_channels', 'activations'}
    for key in scales_dict.keys():
        if key not in scale_keys: 
            raise ValueError(f'Key "{key}" not recognized. '
                             f'Scales dictionary should have the following keys: '
                             f'{scale_keys}')
        
    scale_keys_list = scale_keys.copy()
    scale_keys_list.remove('scale_factor')
    len_temp = []
    log = {}
    for key in scale_keys_list:
        len_temp.append(len(scales_dict[key]))
        log[key] = len(scales_dict[key])
    if len(set(len_temp)) > 1:
        raise ValueError(f'The following keys ({scale_keys_list}) '
                         f'in scales_dict {scales_dict} '
                         f'should have the same list length '
                         f'(found lengths {log})' )


def _build_multiscale(layers_description, padding_mode):
    layers = []
    log.info(f'Building multi-scale network...')
    for s, scale_dict in enumerate(layers_description):
        log.info(f'Building scale {s}')
        _check_dict_consistency(scale_dict)
        layers.append(_make_scale(**scale_dict, padding_mode=padding_mode))

    log.info(f'Multi-scale network succesfully built.\n')
    return layers


class MultiScaleNet(nn.Module):
    r""" Multi-scale convolutional neural network as used by 
    Mathieu, Couprie and LeCun (2016)

    Arguments:
        layers_description: list of dictionaries with arguments to build network.
                            Each dict contains the arguments for one scale of 
                            convolutions. The number of dictionaries corresponds
                            to the number of scales.

                            For each scale; the input is downsampled by a scaling factor
                            and output is upsampled back to the original size.

                            Dictionary keys:
                            scale_factor: integer target domain size at which
                                          convolutions will be performed.
                            kernel_sizes: list of integers defining the kernel
                                          sizes of the sequence of convolution blocks.
                            in_channels: list of integers defining the number
                                          of input features of the
                                          sequence of convolution blocks.
                            out_channels: list of integers defining the number
                                          of output features of the
                                          sequence of convolution blocks.
                            activations: list of booleans defining if
                                         a non-linear (ReLU) activation is
                                         used (True) or not (False) for
                                         the sequence of convolution blocks.
        padding_mode : padding mode
         
    """
    def __init__(self, layers_description: Sequence[Mapping], padding_mode):
        super(MultiScaleNet, self).__init__()

        scales = _build_multiscale(layers_description, padding_mode)

        self.scale_names = []
        self.scale_factors = []
        for i, scale in enumerate(scales):
            name = f'Scale_{i}'
            self.scale_names.append(name)
            self.scale_factors.append(layers_description[i]['scale_factor'])
            setattr(self, name, nn.Sequential(*scale))

    def forward(self, x):
        input_size = list(x.size()[2:])
        for name, factor in zip(self.scale_names, self.scale_factors):
            if name == 'Scale_0':
                x_input_interp = x
                if factor > 1:
                    scale_size = [int(j/factor) for j in list(x.size()[2:])]
                    x_input_interp = F.interpolate(x, size=scale_size, mode='bilinear', align_corners=False)
                x_i_1 = getattr(self, name)(x_input_interp)
                if factor > 1:
                    x_i_1 = F.interpolate(x_i_1, size=input_size, mode='bilinear', align_corners=False)

            else:
                x_input_interp = x
                if factor > 1:
                    scale_size = [int(j/factor) for j in list(x.size()[2:])]
                    x_input_interp = F.interpolate(x, size=scale_size, mode='bilinear', align_corners=False)
                    x_i_1 = F.interpolate(x_i_1, size=scale_size, mode='bilinear', align_corners=False)
                x_i_1 = getattr(self, name)(
                    torch.cat((x_input_interp, x_i_1), dim=1)
                )
                if factor > 1:
                    x_i_1 = F.interpolate(x_i_1, size=input_size, mode='bilinear', align_corners=False)
        return x_i_1

if __name__ == "__main__":
    test_dict = [
        {
            'scale_factor': 4,
            'kernel_sizes':[3, 3],
            'in_channels':[4,16],
            'out_channels':[16,1],
            'activations':[True,False]
        },
        {
            'scale_factor': 2,
            'kernel_sizes':[3, 3],
            'in_channels':[5,16],
            'out_channels':[16,1],
            'activations':[True,False]
        },
        {
            'scale_factor': 1,
            'kernel_sizes':[3, 3],
            'in_channels':[5,16],
            'out_channels':[16,1],
            'activations':[True,False]
        }

    ]

    net = MultiScaleNet(test_dict)
    log.info(net)
    input = torch.rand((1, 4, 33, 33))
    y = net(input)
    log.info(y.shape)
