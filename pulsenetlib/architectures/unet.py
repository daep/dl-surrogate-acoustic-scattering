#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    U-Net CNN with arbitrary number of scales and layers
    Ronneberger, O., Fischer, P., & Brox, T. (2015). 
    U-Net: Convolutional Networks for Biomedical Image Segmentation. 
    Medical Image Computing and Computer-Assisted Intervention, 234–241.
    https://doi.org/10.1109/ACCESS.2021.3053408
"""

# native modules
import logging
from typing import List, Mapping, Sequence

# third-party modules
import torch
import torch.nn as nn
import torch.nn.functional as F

from collections import OrderedDict

log = logging.getLogger(__name__)

#Create the model

def get_activation(name, *args):
    if name in ['tanh', 'Tanh']:
        return lambda: nn.Tanh()
    elif name in ['identity', 'Identity', 'id']:
        return lambda: nn.Identity()
    elif name in ['relu', 'ReLU']:
        return lambda: nn.ReLU(inplace=True)
    elif name in ['lrelu', 'LReLU']:
        return lambda: nn.LeakyReLU(inplace=True)
    elif name in ['sigmoid', 'Sigmoid']:
        return lambda: nn.Sigmoid()
    elif name in ['softplus', 'Softplus']:
        return lambda: nn.Softplus(beta=4)
    else:
        raise ValueError('Unknown activation function')

def _make_scale(scale_factor : int,
                down_kernel_sizes : List,
                down_in_channels : List,
                down_out_channels : List,
                down_activations: List,
                up_kernel_sizes : List,
                up_in_channels : List,
                up_out_channels : List,
                up_activations: List,
                padding_mode='zeros',
                bias=True):
    r"""Sequential CNN scale used for Unet CNN.

    The following arguments should have the same list length:
    'kernel_size', 'in_channels', 'out_channels', 'activations'.

    Inputs:
        kernel_sizes: list of integers defining the kernel sizes of the
                      sequence of convolution blocks.
        in_channels: list of integers defining the number of input features of the
                      sequence of convolution blocks.
        out_channels: list of integers defining the number of output features of the
                      sequence of convolution blocks.
        activations: list of strings defining the non-linear for the sequence of convolution blocks.
        padding_mode: padding mode
    """
    if not isinstance(scale_factor, int):
       raise ValueError(f'Scale factor ({scale_factor}) should be an integer number')

    if len(down_kernel_sizes) != len(down_activations):
        raise ValueError(f'"down_kernels_sizes" (len = {len(down_kernel_sizes)}) '
                         f'must have the same'
                         f'length as "down_activations" (len = {len(down_activations)})')
    if len(up_kernel_sizes) != len(up_activations):
        raise ValueError(f'"up_kernels_sizes" (len = {len(up_kernel_sizes)}) '
                         f'must have the same'
                         f'length as "up_activations" (len = {len(up_activations)})')
    

    down_layers = []
    up_layers = []
    
    for kernel, in_c, out_c, activ_name in zip(down_kernel_sizes, down_in_channels, down_out_channels, down_activations):
        activation = get_activation(activ_name)
        padding =int((kernel-1)/2)
        down_layers.append(
            nn.Conv2d(in_c, out_c, kernel_size=kernel, padding=padding, padding_mode=padding_mode, bias=bias)
        )
        down_layers.append(
            activation()
        )
    
    for kernel, in_c, out_c, activ_name in zip(up_kernel_sizes, up_in_channels, up_out_channels, up_activations):
        activation = get_activation(activ_name)
        padding =int((kernel-1)/2)
        up_layers.append(
            nn.Conv2d(in_c, out_c, kernel_size=kernel, padding=padding, padding_mode=padding_mode, bias=bias)
        )
        up_layers.append(
            activation()
        )
    
    return down_layers, up_layers

def _check_dict_consistency(scales_dict):

    scale_keys = {'scale_factor'}
    for phase in ['down_', 'up_']:
        scale_keys.update({phase+'kernel_sizes',
                           phase+'in_channels',
                           phase+'out_channels',
                           phase+'activations'})
    for key in scales_dict.keys():
        if key not in scale_keys: 
            raise ValueError(f'Key "{key}" not recognized. '
                             f'Scales dictionary should have the following keys: '
                             f'{scale_keys}')

    for phase in ['down_', 'up_']:
        scale_keys = {phase+'kernel_sizes',
                           phase+'in_channels',
                           phase+'out_channels',
                           phase+'activations'}
        scale_keys_list = scale_keys.copy()
        len_temp = []
        log = {}
        for key in scale_keys_list:
            len_temp.append(len(scales_dict[key]))
            log[key] = len(scales_dict[key])
        if len(set(len_temp)) > 1:
            raise ValueError(f'The following keys ({scale_keys_list}) '
                             f'in scales_dict {scales_dict} '
                             f'should have the same list length '
                             f'(found lengths {log})' )

def _build_unet(layers_description, padding_mode, bias=True):
    down_layers = []
    up_layers = []
    log.info(f'Building U-Net network...')
    num_scales = len(layers_description)
    for s, scale_dict in enumerate(layers_description):
        log.info(f'Building scale {s}')
        _check_dict_consistency(scale_dict)
        down, up = _make_scale(**scale_dict, padding_mode=padding_mode, bias=bias)
        if s < num_scales-1:
            down_layers.append(down)
            up_layers.append(up)
        else:
            bottleneck = down

    log.info(f'U-Net network succesfully built.\n')
    return down_layers, up_layers, bottleneck

class UNet(nn.Module):
    r""" UNet convolutional neural network as used by 
    Ronneberger et al. (2015)

    Arguments:
        layers_description: list of dictionaries with arguments to build network.
                            Each dict contains the arguments for one scale of 
                            convolutions. The number of dictionaries corresponds
                            to the number of scales.

                            For each scale; the input is downsampled by a scaling factor
                            and output is upsampled back to the original size.

                            Dictionary keys:
                            scale_factor: integer target domain size at which
                                          convolutions will be performed.
                            kernel_sizes: list of integers defining the kernel
                                          sizes of the sequence of convolution blocks.
                            in_channels: list of integers defining the number
                                          of input features of the
                                          sequence of convolution blocks.
                            out_channels: list of integers defining the number
                                          of output features of the
                                          sequence of convolution blocks.
                            activations: list of booleans defining if
                                         a non-linear (ReLU) activation is
                                         used (True) or not (False) for
                                         the sequence of convolution blocks.
        padding_mode : padding mode
         
    """
    def __init__(self, layers_description: Sequence[Mapping], padding_mode):
        super(UNet, self).__init__()

        down_scales, up_scales, bottleneck = _build_unet(layers_description, padding_mode)

        self.down_scale_names = []
        self.down_scale_factors = []
        for i, scale in enumerate(down_scales):
            name = f'Down_scale_{i}'
            self.down_scale_names.append(name)
            self.down_scale_factors.append(layers_description[i]['scale_factor'])
            setattr(self, name, nn.Sequential(*scale))
        
        self.bottleneck_name = f'Bottleneck_scale_{i}'
        setattr(self, self.bottleneck_name, nn.Sequential(*bottleneck))
        
        self.up_scale_names = []
        self.up_scale_factors = []
        self.num_scales = len(up_scales)
        i = self.num_scales - 1
        for scale in reversed(up_scales):
            name = f'Up_scale_{i}'
            self.up_scale_names.append(name)
            self.up_scale_factors.append(layers_description[i]['scale_factor'])
            setattr(self, name, nn.Sequential(*scale))
            i = i - 1

    def forward(self, x):
        input_size = list(x.size()[2:])
        down_outputs = []
        for name, factor in zip(self.down_scale_names, self.down_scale_factors):
            x = getattr(self, name)(x)
            down_outputs.append(x)
            x = nn.functional.max_pool2d(x, kernel_size=2)

        x = getattr(self, self.bottleneck_name)(x)

        i = self.num_scales - 1
        for name, factor in zip(self.up_scale_names, self.up_scale_factors):
            scale_size = [int(j/factor) for j in input_size]
            x = F.interpolate(x, size=scale_size, mode='bilinear', align_corners=False)
            x = getattr(self, name)(x+down_outputs[i])
            i = i - 1

        return x
