#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Functions for importing and exporting the model and configuration files.
"""

# native modules
import os
import glob
import shutil
import importlib.util
import logging
from collections import OrderedDict

# third-party modules
import torch
import numpy as np
import yaml

if importlib.util.find_spec('vtk') is not None:
    import vtk

import pytorch_lightning as pl
from pytorch_lightning.utilities import rank_zero_only

# local modules
import pulsenetlib.model

log = logging.getLogger(__name__)

def get_file_from_regex(save_dir, regex):
    available_files = glob.glob(os.path.join(save_dir,regex))
    # sort available files
    if len(available_files) >= 1:
        available_files.sort(key=os.path.getmtime)
        filename = os.path.basename(available_files[-1])
    else:
        raise FileNotFoundError(
            f'No file with regex ({regex}) found in {save_dir}')
        
    return os.path.join(save_dir,filename)
    

    
def resume_configuration_files(config_dir,
             train_config_path=None,
             model_config_path=None):
    """ Function for overwriting training and model configuration dicts.
    
    Reads the yaml configuration files located in the path indicated in
    config_dir and loads the selected training and model configuration 
    dictionaries.

    ----------    
    ARGUMENTS

        config_dir: folder where the configuration files are located
        train_config_path: training configuration files filename. Optional,
                           if None, default names is used:
                           *_conf.yaml'                        
        model_config_path: model configuration files filename. Optional,
                           if None, default names is used:
                           *_mconf.yaml'

    ----------    
    RETURNS

        training_configuration: Dictionary with training configuration 
        model_configuration: Dictionary with model configuration
    
    """
    if train_config_path is None:
        regex = '*_conf.yaml'
        train_config_path = get_file_from_regex(config_dir, regex) 
    elif not os.path.isfile(train_config_path):
        raise FileNotFoundError(f'{train_config_path} does not exist')
    else:
        pass

    if model_config_path is None:
        regex = '*_mconf.yaml'
        model_config_path = get_file_from_regex(config_dir, regex) 
    elif not os.path.isfile(model_config_path):
        raise FileNotFoundError(f'{model_config_path} does not exist')
    else:
        pass
    # reading the yaml files and updating the dictionaries
    with open(train_config_path, 'r') as f:
        train_configuration = yaml.load(f, Loader=yaml.FullLoader)
    
    with open(model_config_path, 'r') as f:
        model_configuration = yaml.load(f, Loader=yaml.FullLoader)

    return train_configuration, model_configuration



def load_pl_ckpt(ckpt_dir, file_to_load=None):
    """ Function for loading a Lightning checkpoint.
    
    ----------    
    ARGUMENTS
    
        train_configuration: dictionary with the training
                             configuration
        file_to_load: string with the name of the *.pth to
                      be loaded. Optional, if None, the latest
                      *.pth file in folder is selected.

    ----------    
    RETURNS
        path_to_ckpt: /path/to/ligtning/checkpoint.ckpt 
    
    """
    
    if file_to_load is None:        
        regex = '*.ckpt'
        path_to_ckpt = get_file_from_regex(ckpt_dir, regex)
    else:
        path_to_ckpt = os.path.join(ckpt_dir, file_to_load)
    
    if not os.path.isfile(path_to_ckpt):
        raise FileNotFoundError(
            f'Checkpoint file {path_to_ckpt} not found'
       ) 
    else:
        log.info(f'Loading checkpoint file {path_to_ckpt}\n')
    
    return path_to_ckpt


def save_configuration_files(save_dir,
                training_configuration, model_configuration,
                train_config_path=None, model_config_path=None):
    """ Function to save yaml files with the configuration dictionaries.

    ----------
    ARGUMENTS
    
        training_configuration: training configuration dictionary
        model_configuration: model configuration dictionary
        train_config_path: training configuration files filename. Optional,
                        if None, default names is used:
                        [modelFilename]_conf.yaml'                        
        model_config_path: model configuration files filename. Optional,
                         if None, default names is used:
                         [modelFilename]_mconf.yaml'
        
    ----------
    RETURNS
    
        ##none##
    
    """    
    
    if train_config_path is None:
        train_config_path = os.path.join(
            save_dir,
            training_configuration['modelFilename'] + '_conf.yaml'
        )
            
    if model_config_path is None:
        model_config_path = os.path.join(
            save_dir,
            training_configuration['modelFilename'] + '_mconf.yaml'
        )
    
    log.info(f'Saving yaml files with configuration dictionaries to folder:'
          f'{save_dir}')
    
    for name, file, config in zip(
                      ['Training','Model'],
                      [train_config_path,model_config_path],
                      [training_configuration,model_configuration]):
        if os.path.isfile(file):
            log.info(f'  {name} configuration yaml file is being overwriten:\n'
                  f'  {file}')
            
        # create YAML files in output folder (human readable)
        with open(file, 'w') as outfile:
            yaml.dump(config, outfile)

    log.info('')



def load_model(model_dir, ckpt_name=None, is_legacy=False, run_device=None):
    """ Load model and state from files in folder
    
    ----------    
    ARGUMENTS
        
        model_dir: path to the model training files
        ckpt_name: checkpoint name. Optional, if not specified, will find the
        best performing model.
        run_device: running device. Optional, default is to check and 
                select GPU if available.
    
    ----------    
    RETURNS
        
        net: neural network, updated to the checkpoint on ckpt_path
        state_dict: model's state dict
        train_configuration: dictionary with the training configuration
        model_configuration: dictionary with the model configuration
    
    """
    if ckpt_name is None:
        try:
            best_models_path = os.path.join(model_dir, 'checkpoints/best_k_models.yaml')
            with open(best_models_path, 'r') as f:
                temp = yaml.load(f, Loader=yaml.FullLoader)
                key_min = min(temp.keys(), key=(lambda k: temp[k]))
                ckpt_format = os.path.split(key_min)[-1]

            config_path, model_config_path, path_to_ckpt = \
                get_default_files(model_dir, f'checkpoints/{ckpt_format}')
            # boolean indicating must consider legacy file formats and functions
        except FileNotFoundError:
            config_path, model_config_path, path_to_ckpt = \
                get_default_files(model_dir, ckpt_format='*epoch*.pth')
            is_legacy = True
    else:
        config_path, model_config_path, path_to_ckpt = \
            get_default_files(model_dir, f'checkpoints/{ckpt_name}')


    log.info(f'Loading checkpoint at {path_to_ckpt}')
    if run_device is None:
        if torch.cuda.is_available():
            run_device = torch.device('cuda:0')
        else:
            run_device = torch.device('cpu')
    
    # reading the yaml files of the configuration dictionaries
    train_configuration = {}
    model_configuration = {}
    with open(config_path, 'r') as f:
        temp = yaml.load(f, Loader=yaml.FullLoader)
        train_configuration.update(temp)    
    with open(model_config_path, 'r') as f:
        temp = yaml.load(f, Loader=yaml.FullLoader)
        model_configuration.update(temp)
    
    # model properties
    model_name = model_configuration['model']
    channels = model_configuration['channels']
    num_input_channels = len(channels)
    input_frames = model_configuration['numInputFrames']
    output_frames = model_configuration['numOutputFrames']
    mask_name = model_configuration['mask']

    output_channels = channels
    if 'outputChannels' in  model_configuration:
        output_channels = model_configuration['outputChannels']
        for channel in output_channels:
            if channel not in channels:
                raise ValueError(f"Output channel name ('{channel}') "
                                 f"not found among available "
                                 f"channels ({channels}).")

    num_output_channels = len(output_channels)

    layers_description = None 
    if 'layersDescription' in model_configuration:
        layers_description = model_configuration['layersDescription']
    normalization = 'gaussian'
    if 'normalization' in model_configuration:
        normalization = model_configuration['normalization']
    padding_mode = 'replicate'
    if 'paddingMode' in model_configuration:
        padding_mode = model_configuration['paddingMode']

    std_norm = torch.tensor(
        model_configuration['stdNorm']).to(device=run_device)
    avg_remove = torch.tensor(
        model_configuration['avgRemove']).to(device=run_device)

    mask_channel = None
    if mask_name is not None:        
        for index, field in enumerate(channels):
            if field == mask_name:
                mask_channel = index
        if mask_channel is None:
            raise ValueError(f"mask name ('{mask_name}') "
                             f"not found among available "
                             f"channels ({channels}).")  
    # network
    net = pulsenetlib.model.PulseNet(num_input_channels, num_output_channels,
                                    input_frames, output_frames,
                                    std_norm, avg_remove,
                                    layers_description,
                                    model_name=model_configuration['model'],
                                    mask_channel=mask_channel,
                                    normalization=normalization,
                                    padding_mode=padding_mode,
                                 )
    
    state_dict = dict()
    if is_legacy:
        checkpoint = torch.load(
            path_to_ckpt, map_location=run_device)
        # create a stat_dict with renamed keys (only network, not model)
        for key in checkpoint['state_dict'].keys():
            state_dict[key.replace('model.','')] = \
                checkpoint['state_dict'][key]
        net.load_state_dict(state_dict)
    else:
        class DummyModel(pl.LightningModule):
            def __init__(self, model):
                super(DummyModel, self).__init__()
                self.model = model
            
        pl_loader = DummyModel(net)
        
        pl_loader.load_from_checkpoint(path_to_ckpt,
                            model=net)
        net = pl_loader.model
        net = net.to(run_device)

    return net, path_to_ckpt, train_configuration, model_configuration



def get_default_files(model_dir,
                      ckpt_format='./checkpoints/epoch=*.ckpt'):
    """ For a given folder, returns the paths to model training files.
    
    Its is based on the files default names and locations.
    
    ----------
    ARGUMENTS
    
        model_dir: path to folder
        ckpt_format: 
    
    ----------    
    RETURNS
    
        model_path: path of the model .py script
        config_path: path of the training configuration .yaml file
        mconfig_path: path of the model configuration .yaml file
        ckpt_path: path of the model configuration .ckpt file. If more
                   than one file is available on folder, the one that
                   was modified last is selected
    
    """
    paths = []
    for regex in ['*_conf.yaml', '*_mconf.yaml', ckpt_format]:
        paths.append(
            get_file(os.path.join(model_dir, regex))
            )
    config_path, mconfig_path, ckpt_path = paths

    return config_path, mconfig_path, ckpt_path



def get_file(file_path):
    """ Return full path to a regex path.
    If multiple exist, return the youngest one.
    """
    files = glob.glob(file_path)
    if len(files) == 0: raise FileNotFoundError(f'{file_path} not found.')
    elif len(files) > 1: files.sort(key=os.path.getmtime)
    return files[-1]


if __name__ == "__main___":
    pass
