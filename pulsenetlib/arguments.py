#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Module for dealing with the script arguments and training parameters.
"""

# native modules
import argparse
import logging
from multiprocessing.sharedctypes import Value

# third party modules
import yaml

# local modules
#none

log = logging.getLogger(__name__)

class SmartFormatter(argparse.HelpFormatter):
    """ Class with the argument parser format
    """
    def _split_lines(self, text, width):
        if text.startswith('R|'):
            return text[2:].splitlines()
        # this is the RawTextHelpFormatter._split_lines
        return argparse.HelpFormatter._split_lines(self, text, width)

class ParseKwargs(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, dict())
        for value in values:
            key, value = value.split('=')
            getattr(namespace, self.dest)[key] = value    
    
def parse_arguments(args):
    """ Argument parser function.
    
    ----------
    ARGUMENTS
    
        parser: argument parser
        
    ----------    
    RETURNS
    
        arguments: parsed arguments
    
    """
    parser = argparse.ArgumentParser(
                description='Training script.',
                formatter_class=SmartFormatter
    )
    
    parser.add_argument(
        '--trainingConf',
        default='trainConfig.yaml',
        help='R|Training yaml config file.\n'
             '  Default: trainConfig.yaml')
    parser.add_argument(
        '--modelDir',
        help='R|Output folder location for trained model.\n'
             'When resuming, reads from this location.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--modelFilename',
        help='R|Model name.\n'
        '  Default: written in trainingConf file.')
    parser.add_argument(
        '--dataDir',
        help='R|Dataset location.\n'
        '  Default: written in trainingConf file.')
    parser.add_argument(
        '--resume', action="store_true",
        default=False,
        help='R|Resumes training from checkpoint in modelDir.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--version', type=str,
        help='R|Logger version. Creates a folder: "modelDir/version"\n'
            '  Default: None. If version is not specified the logger inspects the modelDir \n'
            '  directory for existing versions, then automatically assigns the next available version.\n'
            '  If resume is set to True, loads from version checkpoing or the last available version.')
    parser.add_argument(
        '--batchSize', type=int,
        help='R|Batch size for training.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--maxEpochs', type=int,
        help='R|Maximum number training epochs.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--maxSteps', type=int,
        help='R|Maximum number training steps.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--maxDatapoints', type=int,
        help='R|Maximum number of datapoints.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--shuffle', action="store_true",
        help='R|Shuffle dataset when training.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--lr', type=str,
        help='R|Learning rate.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--numWorkers', type=int,
        help='R|Number of parallel workers for dataset loading.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--printTraining', action="store_true",
        default=False,
        help='R|Training debug options. Prints results during training.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--freqToFile', type=int,
        help='R|Epoch frequency for loss output to file/image saving.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--saveTopK', type=int,
        help='R|Save k best validation loss models during training.\n'
             '  Default: written in trainingConf file.')
    parser.add_argument(
        '--verbose', action="store_true",
        default=False,
        help='R|Prints detailed information throughout the run.\n'
             '  Default: False.')
    parser.add_argument(
        '--preproc', action="store_true",
        default=False,
        help='R|Perform preprocessing or run training.\n'
             '  Default: False.')
    parser.add_argument(
        '--overwritePreproc', action="store_true",
        default=False,
        help='R|Delete previous preprocessed files before performing preprocess.\n'
             '  Default: False.')
    parser.add_argument(
        '--logger', type=str, action="append",
        help='R|Logger set by user.\n'
             '  "np": saves logs in numpy format. \n'
             '  "tb": saves logs in tensorboard format  \n'
             '  Default: np')
    parser.add_argument(
        '-init_ckpt', '--initialCheckpoint', type=str,
        help='if provided, will initialize the network works to a previous checkpoint.'
    )
    parser.add_argument(
    '--gpus', type=int,
        default=1,
        help='R|List of gpu indices.\n'
             'Default: 1')

    # check arguments
    log.info('Parsing and checking arguments\n')
    return parser.parse_args(args)



def read_arguments(arguments, conf_resume=None):
    """ Reading the training configuration file.
        
    When parameters are defined both in configuration
    file and in the command, priority is given to the
    command options.
    
    ----------
    ARGUMENTS
        
        arguments: main script parsed arguments
        resume: set to True when resuming training
        
    ----------
    RETURNS
    
        conf: updated configuration dictionnary
        resume: boolean indicating if one must resume a 
                training
        verbose: boolean indicating if verbose run
                 (print details for each epoch)
        print_training: boolean indicating if images are saved
                        during training
        logger_name: string or list of strings
                     indicating the type of logger
                     ('np', 'tb')
    
    """
    
    # reading parameters file
    if conf_resume is None:
        with open(arguments.trainingConf, 'r') as f:
            conf = yaml.load(f, Loader=yaml.FullLoader)
    else:
        conf = conf_resume

    # overwriting parameters defined in file, if
    # provided on command.
    for parameter in ['modelDir', 'modelFilename', 'dataDir', 
                      'batchSize', 'maxDatapoints', 
                      'numWorkers', 'freqToFile', 'saveTopK']:
        argument = getattr(arguments, parameter)
        if argument is not None:
            conf[parameter] = argument
        else:
            pass # keep conf defaults

    if arguments.maxEpochs is not None and arguments.maxSteps is not None:
        raise ValueError('--maxEpochs and --maxSteps are mutually exclusive. Give only one')
    if arguments.maxEpochs is not None:
        conf['maxEpochs'] = arguments.maxEpochs
        if 'maxSteps' in conf:
            del conf['maxSteps'] 
    if arguments.maxSteps is not None:
        conf['maxSteps'] = arguments.maxSteps
        if 'maxEpochs' in conf:
            del conf['maxEpochs'] 
    
    if arguments.version is not None: conf['version'] = arguments.version
    if arguments.shuffle: conf['shuffleTraining'] = True
    if arguments.lr is not None: conf['optimizer']['lr'] = arguments.lr
    
    try:
        conf['optimizer']['lr'] = float(conf['optimizer']['lr'])
    except ValueError:
        if not conf['optimizer']['lr'] == 'auto':
            raise ValueError(
                f'Argument lr ({conf["optimizer"]["lr"]}) must be either "auto" or float number')
    
    if not arguments.resume:
        resume = conf['resumeTraining']
    else:
        resume = arguments.resume

    if arguments.initialCheckpoint is not None:
        initial_checkpoint = arguments.initialCheckpoint
    elif 'initializeFromCheckpoint' in conf:
        initial_checkpoint = conf['initializeFromCheckpoint']
    else:
        initial_checkpoint = None 

    # force to preproc
    if arguments.preproc:
        log.info('Running preprocessing only')
        resume = False        
    
    # force to print training
    if not arguments.printTraining:
        print_training = conf['printTraining']
    else:
        print_training = arguments.printTraining
    
    verbose = arguments.verbose

    logger_name = []
    if arguments.logger is not None:
        for name in arguments.logger:
            if name in ('np', 'tb'):
                logger_name.append(name)
    else:
        logger_name = conf['logger']
    if not logger_name:
        # if empty, fallback to default 
        logger_name = ['np']
    conf['logger'] = logger_name
    
    return conf, resume, verbose, print_training, initial_checkpoint 

