#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Mathematical functions
"""

# native modules
##none##

# third-party modules
import torch
import torch.nn as nn

import numpy as np
import scipy
import scipy.integrate

# local modules
##none##


class Derivation2D:
    """ Class for performing a 2D derivation of a tensor
    
    Derivation will be applied in 2D, considerig the last 2 dimensions of
    the tensor. A mask can be used to defined solid zones, where derivation
    will not be perfomed. Scheme close to the solid zones is degenerated to
    2nd order.
    
    For a reduce processing time, the derivation class defines the
    operator only once. If the calculation is performed only a few times,
    function `first_derivative` may be called (a wrapper to this class)
    
    ----------
    ARGUMENTS
        
        nx, ny: scalar field dimensions (number of rows and number of columns)
        number_frames: number of frames. Must be equal the total number of 2D
                       fields present in the tensor to derivate.
        mask: tensor indicating the regions to ignore, that is, a False is not
              considered in the derivation.              
              - If none (default) considers all field. 
              - If 2D, mask is copied and the same is applied for all frames. 
              - When higher dimension is given, derivation operator considers 
                every frame individually
        dx, dy: infinitesimal in x (row) and y (column) directions,
                respectively. Optional, unity as default
        device: device where to put tensors. Must be the same where the fields
                to be derived are stored. Optional, default is 'cpu'
        
    ----------    
    RETURNS
    
        dfield_dx, dfield_dy: when called, returns the derivated 
                              fields               
    
    """
    
    def __init__(self, nx, ny, number_frames, mask=None,
                 dx=1.0, dy=1.0, device='cpu'):
        
        self.dx, self.dy = dx, dy
        self.nx, self.ny = nx, ny
        self.number_frames = number_frames
        
        # defining if analysis is with multiple (one for each frame) 
        # or single mask
        mask_T = mask
        if mask is None or number_frames != mask.numel()/(nx*ny):
            
            if mask is not None: mask_T = mask.T

            # single mask for all frames
            # building the finite differences operator
            neighbourhood_array = build_neighbourhood_array(
                (ny,nx), mask=mask_T, device=device)
            operator_dx = build_derivative_operator(
                neighbourhood_array).to(device)
            
            neighbourhood_array = build_neighbourhood_array(
                (nx,ny), mask=mask, device=device)
            operator_dy = build_derivative_operator(
                neighbourhood_array).to(device)
                    
            # adding the batch dimension to the operator
            self.operator_dx_batch = expand_derivative_operator(
                operator_dx, number_frames
                )
            self.operator_dy_batch = expand_derivative_operator(
                operator_dy, number_frames
                )

        # TODO: currently doing the same work twice! think in a new way to do it!
        # unique masks
        else:
            mask_T = mask.transpose(-2,-1)                    
            # adding the batch dimension to the operator
            self.operator_dx_batch = \
                build_multiframe_derivative_operators(mask_T)
            self.operator_dy_batch = \
                build_multiframe_derivative_operators(mask)

                
    def __call__(self, field):
        # applying the derivative
        field_dx, field_dy = calculate_derivatives(
                field.view((self.number_frames,self.nx,self.ny)),
                self.operator_dx_batch,
                self.operator_dy_batch,
                dx=self.dx, dy=self.dy
                )
        return field_dx.reshape(field.shape), field_dy.reshape(field.shape)



def build_neighbourhood_array(shape, mask=None, device='cpu'):
    """ Create a neighbourhood array for defining the derivative operator
    
    Only considers the columns direction (derivating for each row). When 
    derivating in terms of rows, one must use transposed versions of
    the shape and mask as inputs
    
    ----------
    ARGUMENTS
        
        shape: array dimension
        mask: boolean array indicating the regions to derivate (elements
              set to False will be considered as not available for the
              calculation of the derivative). Optional, if not given,
              lower order schemes are set only at array borders.
        device: device where to put tensors. Must be the same where the fields
                to be derived are stored. Optional, default is 'cpu'
                      
    ----------    
    RETURNS
    
        neighbourhood_array:
            
            +2: 0 backward, 2 forward (forward scheme)
            +1: 0 backward, 1 forward (forward scheme)
             0: 2 points backward and forward (centered scheme)
            -1: 2 backward, 1 forward (backward scheme)
            -2: 2 backward, 0 forward (backward scheme)
            127: inside mask, not be used
    
    """
    
    neighbourhood_array = torch.zeros(shape, dtype=torch.int16).to(device)
    # setting left borders as +1 and +2 (forward scheme)
    neighbourhood_array[:,0] = +2
    neighbourhood_array[:,1] = +1
    # setting upper borders as -1 and -2 (backward scheme)
    neighbourhood_array[:,-1] = -2
    neighbourhood_array[:,-2] = -1
        
    if mask is not None:
        # convert mask to integer for performing operations
        mask_int = mask.long()
        
        # adding the information of the mask to the neighbors array
        neighbourhood_array[:,2:-2] = \
           neighbourhood_array[:,2:-2] - mask_int[:,1:-3] - mask_int[:,0:-4]
        neighbourhood_array[:,2:-2] = \
           neighbourhood_array[:,2:-2] + mask_int[:,4:] + mask_int[:,3:-1]
        
        # elements "inside" mask - not considered!    
        neighbourhood_array[~mask] = 2**7 - 1
    
    return neighbourhood_array

    

def build_derivative_operator(neighbourhood_array):
    """ Create an sparse matrix for the first derivative of 2D scalar fields
    
    Considers 4-order central scheme if 2 forward and backward neighbors are
    available, 2-order forward and backward scheme otherwise. Considers
    a derivation on columns direction (derivating for each row)
    
    ----------
    ARGUMENTS
        
        neighbourhood_array: array indicating the type of scheme to be used
                             for each element (border, next to masked region).
                             More information on `build_neighbourhood_array`
                      
    ----------    
    RETURNS
        
        operator: sparse 2D matrix with the first derivative coefficients.
                  To calculate the derivative, you should call:
                      `operator.matmul(f_columns)/dx`
                  where f_columns is the version of the
        
    """
    
    n_rows, n_cols = neighbourhood_array.shape
    n_grid_points = n_rows*n_cols
    
    # finite differencies coefficients
    # first we consider a 5-points stencil for everybody, later, elements
    # with null coefficients are removed
    backward_coeffs = torch.tensor([1/2, -2, 3/2, 0.0, 0.0])
    centered_coeffs = torch.tensor([1/12, -2/3, 0.0, 2/3, -1/12])
    forward_coeffs = torch.tensor([0.0, 0.0, -3/2, 2, -1/2])
        
    neighbourhood_vector = neighbourhood_array.reshape((n_grid_points))
    sparse_values = torch.zeros((n_grid_points,5))
    
    sparse_values[neighbourhood_vector == 0,:] = centered_coeffs
    sparse_values[neighbourhood_vector < 0,:] = backward_coeffs
    sparse_values[neighbourhood_vector > 0,:] = forward_coeffs
    sparse_values[neighbourhood_vector == 127,:] = torch.zeros((5))
    
    # row and columns of elements in grid
    elements_base_row = torch.vstack(n_cols*[torch.arange(n_rows)]).T
    elements_base_col = torch.vstack(n_rows*[torch.arange(n_cols)])
    
    # row and columns of elements in 2D operator matrix
    elements_row = elements_base_row*n_cols + elements_base_col
    elements_col = elements_row
    
    sparse_indices_row = torch.hstack(
        5*[elements_row.reshape(n_grid_points,1)])
    sparse_indices_col = torch.hstack(
        [elements_col.reshape(n_grid_points,1) + offset 
             for offset in range(-2,+3)]
        )
        
    # removing values that have a null coefficient
    sparse_indices_row = sparse_indices_row[~(sparse_values == 0.0)]
    sparse_indices_col = sparse_indices_col[~(sparse_values == 0.0)]
    sparse_indices = torch.stack(
         (sparse_indices_row,sparse_indices_col)
         )
    
    sparse_values = sparse_values[~(sparse_values == 0.0)]
    
    # building the torch sparse tensor
    operator = torch.sparse.FloatTensor(
        sparse_indices,
        sparse_values,
        torch.Size([n_rows*n_cols]*2)).requires_grad_(False)
        
    return operator



def expand_derivative_operator(operator_2D, batch_size):
    """ Expand 2D operator to be able to do derivatives for a batch arrays
    
    Coefficients are replicated on the first dimension in order to adapt
    the derivative operator to a batch (multiple 2D arrays) use
    
    ----------
    ARGUMENTS
        
        operator_2D: sparse 2D matrix with the first derivative coefficients,
                     shape (m, n)
        batch_size: number of frames to consider
        
    ----------    
    RETURNS
        
        operator_3D: sparse 3D matrix with the first derivative coefficients.
                     shape (batch_size, m, n) 
                     
        
    """
    
    indices_2D = operator_2D.coalesce().indices()
    values_2D = operator_2D.coalesce().values()
    n_2D_points = indices_2D.shape[1]
    
    # creating batch_size copies of the 2D indices and values
    indices_2D = torch.hstack(batch_size*[indices_2D])
    sparse_values_3D = torch.hstack(batch_size*[values_2D])
    
    indices_channel = torch.ones((1,batch_size*n_2D_points),
        dtype=torch.long, device=operator_2D.device
        )

    for ind in range(batch_size):
        indices_channel[0,ind*n_2D_points:(ind+1)*n_2D_points+1] = ind
        
    sparse_indices_3D = torch.vstack((indices_channel,indices_2D))
    
    operator_3D = torch.sparse.FloatTensor(
        sparse_indices_3D,
        sparse_values_3D,
        torch.Size([batch_size,*operator_2D.shape[-2:]])).requires_grad_(False)
    
    return operator_3D



def build_multiframe_derivative_operators(mask):
    """ Builds a 3D operator for a tensor of different masks

    If the same mask is applied for every frame, use `build_derivative_operator`
    and `expand_derivative_operator`

    """

    nx, ny = mask.shape[-2:]
    grid_size = nx*ny
    number_frames = int(torch.numel(mask)/(grid_size))
    device = mask.device
    mask_3D = mask.view((number_frames, nx, ny))

    # start with the maximun number of possible values, considering a 
    # 5-points stencil
    stencil_size = 5
    sparse_indices_3D = torch.zeros(
        (3,number_frames*grid_size*stencil_size),
        device=device,
        dtype=torch.long)
    sparse_values_3D = torch.zeros(
        (number_frames*grid_size*stencil_size),
        device=device
        )

    total_number_values = 0
    # loop for the creation of the 3D derivation operator
    for index_frame in range(number_frames):

        mask_frame = mask_3D[index_frame]

        frame_starting_index = index_frame*grid_size
        frame_ending_index = frame_starting_index + grid_size

        neighbourhood_array = build_neighbourhood_array(
            (ny,nx), mask=mask_frame, device=device)
        frame_operator = build_derivative_operator(
            neighbourhood_array).to(device)

        sparse_indices_2D = frame_operator.coalesce().indices()
        sparse_values_2D = frame_operator.coalesce().values()

        sparse_n_values = sparse_values_2D.numel()

        indices_frame = torch.ones((1,sparse_n_values),
            dtype=torch.long, device=device
        )*index_frame

        end_index = total_number_values + sparse_n_values
        # adding the frame indexes information
        sparse_indices_3D[:,total_number_values:end_index] = \
            torch.vstack((indices_frame,sparse_indices_2D))
        sparse_values_3D[total_number_values:end_index] = \
            sparse_values_2D
                
        total_number_values += sparse_n_values

    # ignoring null values
    sparse_indices_3D = sparse_indices_3D[:,:total_number_values]
    sparse_values_3D = sparse_values_3D[:total_number_values]

    # rebuilding sparse operator considering the mask by frame
    operator_3D = torch.sparse.FloatTensor(
        sparse_indices_3D,
        sparse_values_3D,
        torch.Size([number_frames, grid_size, grid_size])).requires_grad_(False)
        
    return operator_3D



def calculate_derivatives(field, operator_dx, operator_dy, dx=1.0, dy=1.0):
    """ Calculate the derivatives of a 2D array
    
    operators must be in batch form (one extra dimension)
    use `expand_derivative_operator`
    
    ----------
    ARGUMENTS
        
        field: scalar tensor with 2D arrays to be derivated. Must be in batch
               shape (batch, m, n). Add an extra dimension in order to deal
               with a single 2D field
        operator_dx: sparse matrix with the derivative coefficients, 
                      for derivation on rows direction (for each column).
                      More details in `build_derivative_operator` and 
                      `expand_derivative_operator`
        operator_dy: same as `operator_col`, but considers the columns
                     direction (for each row).
        dx, dy: infinitesimal in x (row) and y (column) directions,
                respectively. Optional, unity as default
        
    ----------    
    RETURNS
    
        dfield_dx, dfield_dy: tensor with the derivative of every 2D scalar
                              fields in the x and y directions (along columns
                              and rows, respectively), same shape as input
                              tensor `field`
    
    """
    
    batch_size, nx, ny = field.shape
    n_elements = nx*ny
    
    # data as vector, ordered by columns -> derivate in rows direction (x)    
    field_div_rows = field.transpose(-2,-1).reshape((batch_size,n_elements)) 
    # data as vector, ordered by rows -> derivate in columns direction (y)
    field_div_columns = field.reshape((batch_size,n_elements)) 
    
    # adding one dimension to perform the batch operation
    field_div_columns = field_div_columns.unsqueeze(-1)
    field_div_rows = field_div_rows.unsqueeze(-1)
        
    # calculating the derivatives and reshaping to match input field shape
    dfield_dx = torch.bmm(operator_dx,field_div_rows)/dx
    dfield_dx = dfield_dx.reshape(batch_size,ny,nx).transpose(-2,-1)
    
    dfield_dy = torch.bmm(operator_dy,field_div_columns)/dy
    dfield_dy = dfield_dy.reshape(batch_size,nx,ny)
    
    return dfield_dx, dfield_dy



def first_derivative(field, mask=None, infinitesimal=1.0):
    """ Wrapper function to calculate the x and y first derivatives
    
    Creates an instance of the Derivation2D
    """
    # getting shape of corresponing 3D tensor
    nx, ny = field.shape[-2:]    
    number_frames = int(torch.numel(field)/(nx*ny))
    
    derivate = Derivation2D(
        nx, ny, number_frames, mask=mask,
        dx=infinitesimal, dy=infinitesimal,
        device=field.device,
        )
    return derivate(field)



def derivate_dx(f, dx, mask=None):
    """ Wrapper function to calculate the derivative in x (rows direction)
    """
    f_dx, _ = first_derivative(f, mask=mask, infinitesimal=dx)    
    return f_dx



def derivate_dy(f, dx, mask=None):
    """ Wrapper function to calculate the derivative in y (columns direction)
    """
    _, f_dy = first_derivative(f, mask=mask, infinitesimal=dx)    
    return f_dy



def divergence(x, dx, mask=None):
    """ Calculate the divergence field of x, considering uniform
    infinitesimal dx
    """
    return derivate_dx(x, dx, mask) + derivate_dy(x, dx, mask)

def build_neighbourhood_array_normal(shape, mask=None, neigh_size=1, device='cpu'):
    neighbourhood_array = torch.zeros(shape, dtype=torch.float).to(device)   
    neigh_size = int(neigh_size)     
    if mask is not None:
        # convert mask to integer for performing operations
        mask_int = mask.long()
        
        # adding the information of the mask to the neighbors array
        for i in range(1, neigh_size):
            neighbourhood_array[:,:,neigh_size:-neigh_size] -= mask_int[:,:,(neigh_size-i):(-neigh_size-i)]
            neighbourhood_array[:,:,neigh_size:-neigh_size] += mask_int[:,:,(neigh_size+i):(-neigh_size+i)]
        
        neighbourhood_array[:,:,neigh_size:-neigh_size] -= mask_int[:,:,:-2*neigh_size]
        neighbourhood_array[:,:,neigh_size:-neigh_size] += mask_int[:,:,2*neigh_size:]
        # elements "inside" mask - not considered!    
        neighbourhood_array[~mask] = 0
    
    return neighbourhood_array

def calculate_normal(nx, ny, number_frames, mask=None, neigh_size=1, device='cpu'):
    normal = torch.zeros((number_frames,2,nx,ny), dtype=torch.float, device=device)
    
    if mask is not None:
        out_shape = mask.shape[:2]
        mask_3D = mask.view((number_frames, nx, ny))

        normal[:,0,:,:] = build_neighbourhood_array_normal(
            (number_frames,ny,nx), mask=mask_3D.transpose(-1,-2), neigh_size=neigh_size, device=device).transpose(-1,-2)
        
        normal[:,1,:,:] = build_neighbourhood_array_normal(
            (number_frames,nx,ny), mask=mask_3D, neigh_size=neigh_size, device=device)

        normal[normal!=0] = (normal/torch.sqrt(torch.sum(normal**2, dim=1, keepdims=True, dtype=torch.float)))[normal!=0]
        return normal.view(out_shape + (2,nx,ny))

    else:
        return normal



def online_mean_and_std(loader, transforms=None, dxdy=(1.0,1.0)):
    """ Compute the first and second statistical moments

                Var[x] = E[X^2] - E^2[X]
                
    ----------    
    ARGUMENTS

        loader: dataloader of the model
        
    ----------    
    RETURNS

        fst_moment: first moment in format (C,3), each row
                    representing a channel (quantity)
        second moment

    """
    # reading a case just to define the dimensions
    dummy_data, _, = next(iter(loader))
    _, c, t, h, w = dummy_data.shape
        
    cnt = torch.zeros((c,3))
    fst_moment = torch.empty((c,3))
    snd_moment = torch.empty((c,3))
    
    for h_data, __ in loader:
        b, _, _, _, _ = h_data.shape
        # number of pixels per channel (quantity)
        nb_pixels = b * t * h * w

        if torch.cuda.is_available():
            data = h_data.cuda(non_blocking=True)
        else:
            data = h_data
        
        if transforms is not None:
            for transform in transforms:
                transform(data)
        
        # calculate the derivatives of each page
        gdl_x = derivate_dx(data, dx=dxdy[0])
        gdl_y = derivate_dy(data, dx=dxdy[1])
        
        for i, field in enumerate([data, gdl_x, gdl_y]):
            for j in range(c):
                sum_ = torch.sum(field[:,j])
                sum_of_square = torch.sum(field[:,j] ** 2)

                fst_moment[j,i] = \
                    (cnt[j,i]*fst_moment[j,i] + sum_) / \
                        (cnt[j,i] + nb_pixels)

                snd_moment[j,i] = \
                    (cnt[j,i]*snd_moment[j,i] + sum_of_square) / \
                        (cnt[j,i] + nb_pixels)

                cnt[j,i] += nb_pixels

    return fst_moment, torch.sqrt(snd_moment - fst_moment ** 2)

class Solve_1ord_ODE:
    """
        Solve 1st order Ordinary Differential Equation with
        arbitrary non-constant coefficients. 
        A(t) dy/dt + B(t) y + C(t) = 0
        y(0) = t0
    """

    def __init__(self, dt, debug=False):
        """
            Arguments:
                dt (float): integration time-step
                debug (bool, optional): debug flag
        """
        self.dt = dt
        self.debug = debug

    def solve_stencil3(self, i, y0, int0, A, B, C):
        """
            Solving by integrating on all previous time-steps, resulting
            in second-order accuracy in time
            Arguments:
                i (int): current iteration 
                y0 (float): intial condition
                A (np.array): A coefficient at all time steps (0, ... , i)
                B (np.array): B coefficient at all time steps (0, ... , i)
                C (np.array): C coefficient at all time steps (0, ... , i)
        """
        t = np.arange(i-2, i+1)*self.dt

        if self.debug:
            print(f"i={i}")
            print(f"t={t}")    
 
        int1 = scipy.integrate.cumtrapz(-B/A, t, initial=int0)      
        int2 = scipy.integrate.romb(-C/A*np.exp(-int1), self.dt)
        res = np.exp(int1[-1])*(int2 + y0)
        
        if self.debug:
            print(f"int1={int1}")
            print(f"int2={int2}")
            print(f"y0={y0}")
            print(f"res={res}")
            print()
        
        return t[-1], res, int1[-1]
    
    def solve_o1(self, i, y0, int0, A, B, C):
        """
            Solving by integrating on all previous time-steps, resulting
            in second-order accuracy in time
            Arguments:
                i (int): current iteration 
                y0 (float): intial condition
                A (np.array): A coefficient at all time steps (0, ... , i)
                B (np.array): B coefficient at all time steps (0, ... , i)
                C (np.array): C coefficient at all time steps (0, ... , i)
        """
        t = np.arange(i-1, i+1)*self.dt

        if self.debug:
            print(f"i={i}")
            print(f"t={t}")    
    
        int1 = scipy.integrate.cumtrapz(-B/A, t, initial=int0)      
        int2 = scipy.integrate.trapz(-C/A*np.exp(-int1), t)
        res = np.exp(int1[-1])*(int2 + y0)
        
        if self.debug:
            print(f"int1={int1}")
            print(f"int2={int2}")
            print(f"y0={y0}")
            print(f"res={res}")
            print()
        
        return t[-1], res, int1[-1]
    
    def solve_o2(self, i, y0, A, B, C):
        """
            Solving by integrating on all previous time-steps, resulting
            in second-order accuracy in time
            Arguments:
                i (int): current iteration 
                y0 (float): intial condition
                A (np.array): A coefficient at all time steps (0, ... , i)
                B (np.array): B coefficient at all time steps (0, ... , i)
                C (np.array): C coefficient at all time steps (0, ... , i)
        """
        t = np.arange(0, i+1)*self.dt

        if self.debug:
            print(f"i={i}")
            print(f"t={t}")    
    
        int1 = scipy.integrate.cumtrapz(-B/A, t, initial=0)      
        int2 = scipy.integrate.trapz(-C/A*np.exp(-int1), t)
        res = np.exp(int1[-1])*(int2 + y0)
        
        if self.debug:
            print(f"int1={int1}")
            print(f"int2={int2}")
            print(f"y0={y0}")
            print(f"res={res}")
            print()
        
        return t[-1], res

def masked_mean(input, mask):
    return torch.sum(input[mask]) / torch.count_nonzero(mask)

if __name__ == '__main__':
    
    print("Testing the derivation code")
    # Testing the derivation
    
    # generating the uniform grid
    grid_size = 500
    x = torch.linspace(0, 100, steps=grid_size)
    y = torch.linspace(0, 100, steps=grid_size)
    
    dx = x[1] - x[0]
    
    # using reverse (considering rows as y and columns as x)
    grid_y, grid_x = torch.meshgrid(x, y)
    
    # defining the test equation
    z = 4*grid_y + 5*grid_x**2 + 5*grid_x*torch.sin(grid_y)
    
    # analytical derivatives
    analytical_dzdx = 10*grid_x + 5*torch.sin(grid_y)
    analytical_dzdy = 4 + 5*grid_x*torch.cos(grid_y)
    
    # adding dimensions to use on the functions
    z_torch = z.unsqueeze(0).unsqueeze(0)
        
    criterion = torch.nn.MSELoss()
    
    # calculating the error    
    error_dzdx = criterion(analytical_dzdx,
                           derivate_dx(z_torch,dx)[0,0])
    error_dzdy = criterion(analytical_dzdy,
                           derivate_dy(z_torch,dx)[0,0])
        
    print("\nDerivative error: {:.5e} {:.5e}\n".format(
                error_dzdx,error_dzdy))
    
