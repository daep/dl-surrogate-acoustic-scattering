#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Deep Learning Surrogate for the Temporal Propagation
# and Scattering of Acoustic Waves
# Copyright (C) 2022 A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau
#
# Contact
# antonio.alguacil.cabrerizo[at]usherbrooke.ca
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 5 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
    Classes that define the loss criterion used for training the model.
"""

# native modules
import logging
import functools
from typing import Callable

# third-party modules
import torch
import torch.nn as nn

# local modules
import pulsenetlib

log = logging.getLogger(__name__)

class Criterion(nn.Module):    
    def __init__(self, channels, L2lambda, GDL2lambda,
                 mask_channel=None, reduction='mean', normalize=True):
        """ Class that defines the loss functions.

        For instanciating, one most indicates the weight (lambda) for
        each component (L2, GDL2 representing
        the L2-norm, L2-norm of the spatial gradient). If any of the lambdas is null, the
        corresponding loss is not calculated.

        ----------
        ARGUMENTS

            channels (list): name of channels
            L2lambda (float/list): channel-wise weights of loss
            GDL2Lambda (float/list): channel-wise weights of gradient-difference loss (GDL)
            mask_channel (int): index of mask channels  (loss is not calculated)
            reduction (str): type of reduciton applied to loss
            normalize (bool): normalize the output wrt sum of lambdas
            
        """
                
        super(Criterion, self).__init__()

        self.channels = channels.copy()
        self.normalize = normalize

        if isinstance(L2lambda, float):
            L2lambda = [L2lambda]
        if isinstance(GDL2lambda, float):
            GDL2lambda = [GDL2lambda]

        self.reduction_func = None
        if isinstance(reduction, Callable):
            self.reduction = 'none'
            self.reduction_func = reduction
        elif isinstance(reduction, str):
            self.reduction = reduction
        else:
            raise ValueError(f'reduction arg must be either a string or a callable.')

        self._mse = nn.MSELoss(reduction=self.reduction)
        
        # define the functions that calculate the loss forms
        # only selected if the multiplier is bigger than 0
        self.loss_funs = []
        self.loss_names = []
        loss_lambdas = []

        # do not calculate loss on an output mask
        if mask_channel is not None:
            self.channels.pop(mask_channel)

        self.register_buffer('L2lambda', torch.tensor(L2lambda), persistent=False)
        self.register_buffer('GDL2lambda', torch.tensor(GDL2lambda), persistent=False)

        if torch.any(self.L2lambda > 0.0):
            for i, field in enumerate(self.channels): 
                self.loss_names.append(f'L2Lambda_{field}')
                loss_lambdas.append(self.L2lambda[i].item())
            self.L2lambda = self.L2lambda.view(1, len(self.channels), 1, 1, 1)
            self.loss_funs.append(
                self.loss_L2
                             )
        self.calc_gradient = False
        if torch.any(self.GDL2lambda > 0.0): 
            for i, field in enumerate(self.channels): 
                self.loss_names.append(f'GDL2Lambda_{field}')   
                loss_lambdas.append(self.GDL2lambda[i].item())
            self.GDL2lambda = self.GDL2lambda.view(1, len(self.channels), 1, 1, 1)
            self.loss_funs.append(
                self.loss_GDL2
                             )
            self.calc_gradient = True
            
        self.register_buffer('loss_lambdas', torch.tensor(loss_lambdas), persistent=False)
        # Normalization by the number of loss components
        self.register_buffer('normalization', torch.ones(len(self)), persistent=False)


    def get_loss_names(self):
        return self.loss_names
    
    def get_loss_lambdas(self):
        return self.loss_lambdas

    def __len__(self):
        """ Returns the number of loss terms.
        """
        return self.loss_lambdas.numel()
    

    def summary(self):
        """ Prints the loss terms functions.
        """
        log.info('Selected loss terms:\n')
        for loss_fun in self.loss_funs:
            log.info(f'{loss_fun} \n')
    
        
    def forward(self, out, target, mask=None):
        """ Calculate the loss.

        ----------
        ARGUMENTS
            out: model prediction tensor, format (N,C,D,H,W)
            target: target tensor, format (N,C,D,H,W)
            mask: tensor indicating the masked regions (not to be considered
                  on analysis). Optional, none as default
        
        ----------    
        RETURNS
            result: loss tensor, normalized by the
                    normalization factor
        
        """
                
        _assert_no_grad(target)

        self.mask = mask
        nx, ny = target.shape[-2:]
        
        # Building the derivation operator. Since the derivation algorithm
        # considers a different standard (True for calculating the 
        # derivation and False for ignoring), the opposite of mask is used
        if self.calc_gradient:
            number_frames = int(target.numel()/(nx*ny))
            self.derivate = pulsenetlib.math_tools.Derivation2D(
                nx, ny, number_frames, mask=mask if mask is not None else mask, device=target.device)
        
            if self.mask is not None:
                self.normal = pulsenetlib.math_tools.calculate_normal(
                    nx, ny, number_frames, mask=self.mask, neigh_size=3, device=out.device)
                self.mask_normal = ((self.normal[:,:,0] != 0) |  (self.normal[:,:,1] != 0))[:,:,None]
        
        # calculate the loss term for every available function
        result = torch.empty(0, device=out.device, dtype=out.dtype )
        for loss_fun, norm in zip(self.loss_funs, self.normalization):
            result = torch.cat((result, loss_fun(out, target)/norm))
        return result
    
        
    def loss_L2(self, prediction, target):
        """ Calculate the loss of the fields
        
        Each frame is normalized by the standard deviation
        observed for each channel in the target's first time 
        frame (:,:,0,:,:).
        
        ----------
        ARGUMENTS
        
            prediction: tensor with the model's output, format
                        (N, C, D, H, W)
            target: tensor with the target, format 
                    (N, C, D, H, W)
        
        ----------
        RETURNS
            
            (loss)
            
        """

        if self.normalize: 
            std_mse = pulsenetlib.transforms.std_first_frame(target, mask=self.mask)
            ret = self._mse(prediction/std_mse, target/std_mse)*self.L2lambda
        
        else: 
            ret = self._mse(prediction, target)*self.L2lambda
        
        if self.reduction != 'none':
            return ret.view(-1)
        elif self.reduction_func is None:
            return ret
        else:
            return self.reduction_func(ret)
    
    def loss_GDL2(self, prediction, target):
        """ Calculate the loss of the gradients
        
        Deals with a single channel.
        
        ----------
        ARGUMENTS
        
            prediction: tensor with the model's prediction,
                        format (N, C, D, H, W)
            target: tensor with the target, format
                    (N, C, D, H, W)
            
        ----------
        RETURNS
            
            (loss)
            
        """
        target = target.permute((0,1,2,4,3)).contiguous()
        prediction = prediction.permute((0,1,2,4,3)).contiguous()

        grad_x_tar, grad_y_tar = self.derivate(target)
        
        std_grad_x_first_frame = \
            pulsenetlib.transforms.std_first_frame(grad_x_tar, mask=self.mask)
        std_grad_y_first_frame = \
            pulsenetlib.transforms.std_first_frame(grad_y_tar, mask=self.mask)
        
        
        if self.normalize:
            # selecting the max grad std for each channel for 
            # normalization
            std_grad = torch.max(std_grad_x_first_frame, std_grad_y_first_frame)

            grad_x_tar, grad_y_tar = self.derivate(target/std_grad)
            grad_x_pred, grad_y_pred = self.derivate(prediction/std_grad)
        
        else:
            grad_x_tar, grad_y_tar = self.derivate(target)
            grad_x_pred, grad_y_pred = self.derivate(prediction)
        
        target = target.permute((0,1,2,4,3)).contiguous()
        prediction = prediction.permute((0,1,2,4,3)).contiguous()
        grad_x_tar = grad_x_tar.permute((0,1,2,4,3)).contiguous()
        grad_y_tar = grad_y_tar.permute((0,1,2,4,3)).contiguous()
        grad_x_pred = grad_x_pred.permute((0,1,2,4,3)).contiguous()
        grad_y_pred = grad_y_pred.permute((0,1,2,4,3)).contiguous()

        if self.mask is not None:
            grad_x_tar[self.mask_normal] = 0
            grad_y_tar[self.mask_normal] = 0
            grad_x_pred[self.mask_normal] = 0
            grad_y_pred[self.mask_normal] = 0

        loss = torch.abs(grad_x_pred - grad_x_tar)**2.
        loss += torch.abs(grad_y_pred - grad_y_tar)**2.
    
        if self.reduction != 'none':
            loss = torch.mean(loss) if self.reduction == 'mean' \
                else torch.sum(loss)

        ret = loss*self.GDL2lambda
        
        if self.reduction != 'none':
            return ret.view(-1)
        elif self.reduction_func is None:
            return ret
        else:
            return self.reduction_func(ret)

def _assert_no_grad(tensor):
    assert not tensor.requires_grad, \
        "nn criterions don't compute the gradient w.r.t. targets - please " \
        "mark these tensors as not requiring gradients"
