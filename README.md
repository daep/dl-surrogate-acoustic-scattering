# Deep Learning Surrogate for the Temporal Propagation and Scattering of Acoustic Waves
Demonstration code of the paper:

> A.Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau. Deep Learning Surrogate for the Temporal Propagation
> and Scattering of Acoustic Waves (2022), AIAA Journal

## Example: Scattering of acoustic wave with airfoil
<img align="center" width="128" src="images/video_target.gif">  
Target simulation (LBM Solver Pabalos [https://gitlab.com/unigespc/palabos](https://gitlab.com/unigespc/palabos))
<br/><br/>
<img align="center" width="128" src="images/video_output.gif"> 
Prediction with U-Net Neural Network 
<br/><br/>
<img align="center" width="200" src="images/video_error.gif">  
MSE error between target and prediction
<br/><br/>


## Contact and bug reports

Please contact `antonio.alguacil.cabrerizo[at]usherbrooke.ca` for questions.

## Install the library

```
git clone https://gitlab.isae-supaero.fr/daep/dl-surrogate-acoustic-scattering
```

## Install dependencies

We recommend using a pip virtual env to install all the project dependencies. 
Install all the dependencies by running in the root folder of `dl-surrogate-acoustic-scattering`

```
 pip install -e .
```

**Disclaimer**: This project has been tested on Ubuntu 18.04 and in a Red Hat Enterprise Linux Server release 7.5 (Maipo).

## Downloading the training and testing data

Run this command to download the training (1.27 Gb) and testing (24.6 Mb) data
```
cd data
bash download_data.sh
tar -xvzf dataset_d2_5pls_3cyl_3rect_res200_it500.tar.gz
tar -xvzf dataset_testing.tar.gz
```

## Using pre-trained neural network for predicting acoustic scattering


Run this command to download the models (2.27 Gb) and the losses plots (Optional, 5.56 Mb) data
```
cd models
bash download_models.sh
tar -xvzf models.tar.gz
```

Then go to the examples folder

```
cd examples
```

Run the following command to run a prediction

```
predict --simConf cylinder_glob_unet.yaml
```

You can edit the `yaml` file in order to change the simulation parameters.

Another example for the local normalization "LocUnet" is provided, named `cylinder_loc_unet.yaml`.

## Training the model from scratch

### Preprocessing the data

First you need to preprocess the data `.vti` files into a `.pt` native pytorch format, so that the loading of data is done more efficiently during training.

The `configuration.yaml` file has the following parameters that affect the data preprocessing step:

**Data parameters:**
This category of params handles the location of the data, as well as the format of files.

| param 	| example | description |
|-	| :-:| - |
| `dataset`	| `"mydatabase"` | dataset name, to be used on files and folders |
| `dataPathRaw` | `"path/to/raw/datbase"` | path to the folder with the **raw** database (simulation data) for training |
| `dataPath` | `"path/to/preproc/database"` | path to the folder with **preprocessed** database for training |
| `formatVTK`	| `"{:06d}/vtk{:06d}.vt"` | format of the raw (simulation) data filename |
| `formatPT`	| `"scene{:06d}_group{:05d}.pt"` | format of the preprocessed data filename |
| `frameStep`	| `1` | interval between solution steps (must be a multiple of the LBM recorded values) |
| `numWorkers`	| `0` | number of parallel workers for dataloader (if 0, selected by pytorch) |

**Model parameters:**
Parameters that affect the model definition (e.g. input channels, type of data)

| param 	| example | description |
|-	| :-:| - |
| `numInputFrames`	| `4` | How many data frames will be used as input |
| `numOutputFrames`	| `1` | How many data frames will be output (and compared with a target frame during training) |
| `channels` | `['pressure', 'velocity-x']'` | name of the quantities to consider from preprocessing to training. Options: `pressure`, `velocity-x`, `velocity-y`|

Once configured, you can launch the data preprocessing by executing:

```
train --trainingConf configuration.yaml --preproc
```

## Training the model

To train the model, edit again the `configuration.yaml` file with the parameters listed below. Then run this command:
```
train --trainingConf configuration.yaml
```

**Data paramaters**  

| param 	| example | description |
|-	| :-:| - |
| `numWorkers`	| `0` | number of parallel workers for dataloader (if 0, selected by pytorch) |

**Output**  
  
| param 	| example | description |
|-	| :-:| - |
| `modelDir`	| `'/path/to/model/dir'` | output folder for trained model and loss log |
| `version`	| `'version_1'` | experiment version |
| `modelFilename`	| `'convModel'` | trained model name |

**Training parameters**  
  
| param 	| example | description |
|-	| :-:| - |
| `resume` | `false` | Resume training from checkpoint |
| `maxEpochs`| `100` | Maximum number of training epochs |
| `batchSize`| `32` | Batch size |
| `maxDatapoints`| `0` | Maximum number of datapoints (if 0, all available datapoints are used). A datapoint is the group of frames that is composed by the merge of the input and target frames. When the maximun number of datapoints is smaller than the number of available groups, a reduced number of groups per scene (simulation) is selected rather than a few complete scenes, aiming at a more diverse database|
| `selectMode`| `first` | How the group of frames are selected from the scenes during preprocessing. For _n_ groups:<br/><br/><ul><li>`first`: first _n_ groups (start of the scene)</li><li>`last`: last _n_ groups (end of the scene)</li><li>`random`: random _n_ groups, uniform distribution</li></ul> `first` and `last` options will only change behaviour of the preprocessing when the selected number of datapoints is smaller then the number of available groups; `random` will only change the order of the groups.|
| `shuffleTraining`| `true` | Shuffle data batches during training |
| `flipData`| `true` | Flips data fields during training |

**Optimizer parameters**

Adam optimizer parameters.  

| param 	| example | description |
|-	| :-:| - |
| `overrideatresume` | `false` | override optimizer when resuming training |
| `lr` | `1.0e-4` | learning rate |
| `weight_decay` | `0.0` | weight decay |

**Scheduler parameters**  

| param 	| example | description |
|-	| :-:| - |
| `type` | `'reduceLROnPlateau'` | scheduler type |
| `overrideatresume` | `false` | override scheduler when resuming training |

Check the PyTorch [documentation](https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate) for more information on schedulers.

**Model parameters**  
<ins> Data preparation </ins>

| param 	| example | description |
|-	| :-:| - |
| `scalarAdd` | `'[-1.0]'` | operations to be performed per channel, as sorted by `channels`. Scalar to add to the complete field |
| `stdNorm` | `'[1.0]'` | weighting for the division by the std of the first frame |
| `avgRemove` | `'[0.0]'` | weighting of the average component removal |

<ins> Loss terms </ins>

`fooLambda`: weigthing for each loss term (set to 0 to disable).

| param 	| example | description |
|-	| :-:| - |
| `L2Lambda` | `1.0` | Mean-squared error of channels |
| `GDLLambda` | `1.0` | Mean-squared error of gradients of channels |

**Training monitoring**

| param 	| example | description |
|-	| :-:| - |
| `freqToFile` | `1` | epoch frequency for loss output to disk |
| `printTraing` | `True`/`False` | saves predictions fields during training |
| `channelsToPlot` | `['pressure']` | which channels are plotted |
| `slices` | `- [0, 'x_slice, 75']` | List of lists (one per slice). Each element is composed of: <ul><li>`global data idx`</li><li>`slice type`=`'x_slice'` or `'y_slice'`</li><li>`slice coordinate`</li>|

## Logger

Two loggers are available:
- `np`: saves losses in numpy format to disk (Default)
- `tb`: tensorboard logger

The user can specify it with arguments:
``` 
train --trainingConf configuration.yaml --logger tb
``` 

When using tensorboard:

Go to the tensorboard output directory (e.g. `<modelDir>/<version>`) and run the following command:

```
tensorboard --logdir=<modelDir>/<version>
```

Open it in a web-browser the link.

If you are working in a remote server, you can set the port:

```
tensorboard --logdir=<modelDir>/<version> --port <portNumber> --bind_all
```
and create a ssh tunnel to your local machine.

``` 
ssh -4NfL localhost:<portNumberRemote>:localhost:<portNumberLocal> username@server
```
