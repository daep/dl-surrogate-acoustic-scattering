#!/usr/bin/env python

"""
The setup script for pip. Type `pip install -e .` for installation in your 
virtual environment
"""

from setuptools import setup, find_packages

requirements = ['numpy', 'scipy', 'matplotlib', 'torch', 'pytorch-lightning',
                'PyYAML', 'torchvision', 'pandas',
                'vtk', 'pyevtk', 'gdown']
setup_requirements = []
tests_requirements = ['unittest']

setup(
    author='A. Alguacil, M. Bauerheim, M.C. Jacob, S. Moreau',
    author_email='antonio.alguacil.caberizo@usherbrooke.ca',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.8'
    ],
    description='PulseNet: Learning surrogate models for acoustic propagation',
    install_requires=requirements,
    license='GNU General Public License v3',
    long_description='\n\n',
    include_package_data=True,
    keywords='acoustic propagation wave deep learning pytorch',
    name='pulsenet',
    packages=find_packages(include=['pulsenet']),
    setup_requires=setup_requirements,

    version='1.0.0',
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'train=pulsenetlib.train:cli_main',
            'predict=pulsenetlib.predict:cli_main',
            'tensorboard_to_pkl=pulsenetlib.tensorboard_to_pkl:main',
            'plot_loss_pkl=pulsenetlib.plot_loss_pkl:main',
        ],
    },
)
